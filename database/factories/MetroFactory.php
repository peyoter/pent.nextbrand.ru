<?php

namespace Database\Factories;

use App\Models\Metro;
use Illuminate\Database\Eloquent\Factories\Factory;

class MetroFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Metro::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'metro' => $this->faker->unique()->word(),
            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime,
        ];
    }
}
