<?php

namespace Database\Factories;

use App\Models\Penthouse;
use Illuminate\Database\Eloquent\Factories\Factory;

class PenthouseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Penthouse::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'penthouse_slug' => $this->faker->unique()->sentence(6),
            'penthouse_title' => $this->faker->sentence(6),
            'penthouse_desc' => $this->faker->paragraph,
            'full_price_ru' => $this->faker->randomNumber(),
            'full_price_usd' => $this->faker->randomNumber(),
            'full_price_eur' => $this->faker->randomNumber(),
            'square' => $this->faker->randomFloat('1', '45', '350'),
            'ceiling' => $this->faker->randomFloat('1', '2', '3'),
            'floor' => $this->faker->randomDigit,
            'rooms' => $this->faker->randomDigit,
            'bedrooms' => $this->faker->randomDigit,
            'bathrooms' => $this->faker->randomDigit,
            'penthouse_id' => $this->faker->unique()->randomNumber(),
            'square_terrace' => $this->faker->randomFloat('1', '0', '150'),
            'terrace' => $this->faker->boolean(),
            'concierge_service' => $this->faker->boolean(),
            'fireplace' => $this->faker->boolean(),
            'twostorey' => $this->faker->boolean(),
            'authors_design' => $this->faker->boolean(),
            'penthouse_main_img' => $this->faker->image(),
            'penthouse_img' => $this->faker->image(),
            'penthouse_plan' => $this->faker->image(),
            'complex_id' => $this->faker->numberBetween('1','5'),
            'employee_id' => $this->faker->numberBetween('1','5'),
            'apartment_id' => $this->faker->numberBetween('1','4'),
            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime,
        ];
    }
}
