<?php

namespace Database\Factories;

use App\Models\Complex;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComplexFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Complex::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'complex_slug' => $this->faker->unique()->sentence(4),
            'complex_title' => $this->faker->sentence(4),
            'complex_desc' => $this->faker->paragraph(),
            'complex_address' => $this->faker->streetAddress,
            'delivery' => $this->faker->sentence,
            'infrastructure' => $this->faker->sentence,
            'complex_main_img' => $this->faker->image(),
            'complex_img' => $this->faker->image(),
            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime,
        ];
    }
}
