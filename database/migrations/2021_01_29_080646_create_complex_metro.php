<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplexMetro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complex_metro', function (Blueprint $table) {
            $table->id();
            $table->foreignId('complex_id')->references('id')->on('complexes')->onDelete('cascade');
            $table->foreignId('metro_id')->references('id')->on('metros')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complex_metro');
    }
}
