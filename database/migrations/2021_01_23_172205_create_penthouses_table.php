<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenthousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penthouses', function (Blueprint $table) {
            $table->id();
            $table->string('penthouse_slug')->index()->unique();
            $table->string('penthouse_title');
            $table->text('penthouse_desc')->nullable();
            $table->integer('full_price_ru')->unsigned()->nullable();
            $table->integer('full_price_usd')->unsigned()->nullable();
            $table->integer('full_price_eur')->unsigned()->nullable();
            $table->float('square')->unsigned();
            $table->float('ceiling')->unsigned()->nullable();
            $table->tinyInteger('floor')->unsigned()->nullable();
            $table->tinyInteger('rooms')->unsigned()->nullable();
            $table->tinyInteger('bedrooms')->unsigned()->nullable();
            $table->tinyInteger('bathrooms')->unsigned()->nullable();
            $table->bigInteger('penthouse_id')->unsigned()->unique();
            $table->float('square_terrace')->unsigned()->nullable();
            $table->boolean('terrace')->default(false);
            $table->boolean('concierge_service')->default(false);
            $table->boolean('fireplace')->default(false);
            $table->boolean('twostorey')->default(false);
            $table->boolean('authors_design')->default(false);
            $table->string('views')->nullable();
            $table->string('penthouse_main_img')->nullable();
            $table->string('penthouse_img')->nullable();
            $table->string('penthouse_plan')->nullable();
            $table->float('coordinates_x')->nullable();
            $table->float('coordinates_y')->nullable();
            $table->string('download')->nullable(); // заменить на файл
            $table->foreignId('complex_id')->references('id')->on('complexes');
            $table->foreignId('employee_id')->references('id')->on('employees');
            $table->foreignId('apartment_id')->references('id')->on('apartments');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penthouses');
    }
}
