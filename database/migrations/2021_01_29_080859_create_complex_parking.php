<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplexParking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complex_parking', function (Blueprint $table) {
            $table->id();
            $table->foreignId('complex_id')->references('id')->on('complexes')->onDelete('cascade');
            $table->foreignId('parking_id')->references('id')->on('parkings')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complex_parking');
    }
}
