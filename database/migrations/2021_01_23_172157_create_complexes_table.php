<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplexesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complexes', function (Blueprint $table) {
            $table->id();
            $table->string('complex_slug')->index()->unique();
            $table->string('complex_title')->index();
            $table->text('complex_desc')->nullable();
            $table->string('complex_address')->nullable();
            $table->string('delivery')->nullable();
            $table->string('infrastructure')->nullable();
            $table->string('complex_main_img')->nullable();
            $table->string('complex_img')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complexes');
    }
}
