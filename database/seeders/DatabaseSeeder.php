<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ApartmentSeeder::class,
            ArticleSeeder::class,
            ClassificationSeeder::class,
            ComplexSeeder::class,
            DistrictSeeder::class,
            EmployeeSeeder::class,
            LocationSeeder::class,
            MetroSeeder::class,
            ParkingSeeder::class,
            TagSeeder::class,
            PenthouseSeeder::class,
        ]);
//        \App\Models\User::factory(10)->create();
    }
}
