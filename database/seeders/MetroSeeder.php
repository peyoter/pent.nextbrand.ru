<?php

namespace Database\Seeders;

use App\Models\Metro;
use Illuminate\Database\Seeder;

class MetroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Metro::factory()
            ->count(10)
            ->create();
    }
}
