<?php

namespace Database\Seeders;

use App\Models\Penthouse;
use Illuminate\Database\Seeder;

class PenthouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Penthouse::factory()
            ->count(20)
            ->create();
    }
}
