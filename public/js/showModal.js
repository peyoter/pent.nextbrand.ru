let showOpenBtn = document.querySelectorAll('.show-open');
let showModal = document.querySelector('.show');
let showClose = document.querySelector('.show__close');

for(let i = 0; i < showOpenBtn.length; i++) {
	showOpenBtn[i].addEventListener('click', function(e) {
		e.preventDefault();
		showModal.classList.add('active');
		body.classList.add('no-scroll');
		overlayModal.classList.add('visible');
	});
}

showClose.addEventListener('click', function(e) {
	e.preventDefault();
	showModal.classList.remove('active');
	body.classList.remove('no-scroll');
	overlayModal.classList.remove('visible');
});

overlayModal.addEventListener('click', function() {
	showModal.classList.remove('active');
})