function getFormData($form){
    let unindexed_array = $form.serializeArray();
    let indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function success(data){
    $(".js-mansion__page").html(data);
}

$(document).ready(function(){
  $('.js-mansion-filter').submit(function(e){
       e.preventDefault()
       let catalogData =  getFormData($('.js-mansion-filter'));

      $.ajax({
          type: 'POST',
          url: 'catalog/filterAjax',
          data: catalogData,
          success: success,
      });

  });
});
