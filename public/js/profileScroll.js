let mansionCoor = ($('.mansion').offset());
let houseNext = ($('.js-house-profile').offset());
$(window).on("scroll", function() {
	if ($(window).scrollTop() > mansionCoor.top && $(window).scrollTop() < houseNext.top) $('.profile').addClass('fixed');
	else $('.profile').removeClass('fixed');
});