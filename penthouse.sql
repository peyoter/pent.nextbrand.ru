-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 27 2021 г., 21:05
-- Версия сервера: 5.7.29-log
-- Версия PHP: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `penthouse`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `text`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'voluptatem', 'Aliquam id veniam iure quod. Qui sit eos sequi et. Sit est temporibus fuga quasi iure ut at. Numquam quos quos voluptatem et id eum.', '1980-11-24 21:22:21', '2004-08-19 13:54:24', NULL),
(2, 'velit', 'Molestias quo consequuntur libero et ad. Voluptas iste quis quod eius repellendus hic explicabo. Dolores reiciendis ab perferendis. Nam qui eum praesentium cupiditate dolorem porro.', '1980-12-25 16:32:53', '1979-10-25 20:09:11', NULL),
(3, 'eum', 'Assumenda est aut et saepe. Excepturi esse consequuntur blanditiis sed est. Eveniet omnis omnis omnis exercitationem quidem dolorem et quaerat. Voluptas reiciendis aperiam vero et.', '2013-06-18 23:11:18', '2008-08-20 17:13:52', NULL),
(4, 'perspiciatis', 'Maiores voluptatem quo veritatis libero quibusdam deserunt magnam quae. Doloribus perferendis voluptatum quo quo qui cum dolor. Nemo et est quia quaerat.', '2016-11-20 06:30:30', '1970-03-26 05:51:34', NULL),
(5, 'aut', 'Qui atque voluptas aliquam. Nemo ratione ut magni qui modi. Odit dolorum dicta et officiis facere. Voluptatibus cupiditate eum et. Pariatur labore dolore vitae quae non ipsam blanditiis.', '2006-11-15 03:05:29', '2010-05-23 04:15:22', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `complexes`
--

CREATE TABLE `complexes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `infrastructure` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `complexes`
--

INSERT INTO `complexes` (`id`, `slug`, `title`, `desc`, `address`, `class`, `delivery`, `infrastructure`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Vitaererumminimapossimus', 'Vitae rerum minima possimus.', 'Nihil commodi vero dolor autem numquam vel. Non eum id voluptates soluta neque qui at voluptates. Ex ad sapiente ut in et possimus est autem.', '44373 Susan Mews', 'iste', 'Est recusandae facere quis doloremque est hic.', 'Qui quia ut ducimus dolorem aliquam et nemo.', '/images/barkli/barkli-1-dekstop.png', '2000-07-28 09:28:00', '2007-10-11 10:29:01', NULL),
(2, 'Doloremquealiquamsimilique', 'Doloremque aliquam similique.', 'Expedita non exercitationem eveniet qui. Quia quod voluptatem sit aut exercitationem sunt. Corrupti officiis perferendis quo id.', '7227 Herzog Landing', 'omnis', 'Quia iste ipsum autem accusamus.', 'Officiis doloribus dolorem accusantium numquam aliquid natus placeat delectus.', '/images/barkli/barkli-1-dekstop.png', '1995-12-22 19:51:05', '2020-08-14 04:59:33', NULL),
(3, 'Quilaboreutearumrepellenduseligendi', 'Qui labore ut earum repellendus eligendi.', 'Provident a commodi dolore quae reprehenderit voluptatem. Porro rem architecto odit nihil. Maiores eius deserunt aut autem quo. Harum accusamus provident dolorum rerum explicabo.', '7166 Hilpert Branch Apt. 698', 'qui', 'Cum quidem quidem fugiat dolores animi.', 'Ea voluptate ut et qui tenetur suscipit.', '/images/barkli/barkli-1-dekstop.png', '2014-11-04 03:57:45', '1978-02-13 02:06:02', NULL),
(4, 'Voluptasesseinunde', 'Voluptas esse in unde.', 'Necessitatibus et est similique exercitationem labore porro. Soluta rem dolorem et maxime veniam esse numquam.', '56742 Rebekah Prairie', 'fugiat', 'Veniam non in in est velit.', 'Sunt ea exercitationem praesentium dolor.', '/images/barkli/barkli-1-dekstop.png', '2014-11-26 12:54:51', '1982-06-09 21:05:38', NULL),
(5, 'Consequaturidatutlaborum', 'Consequatur id at ut laborum.', 'Vel temporibus tempore pariatur facilis aperiam. Labore sit quia quam deleniti rerum amet. Debitis minima veritatis at placeat.', '980 Fadel Passage Apt. 993', 'aut', 'Autem pariatur aut voluptatem occaecati.', 'Et corrupti est cumque distinctio saepe provident velit quia.', '/images/barkli/barkli-1-dekstop.png', '2007-04-30 01:58:48', '1993-01-12 20:59:40', NULL),
(6, 'barkli', 'ЖК \"БАРКЛИ ГАЛЕРИ\"', 'Sed ornare risus sit amet neque convallis, non elementum odio venenatis. Donec felis elit, sodales et viverra nec, efficitur ac orci. In suscipit justo ac sapien dapibus, vitae iaculis purus accumsan.', 'Москва, ул. Новый Арбат, 46', 'Клубный дом', 'Сдача в 2020', 'Бассейн, парковая зона', '/images/barkli/barkli-1-dekstop.png', '2021-01-22 19:13:09', '2021-01-22 19:13:09', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `phone`, `desc`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Zelma Nicolas', 'heidenreich.maribel@example.org', '+1557976944434', 'Esse voluptatem quos ducimus tempore qui maxime.', '/images/profile/profile.png', '2016-06-25 22:15:18', '2017-03-22 21:37:13', NULL),
(2, 'Valentina Feeney', 'marilyne.roberts@example.org', '+4245154775970', 'Error tenetur qui laboriosam maiores voluptatem voluptas.', '/images/profile/profile.png', '1979-06-04 05:24:48', '1971-04-05 02:50:37', NULL),
(3, 'Dr. Rubye Buckridge', 'yblick@example.com', '+5523359554734', 'Earum aspernatur eius aspernatur adipisci culpa eos.', '/images/profile/profile.png', '1977-04-03 07:13:22', '1998-07-17 05:02:39', NULL),
(4, 'Dr. Vance Gutmann Sr.', 'maxwell63@example.com', '+6304809667637', 'Enim magni labore nostrum natus.', '/images/profile/profile.png', '1977-03-26 07:17:50', '2013-07-19 11:27:17', NULL),
(5, 'Dr. Helen Hartmann', 'xdaniel@example.net', '+5022221280403', 'Dolorem dolores a illo.', '/images/profile/profile.png', '2010-12-07 03:48:56', '2017-02-01 21:23:21', NULL),
(6, '<span>Марина</span>Афанасенкова', 'rieltor@penthouse.moscow', '+7 495 120 11 00', 'Риэлтор, эксперт в области элитной коммерческой недвижимости', '/images/profile/profile.png', '2021-01-20 18:54:33', '2021-01-28 18:54:33', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2014_10_12_000000_create_users_table', 1),
(11, '2014_10_12_100000_create_password_resets_table', 1),
(12, '2019_08_19_000000_create_failed_jobs_table', 1),
(13, '2021_01_23_165359_create_employees_table', 1),
(14, '2021_01_23_172138_create_tags_table', 1),
(15, '2021_01_23_172157_create_complexes_table', 1),
(16, '2021_01_23_172205_create_penthouses_table', 1),
(17, '2021_01_23_172236_create_articles_table', 1),
(18, '2021_01_23_180917_create_penthouse_tag_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `penthouses`
--

CREATE TABLE `penthouses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `furniture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_price_ru` int(10) UNSIGNED NOT NULL,
  `full_price_usd` int(10) UNSIGNED NOT NULL,
  `full_price_eur` int(10) UNSIGNED NOT NULL,
  `square` double(8,2) UNSIGNED NOT NULL,
  `ceiling` double(8,2) UNSIGNED NOT NULL,
  `floor` tinyint(3) UNSIGNED NOT NULL,
  `rooms` tinyint(3) UNSIGNED NOT NULL,
  `bedrooms` tinyint(3) UNSIGNED NOT NULL,
  `bathrooms` tinyint(3) UNSIGNED NOT NULL,
  `parking` set('Подземный','Наземный') COLLATE utf8mb4_unicode_ci NOT NULL,
  `apartment` set('С отделкой','Без отделки','Whitebox','С мебелью') COLLATE utf8mb4_unicode_ci NOT NULL,
  `penthouse_id` bigint(20) UNSIGNED NOT NULL,
  `square_terrace` double(8,2) UNSIGNED DEFAULT NULL,
  `terrace` tinyint(1) NOT NULL DEFAULT '0',
  `concierge_service` tinyint(1) NOT NULL DEFAULT '0',
  `fireplace` tinyint(1) NOT NULL DEFAULT '0',
  `twostorey` tinyint(1) NOT NULL DEFAULT '0',
  `authors_design` tinyint(1) NOT NULL DEFAULT '0',
  `views` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `download` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complex_id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `penthouses`
--

INSERT INTO `penthouses` (`id`, `slug`, `title`, `desc`, `district`, `metro`, `furniture`, `full_price_ru`, `full_price_usd`, `full_price_eur`, `square`, `ceiling`, `floor`, `rooms`, `bedrooms`, `bathrooms`, `parking`, `apartment`, `penthouse_id`, `square_terrace`, `terrace`, `concierge_service`, `fireplace`, `twostorey`, `authors_design`, `views`, `images`, `plan`, `download`, `complex_id`, `employee_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Officiaveritatisautemquinihilaccusantiumconsequatur', 'Officia veritatis autem qui nihil accusantium consequatur.', 'Rerum et quod ad qui ipsa aut est. Natus assumenda et quia quas sed veritatis enim. At quia aut est quis.', 'fugit', 'Dicta quasi aliquam.', 'Vel et veniam.', 2819, 1824, 3379838, 187.90, 3.00, 9, 7, 0, 4, 'Подземный', 'С отделкой', 29, 145.40, 1, 1, 1, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 2, 1, '1991-07-03 16:47:11', '2014-08-29 16:37:35', NULL),
(2, 'Quaesuscipitrecusandaeipsautrationeomnissed', 'Quae suscipit recusandae ipsa ut ratione omnis sed.', 'Totam doloremque eveniet iusto numquam magnam velit omnis perferendis. Aliquid nihil eum iste magni et id quia. Neque sed velit autem consequatur in temporibus. Ex sit dicta odit nisi animi et cum quo.', 'debitis', 'Est nobis aspernatur et.', 'Sed et non.', 9, 2502, 43, 85.10, 2.10, 1, 3, 0, 8, 'Подземный', 'С отделкой', 29959147, 87.90, 1, 1, 1, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 5, 3, '2020-04-22 23:24:31', '1977-02-24 10:23:04', NULL),
(3, 'Placeatenimnumquamipsamvoluptatumpariaturaccusamusquisquam', 'Placeat enim numquam ipsam voluptatum pariatur accusamus quisquam.', 'Ea accusamus et tenetur ea et similique. Consequatur officiis aut doloribus consequatur perferendis. Hic fuga et deserunt et minima esse adipisci odio. Commodi doloribus dolores ut ea inventore.', 'molestias', 'Suscipit sit veniam nostrum.', 'Velit sunt.', 23494, 3, 6544303, 238.40, 2.60, 9, 4, 4, 0, 'Подземный', 'С отделкой', 93768, 98.60, 1, 1, 0, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 1, 4, '1995-02-19 13:46:32', '1992-01-01 09:37:10', NULL),
(4, 'Reiciendissuscipitcorruptiveritatis', 'Reiciendis suscipit corrupti veritatis.', 'Eos quia perferendis adipisci quia nobis sint temporibus. Nobis non optio illum praesentium consectetur explicabo harum minima. Necessitatibus laudantium suscipit ab quos non nihil laboriosam.', 'fugit', 'Aliquid asperiores fugit dolores.', 'Hic ea a eius.', 5787, 2772057, 824, 270.60, 2.60, 4, 4, 9, 6, 'Подземный', 'С отделкой', 10, 22.30, 0, 0, 0, 0, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 2, 5, '1982-07-15 19:37:23', '2004-12-14 10:55:45', NULL),
(5, 'Omnisvoluptatevoluptaslaudantiumvoluptatem', 'Omnis voluptate voluptas laudantium voluptatem.', 'Minima nostrum eum sint sapiente. Inventore reiciendis aspernatur enim illo consequatur ex eveniet. Necessitatibus natus molestiae totam consequatur ipsa.', 'autem', 'Laudantium rerum neque provident.', 'Eum praesentium.', 1837757, 6, 5, 321.10, 2.20, 8, 7, 9, 8, 'Подземный', 'С отделкой', 84584842, 13.10, 0, 0, 1, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 4, 5, '1993-10-23 00:29:08', '1976-02-05 02:12:46', NULL),
(6, 'Quiadebitisquisexpeditafaciliscorruptiquo', 'Quia debitis quis expedita facilis corrupti quo.', 'Rem omnis quia modi repellendus velit. Et quidem blanditiis accusantium ducimus.', 'inventore', 'Reiciendis quo ut.', 'Nulla earum possimus voluptas.', 7, 1, 524838112, 247.80, 3.00, 1, 8, 5, 2, 'Подземный', 'С отделкой', 33487, 74.90, 1, 0, 1, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 4, 3, '2010-04-07 12:19:06', '2019-03-29 13:29:58', NULL),
(7, 'Ametvoluptasperferendisvoluptatibusperferendisvoluptatumarchitectoprovident', 'Amet voluptas perferendis voluptatibus perferendis voluptatum architecto provident.', 'Dolorum qui et dignissimos enim ipsa earum. In illo velit autem nam sed. Aspernatur saepe suscipit veniam voluptas aut magni quo.', 'et', 'Dolorum excepturi recusandae atque est.', 'Voluptatum rerum ut.', 59451, 1, 59, 213.60, 2.70, 9, 9, 0, 4, 'Подземный', 'С отделкой', 80828744, 12.50, 0, 0, 1, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 2, 2, '2015-10-25 02:56:54', '1981-09-24 07:07:15', NULL),
(8, 'Nonconsequaturautminimaquibusdam', 'Non consequatur aut minima quibusdam.', 'Numquam aliquam maxime sunt quas unde. At atque beatae illo ab necessitatibus non recusandae. Natus qui repellendus qui sed sequi quos et. Adipisci delectus earum ut magni sapiente. Rerum quia eos eum dolore veritatis id.', 'temporibus', 'Natus ut qui inventore.', 'Sunt molestias sed qui.', 60957013, 4, 15891830, 241.50, 2.60, 3, 9, 9, 8, 'Подземный', 'С отделкой', 7507576, 17.50, 0, 1, 0, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 5, 4, '2007-02-18 13:24:29', '1999-09-02 11:32:03', NULL),
(9, 'Iustodignissimosquibusdamquilaboriosam', 'Iusto dignissimos quibusdam qui laboriosam.', 'Commodi quos dolores consequatur qui vero illum. Expedita sed molestiae et. Dolorum quidem ad hic recusandae. Molestias voluptates veritatis dolor nisi.', 'tenetur', 'Nesciunt dolores non voluptatem.', 'Tempora libero modi.', 29849722, 7231, 491229835, 323.00, 2.70, 1, 9, 9, 7, 'Подземный', 'С отделкой', 6932, 29.50, 0, 0, 1, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 2, 2, '2001-05-20 15:16:48', '1971-03-15 00:22:59', NULL),
(10, 'Quivelitetautdistinctiolaboriosamet', 'Qui velit et aut distinctio laboriosam et.', 'Et qui qui delectus ut. Eum eveniet et consequuntur et quis fugiat. Laboriosam vel dolor dolorem et omnis culpa eum. Ratione tenetur beatae doloribus hic et.', 'recusandae', 'Perspiciatis illum vel dolores.', 'Nostrum a ea voluptatem sint.', 5, 92971, 663, 138.10, 2.00, 2, 8, 3, 7, 'Подземный', 'С отделкой', 9509193, 134.40, 1, 0, 1, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 4, 5, '2014-03-03 05:51:08', '1987-03-21 14:29:07', NULL),
(11, 'Illoofficiisearumquiamaximeeumautemfacereet', 'Illo officiis earum quia maxime eum autem facere et.', 'Voluptas est minima odio veritatis. Similique quis repudiandae itaque voluptatem. Quaerat voluptatem fuga mollitia doloribus. Quia maiores magni illo qui vel.', 'qui', 'Dolore veniam vitae voluptatem.', 'Accusantium aut soluta reprehenderit.', 7292323, 79772, 6300545, 76.80, 2.60, 4, 8, 5, 8, 'Подземный', 'С отделкой', 7, 145.10, 1, 0, 0, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 4, 5, '1977-01-11 13:35:22', '1971-03-05 04:00:08', NULL),
(12, 'Ullamillumdebitiseligendideleniti', 'Ullam illum debitis eligendi deleniti.', 'Consequatur est nemo autem. Cumque possimus possimus sunt eos rerum amet. Et nostrum cumque asperiores et temporibus placeat.', 'consequatur', 'Ea ad quia et.', 'Sunt modi qui.', 6815, 2149434, 9934776, 314.10, 2.30, 6, 7, 4, 7, 'Подземный', 'С отделкой', 2382006, 33.10, 0, 0, 1, 0, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 6, 4, '1979-08-06 05:44:20', '1978-01-23 17:59:09', NULL),
(13, 'Autsimiliquenobisquibusdamporro', 'Aut similique nobis quibusdam porro.', 'Commodi qui quo veniam eaque maiores. Minima vel id ullam vitae et qui. Harum expedita non perspiciatis animi.', 'id', 'Quia nostrum fuga praesentium.', 'Quod aperiam.', 9659252, 38006471, 88914, 51.10, 2.30, 6, 3, 4, 4, 'Подземный', 'С отделкой', 861, 13.80, 0, 1, 1, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 5, 2, '1978-06-08 02:06:17', '1994-08-31 00:38:07', NULL),
(14, 'Aperiamutetullammagniquod', 'Aperiam ut et ullam magni quod.', 'Consectetur sint nihil laudantium. Repellendus ut ut aperiam sed.', 'iusto', 'Ad ipsam molestiae in.', 'Alias blanditiis vitae.', 7, 167, 8579, 299.00, 2.70, 6, 9, 3, 1, 'Подземный', 'С отделкой', 30981894, 60.10, 1, 1, 1, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 3, 2, '2019-04-08 10:17:35', '1983-03-28 12:15:42', NULL),
(15, 'Dolorumnatusrecusandaeconsequaturexpeditaquiarem', 'Dolorum natus recusandae consequatur expedita quia rem.', 'Autem et animi velit velit voluptas alias. Voluptatem qui amet explicabo maxime velit sed. Sit quia et blanditiis sit.', 'voluptas', 'Facilis magnam consequatur.', 'Consectetur soluta quibusdam qui.', 96455, 30655, 224, 111.90, 2.30, 2, 9, 3, 7, 'Подземный', 'С отделкой', 351957854, 26.40, 0, 1, 1, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 3, 4, '1973-10-06 23:20:47', '1990-09-14 02:10:16', NULL),
(16, 'Voluptatemeasedtotamillumetaspernaturrerumaut', 'Voluptatem ea sed totam illum et aspernatur rerum aut.', 'Est corporis asperiores nostrum accusamus. Maxime vel sint eaque quibusdam qui officiis occaecati. At aut sed nulla hic illum et cum.', 'dolor', 'Natus dicta.', 'Doloremque maxime voluptatem est.', 909, 5714, 9216, 286.00, 2.90, 0, 8, 3, 4, 'Подземный', 'С отделкой', 125451372, 15.30, 0, 0, 0, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 3, 2, '1985-02-23 18:29:33', '2002-12-12 15:38:12', NULL),
(17, 'Animiundeitaqueconsequaturaliquamillumaspernaturdoloremquedistinctio', 'Animi unde itaque consequatur aliquam illum aspernatur doloremque distinctio.', 'Distinctio quis qui consectetur. Est et et quia ad repellendus magnam assumenda neque. Odio voluptatum non enim ipsam sit ad. Iure tenetur maiores quo distinctio deserunt sunt nisi.', 'repellendus', 'Temporibus voluptatem earum.', 'Laboriosam esse rerum.', 1844, 3, 1481, 102.60, 2.40, 6, 4, 2, 5, 'Подземный', 'С отделкой', 7099, 77.70, 1, 1, 1, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 2, 2, '1973-01-31 11:28:11', '1997-04-26 00:19:08', NULL),
(18, 'Omnisiureducimustenetur', 'Omnis iure ducimus tenetur.', 'Consequatur sint ratione animi nulla omnis ipsa. Dolores optio cum omnis nobis vitae. Quo veritatis pariatur quia fuga quod.', 'est', 'Nihil iste cumque.', 'Ad dignissimos dicta est.', 378932, 88618, 346019260, 68.70, 2.80, 2, 6, 5, 5, 'Подземный', 'С отделкой', 929689, 95.20, 1, 1, 1, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 5, 2, '1991-08-22 00:48:20', '1983-01-02 07:14:54', NULL),
(19, 'Corruptietquiaestassumenda', 'Corrupti et quia est assumenda.', 'Quas dolorum quasi labore dolores. Odio voluptas minima aut iusto. Sit itaque repellat exercitationem rerum iste. Consequatur ratione molestiae id aut. Recusandae temporibus recusandae vel excepturi ducimus enim in doloremque.', 'ipsa', 'Voluptatem aut necessitatibus.', 'Omnis qui ducimus.', 276082300, 21541602, 5309, 83.30, 2.50, 5, 7, 1, 5, 'Подземный', 'С отделкой', 6355159, 100.30, 1, 0, 0, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 5, 4, '1978-06-29 23:57:07', '1988-08-09 15:54:43', NULL),
(20, 'Cumquesimiliquefugiatsednecessitatibuseosillumasperiores', 'Cumque similique fugiat sed necessitatibus eos illum asperiores.', 'Sed voluptas tenetur rerum voluptas ducimus voluptas autem. Asperiores neque vel aut suscipit rerum officiis modi. Cupiditate tempora sequi itaque et sint quam.', 'veritatis', 'Dolorum rerum repellendus.', 'Et minus dicta voluptatem.', 855831, 96934853, 4183, 95.90, 2.40, 4, 8, 8, 1, 'Подземный', 'С отделкой', 973, 95.80, 1, 1, 0, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 4, 5, '1996-04-22 03:12:55', '2004-09-17 12:21:16', NULL),
(21, 'DuplexpenthousewithanopenterraceintheresidentialcomplexBARKLEYGALLERY', 'Двухуровневый пентхаус с открытой террасой в ЖК “БАРКЛИ ГАЛЕРИ”', 'Пентхаус на последнем этаже известного дома купца Филатова — элитный жилой\r\nдом, известный москвичам как «Дом с рюмкой», находится в начале улицы Остоженка. В квартире высокие потолки — 3,4 метра, удобная планировка:\r\nсветлая гостиная с тремя окнами и видом на Кремль, кухня-столовая, хозяйская спальня с отдельной ванной комнатой, детская, спальня, вторая\r\nванная комната. Из окон квартиры открывается великолепный вид на Кремль и Храм Христа Спасителя.\\\\r\\\\nЕсть возможность присоединения мансарды, \r\nдополнительные 164м².\\\\r\\\\nПентхаус на последнем этаже известного дома купца Филатова — элитный жилой дом, известный москвичам как «Дом с рюмкой» находится в начале улицы Остоженка.', 'Замоскоречье', 'Арбатская, Новокузнецкая', 'Бассейн, парк', 122000000, 122000000, 122000000, 425.00, 3.60, 45, 5, 3, 4, 'Подземный', 'С мебелью', 734874647632, 120.00, 1, 1, 0, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/plan/plan.png', NULL, 6, 6, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `penthouse_tag`
--

CREATE TABLE `penthouse_tag` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `penthouse_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `penthouse_tag`
--

INSERT INTO `penthouse_tag` (`id`, `tag_id`, `penthouse_id`, `created_at`, `updated_at`) VALUES
(1, 5, 21, NULL, NULL),
(2, 6, 21, NULL, NULL),
(3, 7, 21, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `slug`, `tag`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'esse', 'aspernatur', '2005-08-04 18:02:07', '1995-07-06 15:20:34', NULL),
(2, 'aut', 'beatae', '1998-05-10 20:12:22', '1979-11-25 09:57:55', NULL),
(3, 'qui', 'et', '2010-04-07 03:36:20', '1993-07-17 08:40:02', NULL),
(4, 'et', 'culpa', '1976-09-24 23:05:17', '1970-03-24 06:19:47', NULL),
(5, 'camin', 'С камином', NULL, NULL, NULL),
(6, 'moscow_city', 'Москва Сити', NULL, NULL, NULL),
(7, 'park', 'Вид на парк', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `complexes`
--
ALTER TABLE `complexes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `complexes_slug_unique` (`slug`);

--
-- Индексы таблицы `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD UNIQUE KEY `employees_phone_unique` (`phone`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `penthouses`
--
ALTER TABLE `penthouses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `penthouses_slug_unique` (`slug`),
  ADD UNIQUE KEY `penthouses_penthouse_id_unique` (`penthouse_id`),
  ADD KEY `penthouses_complex_id_foreign` (`complex_id`),
  ADD KEY `penthouses_employee_id_foreign` (`employee_id`);

--
-- Индексы таблицы `penthouse_tag`
--
ALTER TABLE `penthouse_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penthouse_tag_tag_id_foreign` (`tag_id`),
  ADD KEY `penthouse_tag_penthouse_id_foreign` (`penthouse_id`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`),
  ADD UNIQUE KEY `tags_tag_unique` (`tag`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `complexes`
--
ALTER TABLE `complexes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `penthouses`
--
ALTER TABLE `penthouses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `penthouse_tag`
--
ALTER TABLE `penthouse_tag`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `penthouses`
--
ALTER TABLE `penthouses`
  ADD CONSTRAINT `penthouses_complex_id_foreign` FOREIGN KEY (`complex_id`) REFERENCES `complexes` (`id`),
  ADD CONSTRAINT `penthouses_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`);

--
-- Ограничения внешнего ключа таблицы `penthouse_tag`
--
ALTER TABLE `penthouse_tag`
  ADD CONSTRAINT `penthouse_tag_penthouse_id_foreign` FOREIGN KEY (`penthouse_id`) REFERENCES `penthouses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `penthouse_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
