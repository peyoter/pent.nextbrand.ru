-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 29 2021 г., 15:02
-- Версия сервера: 5.7.29-log
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `penthouse`
--

-- --------------------------------------------------------

--
-- Структура таблицы `apartments`
--

CREATE TABLE `apartments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `apartment_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `apartments`
--

INSERT INTO `apartments` (`id`, `apartment_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'С отделкой', '2016-04-30 17:35:32', '1984-09-28 02:22:51', NULL),
(2, 'Без отделки', '1981-06-17 04:22:43', '1973-01-07 12:00:52', NULL),
(3, 'Whitebox', '1972-04-11 06:38:07', '2002-11-07 23:38:49', NULL),
(4, 'С мебелью', '2021-01-04 08:13:28', '1995-05-18 09:33:18', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `article_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `article_text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `article_title`, `article_desc`, `article_text`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ipsam', 'Eum deserunt et repudiandae voluptas eum provident impedit nobis.', 'Eligendi nihil provident cupiditate distinctio aut et. Consequatur vel quaerat consequatur quia dolores. Ea sint recusandae veniam et blanditiis sint atque.', '1983-06-15 11:03:52', '1991-03-19 07:32:27', NULL),
(2, 'magni', 'Voluptatum porro magnam et fugiat sunt.', 'Ducimus vel sint autem ut eveniet atque. Aperiam libero ab doloribus. Fuga est aut non.', '2009-08-28 12:11:04', '1993-02-24 07:00:00', NULL),
(3, 'repellendus', 'Dolorum exercitationem quia sunt necessitatibus eligendi voluptatum magni.', 'Modi vel nobis vitae. Nobis magnam consequatur autem mollitia. Soluta tempore aliquam non in eum. Pariatur et voluptates et. Qui ut qui odit aliquid quia aut. Ea molestias ratione consectetur alias.', '1980-05-18 01:17:15', '1989-01-15 20:52:52', NULL),
(4, 'odit', 'Debitis quibusdam repellendus maiores aut.', 'Fugiat non voluptas voluptate tempore possimus. Veritatis qui dolores explicabo. Dolorum nulla et ipsam nam deleniti qui.', '2020-07-30 17:59:53', '2019-08-05 03:37:07', NULL),
(5, 'aut', 'Voluptas ut enim fuga voluptas repudiandae.', 'Impedit nihil eos omnis porro omnis sit. Iusto porro eos repellendus a. Ut enim perspiciatis consequatur hic distinctio totam.', '1999-10-06 16:29:18', '1985-01-16 19:02:47', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `classes`
--

CREATE TABLE `classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `classes`
--

INSERT INTO `classes` (`id`, `class`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'neque', '1992-11-08 02:29:47', '1989-04-26 04:18:14', NULL),
(2, 'nisi', '1983-11-29 18:57:27', '1997-12-29 09:53:59', NULL),
(3, 'architecto', '1982-12-20 05:50:53', '2017-02-23 00:37:06', NULL),
(4, 'nam', '2000-10-08 11:08:04', '2017-04-30 17:51:15', NULL),
(5, 'Клубный дом', '1978-01-11 09:25:15', '2018-07-05 01:35:59', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `class_complex`
--

CREATE TABLE `class_complex` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complex_id` bigint(20) UNSIGNED NOT NULL,
  `class_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `class_complex`
--

INSERT INTO `class_complex` (`id`, `complex_id`, `class_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 2, 2, NULL, NULL),
(5, 6, 3, NULL, NULL),
(6, 5, 4, NULL, NULL),
(7, 6, 5, NULL, NULL),
(8, 5, 4, NULL, NULL),
(9, 4, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `complexes`
--

CREATE TABLE `complexes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complex_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complex_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complex_desc` text COLLATE utf8mb4_unicode_ci,
  `complex_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `infrastructure` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complex_main_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complex_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `complexes`
--

INSERT INTO `complexes` (`id`, `complex_slug`, `complex_title`, `complex_desc`, `complex_address`, `delivery`, `infrastructure`, `complex_main_img`, `complex_img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'andreevski', 'жк \"Андреевский\"', 'Consectetur et totam voluptatum est ducimus voluptate explicabo. Magni ut qui sit. Voluptas pariatur tempore voluptatibus reiciendis rerum quibusdam.', '16352 Weissnat Station Suite 895', 'Dicta facere explicabo voluptas quibusdam qui aperiam consequatur.', 'Voluptatem odio eos ut ut aliquam.', '/images/barkli/barkli-1-dekstop.png', '/images/villa/villa-dekstop-2.png', '2001-06-14 08:21:45', '1977-04-14 10:42:09', NULL),
(2, 'plotnikiv', 'ЖК \"ПЛОТНИКОВ\"', 'Maxime et et minus assumenda dolorum id debitis. Voluptate maiores quasi dolores ut soluta unde debitis harum. Velit nobis nesciunt temporibus ut enim. Voluptates ut voluptas maxime sapiente quos sapiente dolor et.', '9548 Oberbrunner Heights Suite 300', 'Sit corrupti numquam quam facere facilis commodi velit provident.', 'Voluptas unde dolores est dolorem quam possimus aut.', '/images/barkli/barkli-1-dekstop.png', '/images/villa/villa-dekstop-2.png', '1988-12-16 08:20:28', '2015-09-30 17:37:39', NULL),
(3, 'resident', 'ЖК \"РЕЗИДЕНЦИЯ НА ВСЕВОЛОЖСКОМ\"', 'Voluptas nihil aut perspiciatis delectus id. Iste occaecati quia nesciunt. Quo ullam sunt magni sint explicabo dignissimos quis. Esse natus culpa et.', '54598 Heathcote Park Apt. 979', 'Excepturi aut eos eum nihil.', 'Sapiente nihil modi pariatur hic.', '/images/barkli/barkli-1-dekstop.png', '/images/villa/villa-dekstop-2.png', '1971-10-08 04:20:28', '2009-04-28 02:16:34', NULL),
(4, 'trilogy', 'ЖК \"ТРИЛОГИЯ\"', 'Exercitationem id ut sed tempora ut et. Illum et nihil aliquam velit qui ut voluptas. Ratione non et quaerat quis maiores.', '345 Farrell Rapids Apt. 591', 'Est veniam est consequatur ea aut et.', 'Sapiente recusandae quos ipsum ex quis.', '/images/barkli/barkli-1-dekstop.png', '/images/villa/villa-dekstop-2.png', '2020-04-13 02:51:06', '2011-10-02 00:48:10', NULL),
(5, 'white', 'ЖК \"WHITE\"', 'Provident nesciunt labore et voluptatem veniam voluptatem illo. Omnis modi suscipit consequuntur tenetur necessitatibus aut. Quasi nisi aut consectetur molestiae quas.', '69871 White Trafficway', 'Molestiae libero deleniti qui consequatur quam non quos ad.', 'Eum sit rerum nulla quas odio cumque.', '/images/barkli/barkli-1-dekstop.png', '/images/villa/villa-dekstop-2.png', '1974-11-28 17:28:22', '2009-02-06 16:07:42', NULL),
(6, 'barkli', 'ЖК \"БАРКЛИ ГАЛЕРИ\"', 'Sed ornare risus sit amet neque convallis, non elementum odio venenatis. Donec felis elit, sodales et viverra nec, efficitur ac orci. In suscipit justo ac sapien dapibus, vitae iaculis purus accumsan. Sed nec purus mi. Ut elit metus, iaculis id accumsan ac, blandit at tortor. Suspendisse potenti. Nulla vel diam ac turpis egestas malesuada sed a erat. mi. Ut elit metus, iaculis id accumsan ac, blandit at tortor. Suspendisse potenti. Nulla vel diam ac turpis egestas malesuada sed a erat.', 'Москва, ул. Новый Арбат, 46', 'Сдача в 2020', 'Бассейн, парковая зона', '/images/barkli/barkli-1-dekstop.png', '/images/villa/villa-dekstop-2.png', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `complex_district`
--

CREATE TABLE `complex_district` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complex_id` bigint(20) UNSIGNED NOT NULL,
  `district_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `complex_district`
--

INSERT INTO `complex_district` (`id`, `complex_id`, `district_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 6, 2, NULL, NULL),
(3, 4, 1, NULL, NULL),
(4, 3, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `complex_location`
--

CREATE TABLE `complex_location` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complex_id` bigint(20) UNSIGNED NOT NULL,
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `complex_location`
--

INSERT INTO `complex_location` (`id`, `complex_id`, `location_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 4, NULL, NULL),
(3, 6, 2, NULL, NULL),
(4, 3, 4, NULL, NULL),
(5, 5, 6, NULL, NULL),
(6, 2, 4, NULL, NULL),
(7, 4, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `complex_metro`
--

CREATE TABLE `complex_metro` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complex_id` bigint(20) UNSIGNED NOT NULL,
  `metro_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `complex_metro`
--

INSERT INTO `complex_metro` (`id`, `complex_id`, `metro_id`, `created_at`, `updated_at`) VALUES
(1, 2, 10, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 1, 8, NULL, NULL),
(4, 3, 2, NULL, NULL),
(5, 4, 7, NULL, NULL),
(6, 6, 3, NULL, NULL),
(7, 5, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `complex_parking`
--

CREATE TABLE `complex_parking` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complex_id` bigint(20) UNSIGNED NOT NULL,
  `parking_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `complex_parking`
--

INSERT INTO `complex_parking` (`id`, `complex_id`, `parking_id`, `created_at`, `updated_at`) VALUES
(1, 6, 1, NULL, NULL),
(2, 5, 1, NULL, NULL),
(3, 4, 2, NULL, NULL),
(4, 2, 3, NULL, NULL),
(5, 1, 1, NULL, NULL),
(6, 3, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `districts`
--

CREATE TABLE `districts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `districts`
--

INSERT INTO `districts` (`id`, `district`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'nemo', '1982-07-18 20:54:33', '2019-12-08 15:32:26', NULL),
(2, 'occaecati', '2012-07-14 21:07:46', '1983-01-10 01:19:06', NULL),
(3, 'recusandae', '1983-11-13 02:39:01', '2001-10-09 13:04:56', NULL),
(4, 'quidem', '1993-02-14 16:22:16', '2016-03-22 17:00:16', NULL),
(5, 'rerum', '1996-04-05 20:25:01', '1998-01-15 21:22:17', NULL),
(6, 'eaque', '1971-06-24 18:25:12', '2006-08-16 15:00:55', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `emp_email`, `emp_phone`, `emp_desc`, `emp_img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hector', 'Will', 'nbins@example.net', '+1466182824044', 'Commodi mollitia numquam cum.', '/images/profile/profile.png', '1973-10-04 01:48:31', '2003-02-11 16:43:06', NULL),
(2, 'Ella', 'Dietrich', 'chris.will@example.net', '+4948465945555', 'Eos reiciendis aliquam velit sit sed id.', '/images/profile/profile.png', '1975-05-08 14:16:31', '1972-07-27 12:44:26', NULL),
(3, 'Armani', 'Cummings', 'austin.goyette@example.net', '+5881436264980', 'Ipsa beatae dolores tempora quibusdam dolor placeat aperiam.', '/images/profile/profile.png', '2004-12-02 15:44:27', '1983-10-27 05:06:35', NULL),
(4, 'Carmen', 'Mann', 'donato60@example.com', '+4374099600049', 'Unde non qui optio eligendi quasi velit est et.', '/images/profile/profile.png', '2005-01-03 00:30:05', '1980-08-13 23:45:20', NULL),
(5, 'Мария', 'Афанасенкова', 'rieltor@penthouse.moscow', '+7 495 120 11 00', 'Риэлтор, эксперт в области элитной коммерческой недвижимости', '/images/profile/profile.png', '2017-07-02 20:47:33', '2010-05-29 21:39:26', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `locations`
--

INSERT INTO `locations` (`id`, `location`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'fugit', '2005-12-21 00:09:20', '1981-10-03 15:01:52', NULL),
(2, 'aspernatur', '2002-05-05 09:22:07', '1985-06-10 01:03:13', NULL),
(3, 'provident', '1976-04-04 05:10:19', '2009-10-31 14:26:59', NULL),
(4, 'est', '1994-09-16 13:26:05', '1982-07-30 00:05:21', NULL),
(5, 'soluta', '1987-05-25 06:52:24', '1994-09-06 11:58:27', NULL),
(6, 'quae', '1973-03-03 22:33:50', '1982-12-27 21:52:42', NULL),
(7, 'atque', '1994-07-25 09:02:10', '1992-11-18 19:09:25', NULL),
(8, 'earum', '2010-06-19 01:37:11', '2013-07-31 13:04:10', NULL),
(9, 'excepturi', '1999-07-25 01:47:53', '1987-11-05 12:03:54', NULL),
(10, 'aliquam', '2008-10-20 04:12:08', '1972-09-20 06:57:56', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `metros`
--

CREATE TABLE `metros` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `metro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `metros`
--

INSERT INTO `metros` (`id`, `metro`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Арбатская', '1990-08-01 00:47:05', '1997-03-28 04:03:41', NULL),
(2, 'Новокузнецкая', '1995-05-05 22:18:11', '1993-11-13 17:30:29', NULL),
(3, 'illo', '1975-12-14 12:38:03', '1975-04-07 20:39:28', NULL),
(4, 'explicabo', '1971-04-03 19:15:16', '1991-07-23 06:17:52', NULL),
(5, 'culpa', '1982-11-16 05:08:35', '2012-07-24 13:24:30', NULL),
(6, 'unde', '1980-10-31 02:54:51', '1999-06-26 22:24:14', NULL),
(7, 'in', '1971-09-02 02:13:22', '1986-03-04 02:46:40', NULL),
(8, 'reiciendis', '2001-09-30 11:00:20', '1996-04-27 16:43:59', NULL),
(9, 'asperiores', '1975-04-25 13:42:18', '2017-06-19 22:43:11', NULL),
(10, 'necessitatibus', '1973-03-26 08:56:17', '1976-09-16 18:58:21', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_23_075710_create_classes', 1),
(5, '2021_01_23_075831_create_parkings', 1),
(6, '2021_01_23_075855_create_metros', 1),
(7, '2021_01_23_075919_create_districts', 1),
(8, '2021_01_23_075929_create_locations', 1),
(9, '2021_01_23_162236_create_articles_table', 1),
(10, '2021_01_23_165359_create_employees_table', 1),
(11, '2021_01_23_172115_create_apartments', 1),
(12, '2021_01_23_172138_create_tags_table', 1),
(13, '2021_01_23_172157_create_complexes_table', 1),
(14, '2021_01_23_172205_create_penthouses_table', 1),
(15, '2021_01_23_180917_create_penthouse_tag_table', 1),
(16, '2021_01_29_080133_create_complex_location', 1),
(17, '2021_01_29_080213_create_complex_district', 1),
(18, '2021_01_29_080646_create_complex_metro', 1),
(19, '2021_01_29_080859_create_complex_parking', 1),
(20, '2021_01_29_080919_create_class_complex', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `parkings`
--

CREATE TABLE `parkings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parking_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `parkings`
--

INSERT INTO `parkings` (`id`, `parking_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'amet', '2013-05-10 01:15:51', '2011-05-27 09:13:22', NULL),
(2, 'voluptas', '2018-08-07 18:31:24', '2009-05-15 10:50:38', NULL),
(3, 'sit', '1972-10-01 17:08:52', '1976-08-21 12:25:46', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `penthouses`
--

CREATE TABLE `penthouses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `penthouse_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penthouse_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penthouse_desc` text COLLATE utf8mb4_unicode_ci,
  `full_price_ru` int(10) UNSIGNED DEFAULT NULL,
  `full_price_usd` int(10) UNSIGNED DEFAULT NULL,
  `full_price_eur` int(10) UNSIGNED DEFAULT NULL,
  `square` double(8,2) UNSIGNED NOT NULL,
  `ceiling` double(8,2) UNSIGNED DEFAULT NULL,
  `floor` tinyint(3) UNSIGNED DEFAULT NULL,
  `rooms` tinyint(3) UNSIGNED DEFAULT NULL,
  `bedrooms` tinyint(3) UNSIGNED DEFAULT NULL,
  `bathrooms` tinyint(3) UNSIGNED DEFAULT NULL,
  `penthouse_id` bigint(20) UNSIGNED NOT NULL,
  `square_terrace` double(8,2) UNSIGNED DEFAULT NULL,
  `terrace` tinyint(1) NOT NULL DEFAULT '0',
  `concierge_service` tinyint(1) NOT NULL DEFAULT '0',
  `fireplace` tinyint(1) NOT NULL DEFAULT '0',
  `twostorey` tinyint(1) NOT NULL DEFAULT '0',
  `authors_design` tinyint(1) NOT NULL DEFAULT '0',
  `views` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penthouse_main_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penthouse_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penthouse_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `download` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complex_id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `apartment_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `penthouses`
--

INSERT INTO `penthouses` (`id`, `penthouse_slug`, `penthouse_title`, `penthouse_desc`, `full_price_ru`, `full_price_usd`, `full_price_eur`, `square`, `ceiling`, `floor`, `rooms`, `bedrooms`, `bathrooms`, `penthouse_id`, `square_terrace`, `terrace`, `concierge_service`, `fireplace`, `twostorey`, `authors_design`, `views`, `penthouse_main_img`, `penthouse_img`, `penthouse_plan`, `download`, `complex_id`, `employee_id`, `apartment_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Vel qui aut voluptatem.', 'Aperiam sit tempora rerum nobis ipsa ducimus hic reprehenderit.', 'Dolorum labore reiciendis eum commodi beatae necessitatibus aut. Sit facilis et enim et. Officiis quasi voluptates nihil qui.', 573, 79123128, 3847, 306.00, 2.30, 9, 5, 5, 4, 49, 51.10, 1, 0, 1, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 1, 5, 1, '2019-08-19 14:55:07', '2020-12-09 17:30:58', NULL),
(2, 'Ex est laborum voluptas reprehenderit.', 'Nihil fugit ad non repellat.', 'Aut odit suscipit voluptatem inventore quasi. Omnis assumenda unde eos adipisci non. Incidunt ut ut repellat facere harum. Velit debitis enim incidunt commodi autem unde.', 8, 675, 531542, 47.60, 2.60, 2, 8, 3, 9, 639, 45.30, 0, 0, 1, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 5, 3, 2, '1986-09-24 13:09:16', '2008-08-22 12:46:42', NULL),
(3, 'Ut asperiores ullam et culpa sit.', 'Consequatur voluptates est vitae deserunt perspiciatis repellendus.', 'Quia maxime est quo amet. Accusantium beatae asperiores occaecati voluptatem qui. Perspiciatis molestias excepturi et fugit ea.', 3, 36213130, 9951, 293.80, 2.80, 3, 6, 8, 4, 3, 111.00, 1, 1, 1, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 4, 5, 4, '1980-10-21 14:03:38', '2017-05-26 05:11:26', NULL),
(4, 'Perspiciatis facere mollitia necessitatibus consequatur veritatis.', 'Delectus autem modi fugiat ea est in.', 'Dolorem magni ut sed ut iusto ut et. Odio dolorum magnam quo officiis necessitatibus reprehenderit. Vel saepe aperiam architecto in dolor non.', 27837746, 67016776, 512390, 181.40, 2.60, 9, 3, 7, 1, 439, 36.70, 0, 1, 0, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 4, 4, 2, '1978-11-05 01:24:39', '1992-12-11 12:38:20', NULL),
(5, 'Sed eveniet eum dolore quibusdam est omnis.', 'Sed sunt aut quibusdam facere sit.', 'Assumenda sunt quis voluptatem ad et earum enim. Fugit sit suscipit et labore et soluta totam vitae.', 3828993, 7286371, 94, 139.60, 2.50, 5, 7, 0, 3, 4482, 15.60, 1, 0, 1, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 3, 4, 4, '1994-08-10 07:51:55', '2019-10-24 04:10:34', NULL),
(6, 'Placeat aliquid quo cupiditate debitis rerum blanditiis nemo amet.', 'Officia aliquam aut debitis iste.', 'Officiis eum ut fugiat qui debitis. Corrupti quo repellendus eius dolores. Optio in exercitationem non. Corrupti alias et repellendus totam numquam. Voluptatem repudiandae qui consequatur cum illo.', 7922, 71232, 9085888, 260.40, 2.10, 9, 0, 7, 1, 2, 64.20, 0, 0, 1, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 2, 1, 1, '1991-03-07 19:17:17', '1981-09-15 08:45:19', NULL),
(7, 'Tempore est quis deleniti et.', 'Sunt velit nemo non.', 'Sunt dignissimos quia aut laudantium quia quo. Qui vel id consequatur at molestias. In dolor facilis cum corrupti illo suscipit sed. Magni iste quas ut laborum.', 4092460, 1677, 417921, 243.00, 2.40, 6, 7, 7, 3, 759077120, 67.40, 1, 1, 0, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 1, 5, 3, '2001-03-03 15:01:46', '2012-02-11 02:31:25', NULL),
(8, 'Maiores alias id aliquid et ut dolore libero.', 'Minima quidem dolore repellat dicta et provident.', 'Et asperiores id quaerat quisquam. Pariatur rem quis est necessitatibus accusamus aliquam culpa. Aperiam sed unde ab distinctio inventore quia esse. Incidunt non provident totam quas.', 57334834, 153, 29, 236.90, 2.90, 9, 4, 6, 8, 4, 128.20, 0, 1, 0, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 4, 3, 4, '2002-05-12 00:52:02', '2009-10-30 08:11:45', NULL),
(9, 'Cupiditate maxime possimus et cumque nam dignissimos ut.', 'Nobis quam earum autem nemo itaque odit.', 'Exercitationem molestiae iusto odit totam dolore rerum. Consectetur harum consequatur nam optio. Fugit maxime nobis est quisquam impedit repellat consequatur quaerat.', 411206, 13026, 463773, 60.80, 2.70, 4, 6, 0, 8, 3992880, 99.30, 1, 1, 1, 0, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 3, 4, 4, '2012-07-02 19:19:09', '2005-07-09 09:59:40', NULL),
(10, 'Ut voluptatem quaerat dolorem similique explicabo nulla temporibus.', 'Fugit quod aut et consequuntur nesciunt sunt.', 'Odit modi laudantium vitae id molestiae corporis rerum. Ut explicabo laborum rerum qui assumenda nam. Et explicabo quia similique aut. Voluptatem minus officia cum et quia fugit ut rerum.', 99205, 613861046, 303201992, 190.20, 2.00, 6, 9, 3, 0, 688, 39.70, 1, 0, 0, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 5, 4, 4, '1990-08-22 01:42:34', '1970-11-10 05:51:20', NULL),
(11, 'Doloremque officiis voluptas aliquam natus fuga.', 'Corporis non quae quos ex et repellat.', 'Nesciunt sint fugiat velit quis et sapiente temporibus. Id perferendis facilis iure.', 6410, 521840630, 311821, 113.60, 2.10, 7, 0, 1, 4, 9, 128.10, 1, 1, 1, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 4, 1, 2, '2001-01-15 18:56:47', '1973-02-22 07:46:48', NULL),
(12, 'Occaecati explicabo perspiciatis saepe veniam accusantium omnis aliquid.', 'Magnam voluptas repellendus aliquid recusandae qui eum.', 'Neque modi et autem saepe. Et expedita et beatae labore a provident laborum. Eum perferendis necessitatibus tempora et quos.', 4988, 73, 16203, 334.40, 2.80, 7, 3, 3, 5, 887930, 22.40, 0, 1, 0, 0, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 5, 1, 2, '1998-02-26 03:52:07', '1998-06-22 05:42:58', NULL),
(13, 'Corporis facere laboriosam consequatur hic ut necessitatibus.', 'Officiis sint repellendus qui quia.', 'Delectus deserunt eveniet dolorem mollitia sit. Voluptate est commodi eligendi qui aut sapiente quo.', 6084888, 2227406, 1547, 238.80, 2.10, 5, 8, 6, 4, 335486024, 3.60, 0, 0, 0, 0, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 3, 5, 4, '1972-04-15 16:36:16', '1999-10-01 22:03:57', NULL),
(14, 'Ea velit voluptatem ab laboriosam ut.', 'Dolorum dolorum maiores et dolores dolorem ipsam eos.', 'Sed dolorem sequi aperiam nihil deleniti. Veritatis quisquam qui a ipsa ipsum et. Non adipisci voluptates vero. Labore ut architecto numquam non fuga voluptatem. Enim enim voluptatibus eveniet repudiandae quod.', 46163, 18366415, 71, 281.30, 2.60, 3, 2, 0, 7, 210832, 12.10, 0, 0, 1, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 1, 1, 2, '1976-12-25 20:09:31', '2014-08-31 01:22:31', NULL),
(15, 'Repellendus quidem quia voluptas.', 'Vitae ex et assumenda ipsa.', 'Nulla accusantium ea et officia esse error animi. At et repellat facilis laudantium fugit ut voluptas. Temporibus vitae impedit nihil at nostrum molestias adipisci. Ea vero sint ut numquam laboriosam.', 5165961, 665, 372787043, 271.90, 2.30, 4, 5, 1, 3, 16399, 97.40, 1, 0, 1, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 1, 3, 4, '2017-06-13 18:12:17', '2002-03-27 16:43:13', NULL),
(16, 'Tenetur corrupti deserunt id qui earum autem.', 'Quos nisi consequatur minus labore.', 'Quaerat qui voluptas consequatur cupiditate. Velit sunt aut aperiam enim ut sit. Eum a voluptatem aut quo et aut.', 7572890, 97, 450, 77.50, 2.90, 8, 3, 7, 7, 4322, 16.80, 0, 1, 1, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 4, 2, 2, '1986-01-27 23:51:32', '2003-09-13 21:12:38', NULL),
(17, 'Dolor neque et vero delectus illo quaerat.', 'Similique veniam eum quia inventore voluptatum repellendus.', 'Quae recusandae et eveniet modi. Voluptatem aut qui consequatur illo et voluptas voluptas. Soluta quos est ut maxime quis expedita. Doloremque totam autem rerum.', 87877000, 103802, 1, 313.70, 2.10, 9, 9, 1, 2, 7714923, 112.40, 1, 0, 0, 0, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 2, 2, 3, '1992-11-26 21:56:23', '2014-06-06 22:08:57', NULL),
(18, 'Nemo rerum sint voluptatem.', 'Earum et hic aspernatur.', 'Expedita soluta necessitatibus aliquam officiis est est. Cupiditate est iure minima ea repudiandae sit. Reprehenderit culpa voluptatem in illum sint sapiente omnis.', 40454726, 1, 460253070, 97.40, 2.10, 3, 8, 8, 3, 97, 117.00, 1, 1, 0, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 4, 1, 3, '1981-09-21 10:03:46', '1970-12-18 17:32:32', NULL),
(19, 'Quo deserunt aut corporis voluptates facere blanditiis pariatur.', 'Voluptas numquam minus et.', 'Sed harum eum non. Cum molestiae est quidem ut aut. Eum ea dolore non quis. Magnam ea mollitia quis accusamus voluptatem nihil.', 8910770, 21, 23907, 229.30, 2.80, 6, 1, 9, 1, 8, 134.20, 0, 0, 0, 1, 0, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 2, 3, 2, '2001-10-26 09:59:50', '2009-03-02 09:09:56', NULL),
(20, 'doyble-penthinbarkli', 'Двухуровневый пентхаус с открытой террасой в ЖК “БАРКЛИ ГАЛЕРИ”', 'Пентхаус на последнем этаже известного дома купца Филатова — элитный жилой дом, известный москвичам как «Дом с рюмкой», находится в начале улицы Остоженка. В квартире высокие потолки — 3,4 метра, удобная планировка: светлая гостиная с тремя окнами и видом на Кремль, кухня-столовая, хозяйская спальня с отдельной ванной комнатой, детская, спальня, вторая ванная комната. Из окон квартиры открывается великолепный вид на Кремль и Храм Христа Спасителя.\r\nЕсть возможность присоединения мансарды, дополнительные 164м².\r\nПентхаус на последнем этаже известного дома купца Филатова — элитный жилой дом, известный москвичам как «Дом с рюмкой», находится в начале улицы Остоженка.', 122000000, 122000000, 122000000, 425.00, 3.60, 45, 5, 4, 4, 734874647632, 120.00, 1, 1, 0, 1, 1, NULL, '/images/flat/flat-dekstop.jpg', '/images/mansion/mansion-1.png', '/images/plan/plan.png', NULL, 6, 5, 4, '1986-01-14 15:05:14', '2019-03-22 15:03:31', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `penthouse_tag`
--

CREATE TABLE `penthouse_tag` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `penthouse_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `penthouse_tag`
--

INSERT INTO `penthouse_tag` (`id`, `tag_id`, `penthouse_id`, `created_at`, `updated_at`) VALUES
(1, 1, 20, NULL, NULL),
(2, 3, 20, NULL, NULL),
(3, 4, 20, NULL, NULL),
(4, 3, 12, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `tag_slug`, `tag_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'quibusdam', 'architecto', '2012-10-24 09:08:38', '1997-09-19 14:54:51', NULL),
(2, 'consequuntur', 'quas', '1973-10-24 23:32:15', '1979-09-24 19:51:44', NULL),
(3, 'pariatur', 'veritatis', '2017-05-02 17:56:50', '1978-05-17 19:02:12', NULL),
(4, 'inventore', 'enim', '1973-11-18 00:42:18', '1998-03-26 23:04:50', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `apartments`
--
ALTER TABLE `apartments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `apartments_apartment_type_unique` (`apartment_type`);

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_article_title_unique` (`article_title`);

--
-- Индексы таблицы `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `classes_class_unique` (`class`);

--
-- Индексы таблицы `class_complex`
--
ALTER TABLE `class_complex`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_complex_complex_id_foreign` (`complex_id`),
  ADD KEY `class_complex_class_id_foreign` (`class_id`);

--
-- Индексы таблицы `complexes`
--
ALTER TABLE `complexes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `complexes_complex_slug_unique` (`complex_slug`),
  ADD KEY `complexes_complex_title_index` (`complex_title`);

--
-- Индексы таблицы `complex_district`
--
ALTER TABLE `complex_district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complex_district_complex_id_foreign` (`complex_id`),
  ADD KEY `complex_district_district_id_foreign` (`district_id`);

--
-- Индексы таблицы `complex_location`
--
ALTER TABLE `complex_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complex_location_complex_id_foreign` (`complex_id`),
  ADD KEY `complex_location_location_id_foreign` (`location_id`);

--
-- Индексы таблицы `complex_metro`
--
ALTER TABLE `complex_metro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complex_metro_complex_id_foreign` (`complex_id`),
  ADD KEY `complex_metro_metro_id_foreign` (`metro_id`);

--
-- Индексы таблицы `complex_parking`
--
ALTER TABLE `complex_parking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complex_parking_complex_id_foreign` (`complex_id`),
  ADD KEY `complex_parking_parking_id_foreign` (`parking_id`);

--
-- Индексы таблицы `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `districts_district_unique` (`district`);

--
-- Индексы таблицы `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_emp_email_unique` (`emp_email`),
  ADD UNIQUE KEY `employees_emp_phone_unique` (`emp_phone`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `locations_location_unique` (`location`);

--
-- Индексы таблицы `metros`
--
ALTER TABLE `metros`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `metros_metro_unique` (`metro`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `parkings`
--
ALTER TABLE `parkings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `parkings_parking_type_unique` (`parking_type`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `penthouses`
--
ALTER TABLE `penthouses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `penthouses_penthouse_slug_unique` (`penthouse_slug`),
  ADD UNIQUE KEY `penthouses_penthouse_id_unique` (`penthouse_id`),
  ADD KEY `penthouses_complex_id_foreign` (`complex_id`),
  ADD KEY `penthouses_employee_id_foreign` (`employee_id`),
  ADD KEY `penthouses_apartment_id_foreign` (`apartment_id`);

--
-- Индексы таблицы `penthouse_tag`
--
ALTER TABLE `penthouse_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penthouse_tag_tag_id_foreign` (`tag_id`),
  ADD KEY `penthouse_tag_penthouse_id_foreign` (`penthouse_id`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_tag_slug_unique` (`tag_slug`),
  ADD UNIQUE KEY `tags_tag_name_unique` (`tag_name`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `apartments`
--
ALTER TABLE `apartments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `classes`
--
ALTER TABLE `classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `class_complex`
--
ALTER TABLE `class_complex`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `complexes`
--
ALTER TABLE `complexes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `complex_district`
--
ALTER TABLE `complex_district`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `complex_location`
--
ALTER TABLE `complex_location`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `complex_metro`
--
ALTER TABLE `complex_metro`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `complex_parking`
--
ALTER TABLE `complex_parking`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `districts`
--
ALTER TABLE `districts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `metros`
--
ALTER TABLE `metros`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `parkings`
--
ALTER TABLE `parkings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `penthouses`
--
ALTER TABLE `penthouses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `penthouse_tag`
--
ALTER TABLE `penthouse_tag`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `class_complex`
--
ALTER TABLE `class_complex`
  ADD CONSTRAINT `class_complex_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `class_complex_complex_id_foreign` FOREIGN KEY (`complex_id`) REFERENCES `complexes` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `complex_district`
--
ALTER TABLE `complex_district`
  ADD CONSTRAINT `complex_district_complex_id_foreign` FOREIGN KEY (`complex_id`) REFERENCES `complexes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `complex_district_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `complex_location`
--
ALTER TABLE `complex_location`
  ADD CONSTRAINT `complex_location_complex_id_foreign` FOREIGN KEY (`complex_id`) REFERENCES `complexes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `complex_location_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `complex_metro`
--
ALTER TABLE `complex_metro`
  ADD CONSTRAINT `complex_metro_complex_id_foreign` FOREIGN KEY (`complex_id`) REFERENCES `complexes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `complex_metro_metro_id_foreign` FOREIGN KEY (`metro_id`) REFERENCES `metros` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `complex_parking`
--
ALTER TABLE `complex_parking`
  ADD CONSTRAINT `complex_parking_complex_id_foreign` FOREIGN KEY (`complex_id`) REFERENCES `complexes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `complex_parking_parking_id_foreign` FOREIGN KEY (`parking_id`) REFERENCES `parkings` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `penthouses`
--
ALTER TABLE `penthouses`
  ADD CONSTRAINT `penthouses_apartment_id_foreign` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`id`),
  ADD CONSTRAINT `penthouses_complex_id_foreign` FOREIGN KEY (`complex_id`) REFERENCES `complexes` (`id`),
  ADD CONSTRAINT `penthouses_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`);

--
-- Ограничения внешнего ключа таблицы `penthouse_tag`
--
ALTER TABLE `penthouse_tag`
  ADD CONSTRAINT `penthouse_tag_penthouse_id_foreign` FOREIGN KEY (`penthouse_id`) REFERENCES `penthouses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `penthouse_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
