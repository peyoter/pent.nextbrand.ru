<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController as IndexController;
use App\Http\Controllers\SearchController as SearchController;
use App\Http\Controllers\ArticleController as ArticleController;
use App\Http\Controllers\Catalog\CatalogController as CatalogController;
use App\Http\Controllers\Catalog\ComplexController as ComplexController;
use App\Http\Controllers\Catalog\PenthouseController as PenthouseController;
use App\Http\Controllers\Forms\FormController as FormController;
use App\Http\Middleware\VerifyCsrfToken as VerifyCsrfToken;
use App\Http\Controllers\Catalog\PdfController as PdfController;

Auth::routes();

Route::get('/', [IndexController::class, 'index'])->name('index');

Route::get('/article', [ArticleController::class, 'index'])->name('article');

Route::prefix('catalog')->name('catalog')->group(function (){
    Route::get('/', [CatalogController::class, 'index']);
    Route::post('/filterAjax', [CatalogController::class,'filterAjax'])->withoutMiddleware([VerifyCsrfToken::class])->name('.filter.ajax');
    Route::get('/{complex}', [ComplexController::class, 'index'])->name('.complex');
    Route::get('/{complex}/{penthouse}', [PenthouseController::class, 'index'])->name('.penthouse');
});

Route::post('/feedback', [FormController::class,'feedback'])->name('forms.feedback');
Route::get('/search', [SearchController::class, 'index'])->name('search');

Route::prefix('pdf')->name('pdf')->group(function (){
    Route::get('/', [PdfController::class, 'index']);
    Route::get('/{complex}', [PdfController::class, 'complex'])->name('.complex');
    Route::get('/{complex}/{penthouse}', [PdfController::class, 'penthouse'])->name('.penthouse');
});

Route::get('/image', [PdfController::class, 'image'])->name('image');
