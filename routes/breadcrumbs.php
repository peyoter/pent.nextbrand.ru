<?php

// index
Breadcrumbs::for('index', function ($trail) {
    $trail->push('Главная', route('index'));
});

// index > article
Breadcrumbs::for('article', function ($trail) {
    $trail->parent('index');
    $trail->push('Что такое пентхаус', route('article'));
});

// index > catalog
Breadcrumbs::for('catalog', function ($trail) {
    $trail->parent('index');
    $trail->push('Каталог пентхаусов', route('catalog'));
});

// index > catalog > [complex]
Breadcrumbs::for('catalog.complex', function ($trail, $complex) {
    $trail->parent('catalog');
    $trail->push($complex->complex_title, route('catalog.complex', $complex->complex_slug));
});

// index > catalog > [complex] > [penthouse]
Breadcrumbs::for('catalog.penthouse', function ($trail, $penthouse) {
    $trail->parent('catalog.complex', $penthouse->complex);
    $trail->push($penthouse->penthouse_title, route('catalog.penthouse', ['complex' => $penthouse->complex->complex_slug,
        'penthouse' => $penthouse->penthouse_slug]));
});
