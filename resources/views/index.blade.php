@extends('layouts.layout')
@section('page-title', 'Penthouse Moscow')
@section('content')
    <section class="penthouse swiper-container">
        <button class="swiper-button-prev"></button>
        <button class="swiper-button-next"></button>
        <div class="swiper-wrapper">
            @foreach($randomPenthouses as $randomPenthouse)
            <a class="penthouse__item swiper-slide" href="{{ route('catalog.penthouse',[$randomPenthouse->complex_slug, $randomPenthouse->penthouse_slug]) }}">
                <div class="penthouse__img">
                    <img src="{{ asset($randomPenthouse->fit($randomPenthouse->penthouse_main_img, 300,354)) }}" alt="penthouse-1" width="282" height="334">
                </div>
                <div class="penthouse__info">
                    <h2 class="penthouse__name">{{ $randomPenthouse->penthouse_title }}</h2>
                    <p class="penthouse__type">{{ $randomPenthouse->square }} м<sup>2</sup>
                        @if($randomPenthouse->terrace == true)
                            + терраса {{ $randomPenthouse->square_terrace }} м<sup>2</sup></p>
                        @endif
                    <p class="penthouse__price">{{ $randomPenthouse->full_price_ru }} &#8381;</p>
                </div>
            </a>
            @endforeach
        </div>
    </section>

    @include('layouts.modules.features')

    <section class="house sec">
        <div class="house__inner">
            <header class="house__header">
                <h2 class="house__chapter">Пентхаусы с террасой</h2>
                <a class="house__all" href="{{ route('catalog', 'terraceBool=1') }}">Показать все предложения</a>
            </header>
            <div class="house__list swiper-container">
                <button class="swiper-button-prev"></button>
                <button class="swiper-button-next"></button>
                <div class="swiper-wrapper">
                    @foreach($terracePenthouses as $terracePenthouse)
                    <a class="house__item swiper-slide" href="{{ route('catalog.penthouse',[$terracePenthouse->complex_slug, $terracePenthouse->penthouse_slug]) }}">
                        <div class="house__img">
                            <img src="{{ asset($terracePenthouse->fit($terracePenthouse->penthouse_main_img, 370,293)) }}" alt="terrace-1" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">{{ $terracePenthouse->penthouse_title }}</h3>
                            <div class="house__about">
                                <p class="house__area">{{ $terracePenthouse->square }} м<sup>2</sup>
                                    @if($terracePenthouse->terrace == true)
                                        + терраса {{ $terracePenthouse->square_terrace }} м<sup>2</sup></p>
                                    @endif
                                <p class="house__price">{{ $terracePenthouse->full_price_ru }} &#8381;</p>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @include('layouts.modules.catalog')

    <section class="house sec fireplace">
        <div class="house__inner">
            <header class="house__header">
                <h2 class="house__chapter">Пентхаусы с камином</h2>
                <a class="house__all" href="{{ route('catalog', 'fireplace=1') }}">Показать все предложения</a>
            </header>
            <div class="house__list swiper-container">
                <button class="swiper-button-prev"></button>
                <button class="swiper-button-next"></button>
                <div class="swiper-wrapper">
                    @foreach($fireplacePenthouses as $fireplacePenthouse)
                    <a class="house__item swiper-slide" href="{{ route('catalog.penthouse',[$fireplacePenthouse->complex_slug, $fireplacePenthouse->penthouse_slug]) }}">
                        <div class="house__img">
                            <img src="{{ asset($fireplacePenthouse->fit($fireplacePenthouse->penthouse_main_img, 370,293)) }}" alt="fireplace-1" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">{{ $fireplacePenthouse->penthouse_title }}</h3>
                            <div class="house__about">
                                <p class="house__area">{{ $fireplacePenthouse->square }} м<sup>2</sup>
                                    @if($fireplacePenthouse->terrace == true)
                                        + терраса {{ $fireplacePenthouse->square_terrace }} м<sup>2</sup></p>
                                    @endif
                                <p class="house__price">{{ $fireplacePenthouse->full_price_ru }} &#8381;</p>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="explain sec">
        <div class="container">
            <div class="explain__inner">
                <div class="explain__info text">
                    <h2 class="explain__title">Что такое пентхаус?</h2>
                    <p>Есть две версии того, как возникли дома пентхаусы. Первая связана с военными действиями Англии в эпоху абсолютизма (16 век) и объясняет жилые помещения на крышах городских домов дефицитом площади для размещения солдат.</p>
                    <p>Вторая значительно романтичнее и отсылает к любителям закрытых, но роскошных пати. Такие вечеринки устраивали на крышах домов представители городской богемы Нью-Йорка в 20 годы прошлого столетия. Специально для удобства и разнообразия развлечений и возникло эксклюзивное жилье.</p>
                    <p class="attention">Имеет значение также имя дизайнера, который <br> оформлял интерьер пентхауса, этажность здания. </p>
                </div>
                <div class="explain__present">
                    <div class="explain__img"><img src="{{ asset('images/explain/explain.png') }}" alt="explain" width="300" height="104"></div>
                    <a class="explain__link" href="#">Подробнее о нас</a>
                </div>
            </div>
        </div>
    </section>

    <section class="house sec twostorey">
        <div class="house__inner">
            <header class="house__header">
                <h2 class="house__chapter">Двухэтажные пентхаусы</h2>
                <a class="house__all" href="{{ route('catalog', 'twostorey=1') }}">Показать все предложения</a>
            </header>
            <div class="house__list swiper-container">
                <button class="swiper-button-prev"></button>
                <button class="swiper-button-next"></button>
                <div class="swiper-wrapper">
                    @foreach($twostoreyPenthouses as $twostoreyPenthouse)
                    <a class="house__item swiper-slide" href="{{ route('catalog.penthouse',[$twostoreyPenthouse->complex_slug, $twostoreyPenthouse->penthouse_slug]) }}">
                        <div class="house__img">
                            <img src="{{ asset($twostoreyPenthouse->fit($twostoreyPenthouse->penthouse_main_img, 370,293)) }}" alt="fireplace-1" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">{{ $twostoreyPenthouse->penthouse_title }}</h3>
                            <div class="house__about">
                                <p class="house__area">{{ $twostoreyPenthouse->square }} м<sup>2</sup>
                                    @if($twostoreyPenthouse->terrace == true)
                                        + терраса {{ $twostoreyPenthouse->square_terrace }} м<sup>2</sup></p>
                                    @endif
                                <p class="house__price">{{ $twostoreyPenthouse->full_price_ru }} &#8381;</p>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
