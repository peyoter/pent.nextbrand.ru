<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Спасибо за заявку</title>
    <link rel="stylesheet" href="css/search/style.min.css">
</head>
<body>

<div class="wrapper">
    <header class="header">
        <h1 class="visually-hidden">Пентхаусы Москва</h1>
        <div class="header__inner">
            <div class="nav__burger nav__burger-tel">
                <a class="nav__toggle" href="#">
                    <span class="nav__toggle-icon"></span>
                </a>
            </div>
            <a class="header__logo" href="{{ route('index') }}">
                <span class="header__logo-penthouse">Penthouse</span>
                <span>.moscow</span>
            </a>
            <div class="header__manage">
                <form class="header__form" action="{{ route('search') }}" method="GET">
                    <label class="header__search">
                        <span class="header__search-icon"><img src="{{ asset('images/icons/search.svg') }}" alt="search" width="17" height="17"></span>
                        <span class="header__search-inner">
								<input class="header__search-input" type="text" name="search" placeholder="Найти пентхаус">
								<span class="header__search-delete">
									<span class="visually-hidden">Очистить кнопку поиска</span>
									<img src="{{ asset('images/icons/header__search-delete.svg') }}" alt="очистить форму поиска" width="20" height="14">
								</span>
								<span class="header__search-toggle"><span class="visually-hidden">Закрыть форму поиска</span></span>
                                <button type="submit">поиск</button>
							</span>
                    </label>
                </form>
                <a href="tel:+74951201100" class="header__tel tel">+7 495 120-11-00</a>
            </div>
        </div>
    </header>
    <main>
        <div class="nav__wrapper">
            <div class="nav__burger nav__burger-dekstop">
                <a class="nav__toggle" href="#">
                    <span class="nav__toggle-icon"></span>
                </a>
            </div>
            <nav class="nav">
                <div class="nav__inner">
                    <ul class="nav__list">
                        <li><a href="{{ route('index') }}">Главная</a></li>
                        <li><a href="{{ route('article') }}">Что такое пентхаус</a></li>
                        <li><a href="{{ route('catalog') }}">Каталог пентхаусов</a></li>
                        <li><a href="#">Покупателям</a></li>
                        <li><a href="#">Собственникам</a></li>
                        <li><a href="#">Застройщикам</a></li>
                        <li><a href="#">Партнерам</a></li>
                        <li><a href="#">О компании</a></li>
                        <li><a href="#">Услуги</a></li>
                        <li><a href="#">Контакты</a></li>
                    </ul>
                    <a class="nav__catalog" href="#">Скачать каталог</a>
                    <div class="nav__contact">
                        <a class="nav__tel" href="tel:+74951201100">+7 495 120-11-00</a>
                        <a class="nav__order" href="#">Заказать звонок</a>
                    </div>
                </div>
            </nav>
        </div>

        <div class="search">
            <div class="search__thanks">
                <p class="search__thanks-text">Спасибо за вашу заявку!</p>
                <p class="search__thanks-descr">Мы свяжемся с вами в ближайшее время.</p>
                <div class="search__to-index"><a href="{{ route('index') }}">Вернуться на главную страницу</a></div>
            </div>
        </div>

        @include('layouts.modules.features')
    </main>

    <footer class="footer">
        <nav class="footer__nav">
            <ul class="footer__list">
                <li><a href="{{ route('index') }}">Главная</a></li>
                <li><a href="{{ route('article') }}">Что такое пентхаус</a></li>
                <li><a href="{{ route('catalog') }}">Каталог</a></li>
                <li><a href="#">Покупателям</a></li>
                <li><a href="#">Собственникам</a></li>
                <li><a href="#">Застройщикам</a></li>
                <li><a href="#">Партнерам</a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#">Услуги</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </nav>
        <ul class="footer__menu">
            <li><a href="{{ route('catalog') }}">Пентхаусы с терассами</a></li>
            <li><a href="{{ route('catalog') }}">Пентхаусы для многодетных</a></li>
            <li><a href="{{ route('catalog') }}">Пентхаусы для тех к кого есть животные</a></li>
        </ul>
        <p class="footer__slogan">Быть особенным - быть собой.</p>
        <div class="footer__tel"><a href="tel:+74951201100">+7 495 120 11 00</a></div>
        <div class="footer__info">
            <div class="footer__logo"><a href="{{ route('index') }}"><img src="{{ asset('images/icons/logo.svg')}}" alt="Логотип компании"></a></div>
            <p class="footer__copy">2020 © Агентство элитной недвижимости «Ашихмин и партнёры»</p>
        </div>
        <div class="phone__wrapper">
            <a class="phone" href="tel:+74951201100"><img src="{{ asset('images/icons/tel.svg')}}" alt="Позвонить в компанию" width="23" height="23"></a>
            <div class="phone__modal modal">
                <button class="phone__modal-close">
                    <span class="visually-hidden">Закрыть модальное окно</span>
                </button>
                <a class="phone__modal-phone phone__modal-link" href="tel:+74951201100">Позвонить нам +7 495 120 11 10</a>
                <a class="phone__modal-mail phone__modal-link" href="mailto:ga@aandp.ru">Написать на почту</a>
                <a class="phone__modal-whatsapp phone__modal-link" href="whatsapp://send?text=Hello&phone=+74951201100&abid=+74951201100">Написать в WhatsApp</a>
            </div>
        </div>
    </footer>
    <div class="overlay"></div>
    <div class="overlay-modal"></div>
</div>

<script src="js/search/script.min.js"></script>

</body>
</html>
