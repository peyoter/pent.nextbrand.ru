@include('layouts.modules.header')
@yield('breadcrumbs')
@yield('content')


@include('layouts.modules.footer')

@include('layouts.modules.mansion-swiper')

</div>

<script src="{{ asset("js/$link/script.min.js") }}"></script>
@yield('script')

</body>
</html>
