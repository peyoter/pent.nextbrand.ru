@section('footer')
    <section class="pro sec">
        <div class="container">
            <div class="pro__inner">
                <div class="pro__profile">
                    <div class="pro__img"><img src="{{ asset('images/pro/ГригорийАшихмин.png')}}" alt="Григорий Ашихмин" width="145" height="188"></div>
                    <div class="pro__info">
                        <p class="pro__name"><span>Ашихмин</span> Григорий</p>
                        <div class="pro__contact">
                            <a class="pro__tel" href="tel:+74951201100">+7 495 120 11 00</a>
                            <p class="pro__email">E-mail: <a href="mailto:ga@aandp.ru">ga@aandp.ru</a></p>
                        </div>
                    </div>
                </div>
                <div class="pro__about">
                    <h2 class="pro__title">Обратитесь к профессионалам</h2>
                    <div class="pro__text text">
                        <p>Несмотря на то, что в настоящее время слово «пентхаус» обозначает роскошные и дорогие апартаменты, откуда открывается панорамный вид, исторически пентхаус был всего лишь пристройкой с односкатной крышей, навесом или другим небольшим строением, которое пристраивалось к сравнительно большому зданию. </p>
                        <p>В средневековые времена пентхаус являлся важным элементом искусства осады, поскольку был временным сооружением, которое защищало осаждающие войска в то время, когда они готовились к наступлению на врага.</p>
                        <p class="attention">Ищете пентхаус в Москве? Мы поможем!</p>
                    </div>
                    <a class="pro__link form-toggle" href="#">Заказать подбор пентхауса</a>
                </div>
            </div>
        </div>
    </section>
    </main>
    <footer class="footer">
        <nav class="footer__nav">
            <ul class="footer__list">
                <li><a href="{{ route('index') }}">Главная</a></li>
                <li><a href="{{ route('article') }}">Что такое пентхаус</a></li>
                <li><a href="{{ route('catalog') }}">Каталог</a></li>
                <li><a href="#">Покупателям</a></li>
                <li><a href="#">Собственникам</a></li>
                <li><a href="#">Застройщикам</a></li>
                <li><a href="#">Партнерам</a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#">Услуги</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </nav>
        <ul class="footer__menu">
            <li><a href="{{ route('catalog') }}">Пентхаусы с терассами</a></li>
            <li><a href="{{ route('catalog') }}">Пентхаусы для многодетных</a></li>
            <li><a href="{{ route('catalog') }}">Пентхаусы для тех к кого есть животные</a></li>
        </ul>
        <p class="footer__slogan">Быть особенным - быть собой.</p>
        <div class="footer__tel"><a href="tel:+74951201100">+7 495 120 11 00</a></div>
        <div class="footer__info">
            <div class="footer__logo"><a href="{{ route('index') }}"><img src="{{ asset('images/icons/logo.svg')}}" alt="Логотип компании"></a></div>
            <p class="footer__copy">2020 © Агентство элитной недвижимости «Ашихмин и партнёры»</p>
        </div>
        <div class="phone__wrapper">
            <a class="phone" href="tel:+74951201100"><img src="{{ asset('images/icons/tel.svg')}}" alt="Позвонить в компанию" width="23" height="23"></a>
            <div class="phone__modal modal">
                <button class="phone__modal-close">
                    <span class="visually-hidden">Закрыть модальное окно</span>
                </button>
                <a class="phone__modal-phone phone__modal-link" href="tel:+74951201100">Позвонить нам +7 495 120 11 10</a>
                <a class="phone__modal-mail phone__modal-link" href="mailto:ga@aandp.ru">Написать на почту</a>
                <a class="phone__modal-whatsapp phone__modal-link" href="whatsapp://send?text=Hello&phone=+74951201100&abid=+74951201100">Написать в WhatsApp</a>
            </div>
        </div>
    </footer>
    <form class="main-form main-form--modal" action="{{ route('forms.feedback') }}" method="POST">
        @csrf
        @if ($errors->any())
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <button class="main-form__close"><span class="visually-hidden">Закрыть модальное окно</span></button>
        <div class="main-form--modal__wrapper">
            <h2 class="main-form__title main-form__title--modal">Заявка на осмотр</h2>
            <div class="main-form__inner">
                <input class="main-form__input" name="name" type="text" placeholder="Ваше имя">
                <input class="main-form__input" name="phone" type="tel" placeholder="Номер телефона">
                <input class="main-form__input" name="email" type="email" placeholder="E-mail">
                <textarea class="main-form__textarea" name="textarea" cols="30" rows="10" placeholder="Комментарий к заявке. Здесь вы можете указать желаемое время звонки или другую важную информацию."></textarea>
                <button class="main-form__btn" type="submit">Отправить</button>
                <div class="main-form__agree">
                    <label class="main-form__agree-label">
                        <input type="checkbox" name="check">
                        <span class="main-form__agree-check"></span>
                        <span class="main-form__agree-text">Соглашаюсь с политикой конфиденциальности.</span>
                    </label>
                </div>
            </div>
        </div>
    </form>
    <div class="overlay"></div>
    <div class="overlay-modal"></div>
@show
