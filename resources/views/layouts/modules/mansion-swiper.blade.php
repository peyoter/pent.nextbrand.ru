@section('mansion-swiper')
    <div class="mansion-swiper__wrapper">
        <div class="mansion-swiper swiper-container">
            <button class="mansion-swiper__close"><span class="visually-hidden">Закрыть слайдер</span></button>
            <h2 class="mansion-swiper__title">Пентхаус ЖК "Баркли Галери"</h2>
            <button class="swiper-button-prev"></button>
            <button class="swiper-button-next"></button>
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img class="mansion-swiper__tel" src="{{asset('images/mansion/mansion-swiper-1.png')}}" alt="пентхаус">
                    <img class="mansion-swiper__dekstop" src="{{asset('images/mansion/mansion-swiper-desktop.png')}}" alt="пентхаус">
                </div>
                <div class="swiper-slide">
                    <img class="mansion-swiper__tel" src="{{asset('images/mansion/mansion-swiper-2.png')}}" alt="пентхаус">
                    <img class="mansion-swiper__dekstop" src="{{asset('images/mansion/mansion-swiper-desktop.png')}}" alt="пентхаус">
                </div>
                <div class="swiper-slide">
                    <img class="mansion-swiper__tel" src="{{asset('images/mansion/mansion-swiper-3.png')}}" alt="пентхаус">
                    <img class="mansion-swiper__dekstop" src="{{asset('images/mansion/mansion-swiper-desktop.png')}}" alt="пентхаус">
                </div>
                <div class="swiper-slide">
                    <img class="mansion-swiper__tel" src="{{asset('images/mansion/mansion-swiper-4.png')}}" alt="пентхаус">
                    <img class="mansion-swiper__dekstop" src="{{asset('images/mansion/mansion-swiper-desktop.png')}}" alt="пентхаус">
                </div>
                <div class="swiper-slide">
                    <img class="mansion-swiper__tel" src="{{asset('images/mansion/mansion-swiper-1.png')}}" alt="пентхаус">
                    <img class="mansion-swiper__dekstop" src="{{asset('images/mansion/mansion-swiper-desktop.png')}}" alt="пентхаус">
                </div>
                <div class="swiper-slide">
                    <img class="mansion-swiper__tel" src="{{asset('images/mansion/mansion-swiper-2.png')}}" alt="пентхаус">
                    <img class="mansion-swiper__dekstop" src="{{asset('images/mansion/mansion-swiper-desktop.png')}}" alt="пентхаус">
                </div>
                <div class="swiper-slide">
                    <img class="mansion-swiper__tel" src="{{asset('images/mansion/mansion-swiper-3.png')}}" alt="пентхаус">
                    <img class="mansion-swiper__dekstop" src="{{asset('images/mansion/mansion-swiper-desktop.png')}}" alt="пентхаус">
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
@show
