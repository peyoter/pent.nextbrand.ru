<section class="features sec">
    <ul class="features__list">
        <li>
            <h2 class="features__title">Сэкономим время</h2>
            <p>Найдём лучшие варианты и обо всём договоримся за вас</p>
        </li>
        <li>
            <h2 class="features__title">Поможем разобраться</h2>
            <p>Экспертиза, знание рынка, внимание к клиенту</p>
        </li>
        <li>
            <h2 class="features__title">Эксклюзивные предложения</h2>
            <p>Клубные дома и закрытые продажи</p>
        </li>
        <li>
            <h2 class="features__title">Лучшие условия</h2>
            <p>Договоримся о скидке, добьёмся наилучших условий</p>
        </li>
    </ul>
</section>
