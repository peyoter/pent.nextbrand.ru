@include('layouts.modules.header')

        @yield('breadcrumbs')

        @yield('content')

@include('layouts.modules.footer')
</div>

<script src="{{ asset("js/$link/script.min.js") }}"></script>
<script>
    $(".main-form__btn").click(function(e){
        e.preventDefault();
        let form = document.querySelector('.main-form__btn');
        let data = new FormData(form);

        $.ajax({
            type:'POST',
            url:'/contact',
            data:data,
            success:function(data){
                alert(data.success);
            }
        });
    });
</script>
</body>
</html>
