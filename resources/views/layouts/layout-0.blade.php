@include('layouts.modules.header')
    @yield('content')
    <footer class="footer">
        <nav class="footer__nav">
            <ul class="footer__list">
                <li><a href="{{ route('index') }}">Главная</a></li>
                <li><a href="{{ route('article') }}">Что такое пентхаус</a></li>
                <li><a href="{{ route('catalog') }}">Каталог</a></li>
                <li><a href="#">Покупателям</a></li>
                <li><a href="#">Собственникам</a></li>
                <li><a href="#">Застройщикам</a></li>
                <li><a href="#">Партнерам</a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#">Услуги</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </nav>
        <ul class="footer__menu">
            <li><a href="{{ route('catalog') }}">Пентхаусы с терассами</a></li>
            <li><a href="{{ route('catalog') }}">Пентхаусы для многодетных</a></li>
            <li><a href="{{ route('catalog') }}">Пентхаусы для тех к кого есть животные</a></li>
        </ul>
        <p class="footer__slogan">Быть особенным - быть собой.</p>
        <div class="footer__tel"><a href="tel:+74951201100">+7 495 120 11 00</a></div>
        <div class="footer__info">
            <div class="footer__logo"><a href="{{ route('index') }}"><img src="{{ asset('images/icons/logo.svg')}}" alt="Логотип компании"></a></div>
            <p class="footer__copy">2020 © Агентство элитной недвижимости «Ашихмин и партнёры»</p>
        </div>
        <div class="phone__wrapper">
            <a class="phone" href="tel:+74951201100"><img src="{{ asset('images/icons/tel.svg')}}" alt="Позвонить в компанию" width="23" height="23"></a>
            <div class="phone__modal modal">
                <button class="phone__modal-close">
                    <span class="visually-hidden">Закрыть модальное окно</span>
                </button>
                <a class="phone__modal-phone phone__modal-link" href="tel:+74951201100">Позвонить нам +7 495 120 11 10</a>
                <a class="phone__modal-mail phone__modal-link" href="mailto:ga@aandp.ru">Написать на почту</a>
                <a class="phone__modal-whatsapp phone__modal-link" href="whatsapp://send?text=Hello&phone=+74951201100&abid=+74951201100">Написать в WhatsApp</a>
            </div>
        </div>
    </footer>
    <div class="overlay"></div>
    <div class="overlay-modal"></div>
</div>

<script src="{{ asset("js/$link/script.min.js") }}"></script>

</body>
</html>
