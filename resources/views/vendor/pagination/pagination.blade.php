@if ($paginator->hasPages())
<a href="#" class="mansion__offer">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
    <p class="mansion__offer-show">Показать еще 10 предложений</p>
    <p class="mansion__offer-num">Показано <span>7</span> из <span>43</span></p>
</a>
@endif
