@extends('layouts.layout-0')
@section('page-title', 'Searching')
@section('content')
<div class="search">
    <div class="search__result">
        <p class="search__result-about">Результаты поиска по запросу <span class="search__result-word">“{{ $request->search }}”:</span></p>
        @if(count($searching['searchComplex']) > 0 || count($searching['searchPenthouse']))
            <div class="header__form-results search__mansion">
                <div class="header__form-results__type">
                    <span class="header__form-results__name">- Жилые комплексы:</span>
                    <ul class="header__form-results__list">
                    @foreach($searching['searchComplex'] as $searchComp)
                        <li class="header__form-results__about"><a href="{{ route('catalog.complex', $searchComp->complex_slug) }}">
                            {!! $searchComp->complex_title  !!}</a>
                        </li>
                    @endforeach
                    </ul>
                </div>
                <div class="header__form-results__type">
                    <span class="header__form-results__name">- Пентхаусы:</span>
                    <ul class="header__form-results__list">
                    @foreach($searching['searchPenthouse'] as $searchPent)
                        <li class="header__form-results__about"><a href="{{ route('catalog.penthouse',[$searchPent->complex_slug, $searchPent->penthouse_slug]) }}">
                            {!! $searchPent->penthouse_title !!}</a>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        @else
            <p class="search__result-object">Объектов найдено: <span class="search__result-object__num">0</span></p>
        @endif
    </div>
</div>
@endsection
