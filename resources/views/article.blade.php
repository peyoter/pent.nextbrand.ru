@extends('layouts.layout')
@section('page-title', $article->title)
@section('breadcrumbs', Breadcrumbs::render('article'))

@section('content')

    <div class="article">
        <h1 class="main-title article__main-title">{{ $article->title }}</h1>
        <div class="container">
            <div class="article__inner">
                <article class="article__explain">
                    <div class="article__about">
                        <div class="article__descr">
                            <h2 class="article__title">Видение</h2>
                            <p class="article__text">{{ $article->text }}</p>
                            <div class="article__text-hidden">
                                <p class="article__text">{{ $article->title }}</p>
                                <p class="article__text">{{ $article->title }}</p>
                            </div>
                        </div>
                        <div class="article__descr">
                            <h2 class="article__title">Миссия</h2>
                            <p class="article__text">Мы максимально сокращаем Ваш путь от мечты к реальности, используя профессиональные навыки сотрудников компании, ее руководства, глубокое знание ситуации и понимание текущих трендов на рынке недвижимости. Мы берем на себя технические стороны покупки и делаем приобретение элитной недвижимости приятным, легким, комфортным, юридически и экономически безопасным процессом.</p>
                        </div>
                        <div class="article__descr">
                            <h2 class="article__title">Цели</h2>
                            <div class="article__text">
                                <ul class="article__list">
                                    <li class="article__item">Наша основная цель – помочь Вам в реализации Вашей мечты!</li>
                                    <li class="article__item">Своей ответственной работой каждый день подтверждаем оказанное Клиентом доверие и проявленную им лояльность при выборе нашей компании.</li>
                                    <li class="article__item">Стремимся обеспечить узнаваемость нашего бренда безупречной репутацией и высоким уровнем профессионализма предоставляемых услуг. Мы прикладываем все усилия, чтобы занять лидирующую позицию на рынке элитной недвижимости.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>
                <form class="article__form main-form" action="{{ route('forms.feedback') }}" method="POST">
                    <h2 class="main-form__title">обратная связь</h2>
                    @csrf
                    @if ($errors->any())
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <div class="main-form__inner">
                        <input class="main-form__input" name="name" type="text" placeholder="Ваше имя">
                        <input class="main-form__input" name="phone" type="tel" placeholder="Номер телефона">
                        <input class="main-form__input" name="email" type="email" placeholder="E-mail">
                        <textarea class="main-form__textarea" name="textarea" cols="30" rows="10" placeholder="Комментарий к заявке. Здесь вы можете указать желаемое время звонки или другую важную информацию."></textarea>
                        <button class="main-form__btn" type="submit">Отправить</button>
                        <div class="main-form__agree">
                            <label class="main-form__agree-label">
                                <input type="checkbox" name="check">
                                <span class="main-form__agree-check"></span>
                                <span class="main-form__agree-text">Соглашаюсь с политикой конфиденциальности.</span>
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('layouts.modules.features')

@endsection
