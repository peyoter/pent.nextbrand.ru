<!DOCTYPE html>
<html>
<head>
    <title>Mail Sending</title>
</head>
<body>
<table align='center' style='height: 200px;width: 200px'>
    <tr>
        <td colspan='2' bgcolor='skyblue' style='text-align: center;font-weight:bold;size: 20px;'>
            <label> Mail Check </label>
        </td>
    </tr>
    <tr bgcolor='#fff7e6'>
        <td style='color: magenta;font-weight: bold;'>
            <label>
                <b>Your Name:</b>
            </label>
        </td>
        <td>
            <b>{{$name}}</b>
        </td>
    </tr>
    <br>
    <tr>
        <td style='color: magenta;font-weight: bold;'>
            <label>
                <b>Your Phone:</b>
            </label>
        </td>
        <td>
            <b>{{$phone}}</b>
        </td>
    </tr>
    <br>
    <tr>
        <td style='color: magenta;font-weight: bold;'>
            <label>
                <b>Your Email:</b>
            </label>
        </td>
        <td>
            <b>{{$email}}</b>
        </td>
        <br>
    </tr>
    <tr>
        <td style='color: magenta;font-weight: bold;'><label><b>Your Text:</b>
            </label>
        </td>
        <td>
            <b>{{$textarea}}</b>
        </td>
        <br>
    </tr>
</table>
</body>
</html>
