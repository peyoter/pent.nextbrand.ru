<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="ru" style="-webkit-box-sizing:border-box; box-sizing:border-box">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Penthouse Moscow</title>
    <style>
        *::before {-webkit-box-sizing:inherit;box-sizing:inherit}
        *::after {-webkit-box-sizing:inherit;box-sizing:inherit}
        .cover__top::after {content:"";position:absolute;left:0;right:0;bottom:0;height:1px;background:#fff}
        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-Light.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-Light.woff2") format("woff2"), url("fonts/stem/Stem-Light.woff") format("woff"), url("fonts/stem/Stem-Light.ttf") format("truetype"), url("fonts/stem/Stem-Light.svg") format("svg");
            font-weight: 300;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-SemiLight.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-SemiLight.woff2") format("woff2"), url("fonts/stem/Stem-SemiLight.woff") format("woff"), url("fonts/stem/Stem-SemiLight.ttf") format("truetype"), url("fonts/stem/Stem-SemiLight.svg") format("svg");
            font-weight: 350;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-Regular.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-Regular.woff2") format("woff2"), url("fonts/stem/Stem-Regular.woff") format("woff"), url("fonts/stem/Stem-Regular.ttf") format("truetype"), url("fonts/stem/Stem-Regular.svg") format("svg");
            font-weight: 400;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-Medium.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-Medium.woff2") format("woff2"), url("fonts/stem/Stem-Medium.woff") format("woff"), url("fonts/stem/Stem-Medium.ttf") format("truetype"), url("fonts/stem/Stem-Medium.svg") format("svg");
            font-weight: 500;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-Bold.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-Bold.woff2") format("woff2"), url("fonts/stem/Stem-Bold.woff") format("woff"), url("fonts/stem/Stem-Bold.ttf") format("truetype"), url("fonts/stem/Stem-Bold.svg") format("svg");
            font-weight: 700;
            font-style: normal;
            font-display: swap;
        }
    </style>
</head>
<body style='font-family:"Stem", sans-serif; letter-spacing:0.1rem; margin:0'>

<table class="cover" cellpadding="0" cellspacing="0" style="background-image: url(/pdf/images/01.jpg); background-repeat:no-repeat; background-size:cover; background-position:center; min-height:2718px; min-width:1900px; padding-left:245px; padding-right:245px; width:100%" width="100%">
    <tbody>
    <tr>
        <td valign="top">
            <table class="cover__top" cellpadding="0" cellspacing="0" style="color:#fff; font-size:62px; font-weight:700; line-height:80px; padding-bottom:400px; padding-top:285px; position:relative; text-transform:uppercase; width:100%" width="100%">
                <tbody>
                <tr>
                    <td><a href="#" style="color:inherit; text-decoration:none">PENTHOUSE<span style="color:#AFAFAF; font-size:49.6134px; font-weight:350; line-height:64px; text-transform:lowercase">.moscow</span></a></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table class="cover__pent" cellpadding="0" cellspacing="0" style="color:rgba(255, 255, 255, 0.9); font-size:162px; font-weight:700; line-height:140px; padding-bottom:692px">
                <tbody>
                <tr><td>ЛУЧШИЕ</td></tr>
                <tr><td>ПЕНТХАУСЫ</td></tr>
                <tr><td>МОСКВЫ</td></tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="bottom">
            <table class="cover__logo" cellpadding="0" cellspacing="0" style="padding-bottom:185px">
                <tbody>
                <tr>
                    <td><a href="#" style="color:inherit; text-decoration:none"><img src="/pdf/images/logo-white.svg" alt="логотип" width="598" height="auto" style="display:block; height:auto; max-width:100%"></a></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<pagebreak/>

<table class="cover" cellpadding="0" cellspacing="0" style="background-image: url(/pdf/images/02.jpg); background-repeat:no-repeat; background-size:cover; background-position:center; min-height:2718px; min-width:1900px; padding-left:245px; padding-right:245px; width:100%" width="100%">
    <tbody>
    <tr>
        <td valign="top">
            <table class="cover__top" cellpadding="0" cellspacing="0" style="color:#fff; font-size:62px; font-weight:700; line-height:80px; padding-bottom:400px; padding-top:285px; position:relative; text-transform:uppercase; width:100%" width="100%">
                <tbody>
                <tr>
                    <td><a href="#" style="color:inherit; text-decoration:none">PENTHOUSE<span style="color:#AFAFAF; font-size:49.6134px; font-weight:350; line-height:64px; text-transform:lowercase">.moscow</span></a></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table class="cover__pent" cellpadding="0" cellspacing="0" style="color:rgba(255, 255, 255, 0.9); font-size:162px; font-weight:700; line-height:140px; padding-bottom:692px">
                <tbody>
                <tr><td>ЛУЧШИЕ</td></tr>
                <tr><td>ПЕНТХАУСЫ</td></tr>
                <tr><td>МОСКВЫ</td></tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="bottom">
            <table class="cover__logo" cellpadding="0" cellspacing="0" style="padding-bottom:185px">
                <tbody>
                <tr>
                    <td><a href="#" style="color:inherit; text-decoration:none"><img src="/pdf/images/logo-white.svg" alt="логотип" width="598" height="auto" style="display:block; height:auto; max-width:100%"></a></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<pagebreak/>

<table class="content" cellpadding="0" cellspacing="0" width="500" align="center" style="font-size:26px; font-weight:400; line-height:26px; padding-bottom:180px; padding-top:250px; padding-left:150px; padding-right:150px; width: 100%; ">
    <tbody>
    <tr>
        <td>
            @foreach($data['complex'] as $complex)
            <table class="content__item" cellpadding="0" cellspacing="0" width="999" align="left" style=" padding-bottom:70px">
                <tbody>
                <tr>
                    <td class="content__title dot" style="color:#A0A0A0; font-weight:700; width:650px; text-align: left !important; overflow:hidden; white-space:nowrap"><span style="color:#000">{{ $complex->complex_title }}</span></td>
                    <td align="right" class="content__page" style="min-width:40px; width: 650px ">{{ $complex->penthouses_count }}</td>
                </tr>
                <tr><td class="content__price" style="color:rgba(194, 194, 194, 0.9); font-size:21px; line-height:25px;  padding-top:5px">от {{ $complex->penthouses_min_full_price_ru }} ₽</td></tr>
                </tbody>
            </table>
            @endforeach
        </td>
    </tr>
    </tbody>
</table>

<pagebreak/>
@foreach($data['complex'] as $complex)
<table cellpadding="0" cellspacing="0" align="center" bgcolor="#fff">
    <tbody>
    <tr>
        <td>
            <table class="header" cellpadding="0" cellspacing="0" align="center" style="padding:95px 0 60px; padding-left: 100px; width: 100%;">
                <tbody>
                <tr><td class="header__title" style="color:rgba(0, 0, 0, 0.9); font-size:56px; font-weight:bold; line-height:56px">{{ $complex->complex_title }}</td></tr>
                <tr><td class="header__street" style="font-size:33px; font-weight:500; letter-spacing:0.1rem; line-height:40px">{{ $complex->complex_address }}</td></tr>
                <tr>
                    @foreach($complex->districts as $district)
                        <td class="header__zone" style="color:#AFAFAF; font-size:26px; font-weight:500; letter-spacing:0.1rem; line-height:40px">{{ $district->district}}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
            <table class="img" cellpadding="0" cellspacing="0" align="left" style="width:100%">
                <tbody>
                <tr>
                    <td align="center" width="1810" height="1080" style="background-image: url(/pdf/images/pent-1.jpg); background-position: center center; background-repeat: no-repeat; background-size: contain;"></td>
                </tr>
                </tbody>
            </table>
            <table class="info" cellpadding="0" cellspacing="0" align="center" width="1685" style="letter-spacing:0.1rem">
                <tbody>
                <tr>
                    <td class="info__left-wrapper" style="position:relative; top:-330px; width:840px" width="840">
                        <table class="info__left" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr><td class="info__left-img"><img src="/pdf/images/map.jpg" alt="карта" style="display:block; height:auto; max-width:100%; margin-top: -50%;" height="auto"></td></tr>
                            <tr><td class="info__left-title" style="font-size:30px; font-weight:500; line-height:38px; padding:0 20px; padding-top:30px">Sed ornare risus si</td></tr>
                            <tr><td class="info__left-text" style="font-size:24px; font-weight:300; line-height:41px; padding:0 20px; padding-top:50px">{{ $complex->complex_desc }}</td></tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="info__right-wrapper" cellpadding="0" cellspacing="0" valign="top" style="padding-left:110px; padding-top:125px; width:800px" width="800">
                        <table class="info__right">
                            <tbody>
                            <tr class="info__right-order" style="white-space:nowrap;">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Класс</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">{{ $complex->class }}</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Готовность</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">{{$complex->delivery}}</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Инфраструктура</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">{{$complex->infrastructure}}</td>
                            </tr>
                            <tr class="info__right-order" style="white-space:nowrap;">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Класс</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">{{ $complex->class }}</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Готовность</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">{{$complex->delivery}}</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Инфраструктура</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">{{$complex->infrastructure}}</td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr><td class="info__right-num" style="font-size:26px; font-weight:350; line-height:40px; padding-top:50px">{{ $complex->penthouses_count }} пентхаусов</td></tr>
                            <tr><td class="info__right-price" style="font-size:52px; font-weight:700; line-height:67px">от {{ $complex->penthouses_min_full_price_ru }} ₽</td></tr>
                            <tr><td style="font-size:20px; font-weight:350; line-height:21px; color: #686868; padding-top: 14px;">328 000 р./м2</td></tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0" class="info__right-contact" width="670" style="padding-top:55px">
                            <tbody>
                            <tr>
                                <td valign="center" width="193">{{ $data['qrCodeC']->generate('http://pent.nextbrand.ru/catalog/'. $complex->complex_slug .'') }}</td>
                                <td valign="center" align="right" style="padding-left: 100px">
                                    <table cellpadding="0" cellspacing="0" width="392">
                                        <tbody>
                                            <tr><td class="info__right-show" align="center" style="-webkit-text-decoration-line:underline; background:#A79078; color:#fff; display:block; font-size:23px; font-weight:500; line-height:30px; padding:22px 5px; text-align:center; text-decoration-line:underline; width:100%" width="100%"><a href="#" style="color:inherit; text-decoration:none">Посмотреть на сайте</a></td></tr>
                                            <tr><td class="info__right-or" align="center" style="font-size:24px; font-weight:300; line-height:41px; padding:18px 0">или</td></tr>
                                            <tr><td class="info__right-tel" align="center" style="font-size:36.95px; font-weight:300; line-height:48px; text-transform:uppercase"><a href="tel:+74951201100" style="color:inherit; text-decoration:none">+7 495 120 11 00</a></td></tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="footer" cellpadding="0" cellspacing="0" style="border-top:1px solid #DFDFDF; padding:60px 0; margin-top: 100px; width:100%" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table class="footer__inner" cellpadding="0" cellspacing="0" align="center" width="1810">
                            <tbody>
                            <tr>
                                <td align="left" class="footer__slogan" style="color:#7E7E7E; font-size:31px; font-weight:300; line-height:40px; text-transform:uppercase; width: 65%;">Быть особенным - быть собой.</td>
                                <td align="right" class="footer__logo"><a href="#" style="color:inherit; text-decoration:none; width: 65%;"><img src="/pdf/images/logo.svg" alt="логотип" width="444" height="auto" style="display:block; height:auto; max-width:100%"></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<pagebreak/>

<table cellpadding="0" cellspacing="0" align="center" bgcolor="#fff">
    <tbody>
    <tr>
        <td>
            <table class="header" cellpadding="0" cellspacing="0" align="center" style="padding:95px 0 60px; padding-left: 100px; width: 100%;">
                <tbody>
                <tr><td class="header__title" style="color:rgba(0, 0, 0, 0.9); font-size:56px; font-weight:bold; line-height:56px; padding-bottom: 15px;">{{ $complex->penthouses->penthouse_title }}</td></tr>
                <tr><td class="header__street" style="font-size:33px; font-weight:500; letter-spacing:0.1rem; line-height:40px">{{ $complex->complex_address }}</td></tr>
                <tr>
                    @foreach($complex->districts as $district)
                        <td class="header__zone" style="color:#AFAFAF; font-size:26px; font-weight:500; letter-spacing:0.1rem; line-height:40px">{{ $district->district}}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
            <table class="img" cellpadding="0" cellspacing="0" align="left" style="width:100%">
                <tbody>
                <tr>
                    <td align="center" width="1860" height="1080" style="background-image: url('/pdf/images/pent-2.jpg'); background-position: center center; background-repeat: no-repeat; background-size: contain;"></td>
                </tr>
                </tbody>
            </table>
            <table class="info" cellpadding="0" cellspacing="0" align="center" width="1685" style="letter-spacing:0.1rem">
                <tbody>
                <tr>
                    <td class="info__left-wrapper" style="position:relative; top:-330px; width:840px" width="840">
                        <table class="info__left" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr><td class="info__left-img"><img src="/pdf/images/project.png" alt="проект" style="display:block; height:auto; max-width:100%; margin-top: -50%;" height="auto"></td></tr>
                            <tr><td class="info__left-title" style="font-size:30px; font-weight:500; line-height:38px; padding:0 20px; padding-top:30px">Sed ornare risus si</td></tr>
                            <tr><td class="info__left-text" style="font-size:24px; font-weight:300; line-height:41px; padding:0 20px; padding-top:50px">Sed ornare risus sit amet neque convallis, non elementum odio venenatis. Donec felis elit, sodales et viverra nec, efficitur ac orci. In suscipit justo ac sapien dapibus, vitae iaculis purus accumsan. Sed nec purus mi. Ut elit metus, iaculis id accumsan ac, blandit at tortor. Suspendisse potenti. Nulla vel diam ac turpis egestas malesuada sed a erat. mi. Ut elit metus, iaculis id accumsan ac, blandit at tortor. Suspendisse potenti. Nulla vel diam ac turpis egestas malesuada sed a erat.</td></tr>
                            <tr><td class="info__left-text" style="font-size:24px; font-weight:300; line-height:41px; padding:0 20px; padding-top:50px">Sed ornare risus sit amet neque convallis, non elementum odio venenatis. Donec felis elit, sodales et viverra nec, efficitur ac orci. In suscipit justo ac sapien dapibus, vitae iaculis purus accumsan. Sed nec purus mi.</td></tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="info__right-wrapper" cellpadding="0" cellspacing="0" valign="top" style="padding-left:110px; padding-top:125px; width:800px" width="800">
                        <table class="info__right">
                            <tbody>
                            <tr class="info__right-order" style="white-space:nowrap;">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Высота потолков</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">3,6 м.</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Комнаты</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">5</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Спальни</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">3</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;"><p class="dot">Количество санузлов</p></td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">4</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Этаж</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">45</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Тип отделки</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">С мебелью</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">ID</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">734874647632</td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr><td class="info__right-num" style="font-size:26px; font-weight:350; line-height:40px; padding-top:50px">10 пентхаусов</td></tr>
                            <tr><td class="info__right-price" style="font-size:52px; font-weight:700; line-height:67px">от 122 000 000 ₽</td></tr>
                            <tr><td style="font-size:20px; font-weight:350; line-height:21px; color: #686868; padding-top: 14px;">328 000 р./м2</td></tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0" class="info__right-contact" width="670" style="padding-top:55px">
                            <tbody>
                            <tr>
                                <td valign="center" width="193"><img src="/pdf/images/qr.svg" alt="qr" width="191" height="auto" style="display:block; height:auto; max-width:100%"></td>
                                <td valign="center" align="right" style="padding-left: 100px">
                                    <table cellpadding="0" cellspacing="0" width="392"><tbody>
                                        <tr><td class="info__right-show" align="center" style="-webkit-text-decoration-line:underline; background:#A79078; color:#fff; display:block; font-size:23px; font-weight:500; line-height:30px; padding:22px 5px; text-align:center; text-decoration-line:underline; width:100%" width="100%"><a href="#" style="color:inherit; text-decoration:none">Посмотреть на сайте</a></td></tr>
                                        <tr><td class="info__right-or" align="center" style="font-size:24px; font-weight:300; line-height:41px; padding:18px 0">или</td></tr>
                                        <tr><td class="info__right-tel" align="center" style="font-size:36.95px; font-weight:300; line-height:48px; text-transform:uppercase"><a href="tel:+74951201100" style="color:inherit; text-decoration:none">+7 495 120 11 00</a></td></tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="footer" cellpadding="0" cellspacing="0" style="border-top:1px solid #DFDFDF; padding:60px 0; margin-top: 100px; width:100%" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table class="footer__inner" cellpadding="0" cellspacing="0" align="center" width="1810">
                            <tbody>
                            <tr>
                                <td align="left" class="footer__slogan" style="color:#7E7E7E; font-size:31px; font-weight:300; line-height:40px; text-transform:uppercase; width: 65%;">Быть особенным - быть собой.</td>
                                <td align="right" class="footer__logo"><a href="#" style="color:inherit; text-decoration:none; width: 65%;"><img src="/pdf/images/logo.svg" alt="логотип" width="444" height="auto" style="display:block; height:auto; max-width:100%"></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
@endforeach

<table cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" style="padding-left: 300px; padding-right: 300px;">
    <tbody>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" align="left" width="1100">
                <tbody>
                <tr>
                    <td class="text" style="font-size:30px; font-weight:300; letter-spacing:0.1rem; line-height:50px; padding-top:260px">Если у вас возникли вопросы по любым объектам,
                        представленным на нашем сайте, предлагаем оставить
                        заявку, чтобы менеджеры компании могли оперативно
                        вам перезвонить и дать наиболее полную информацию
                        по всем интересующим вас объектам.</td>
                </tr>
                </tbody>
            </table>
            <table class="features" cellpadding="0" cellspacing="0" align="left" width="1100" style="letter-spacing:0.1rem; padding-top:170px">
                <tbody>
                <tr>
                    <td>
                        <table class="features__item" style="padding-bottom:90px">
                            <tbody>
                            <tr><td class="features__title" style="font-size:29px; text-decoration: underline; font-weight:bold; line-height:29px; padding-bottom:10px; position:relative; text-transform:uppercase">Сэкономим время</td></tr>
                            <tr><td class="features__text" style="font-size:26px; font-weight:300; line-height:33px; padding-top:19px">Найдём лучшие варианты и обо всём договоримся за вас</td></tr>

                            </tbody>
                        </table>
                        <table class="features__item" style="padding-bottom:90px">
                            <tbody>
                            <tr><td class="features__title" style="font-size:29px; text-decoration: underline; font-weight:bold; line-height:29px; padding-bottom:10px; position:relative; text-transform:uppercase">Лучшие условия</td></tr>
                            <tr><td class="features__text" style="font-size:26px; font-weight:300; line-height:33px; padding-top:19px">Договоримся о скидке, добьёмся наилучших условий</td></tr>
                            </tbody>
                        </table>
                        <table class="features__item" style="padding-bottom:90px"><tbody>
                            <tr><td class="features__title" style="font-size:29px; text-decoration: underline; font-weight:bold; line-height:29px; padding-bottom:10px; position:relative; text-transform:uppercase">Поможем разобраться</td></tr>
                            <tr><td class="features__text" style="font-size:26px; font-weight:300; line-height:33px; padding-top:19px">Экспертиза, знание рынка, внимание к клиенту</td></tr>
                            </tbody></table>
                        <table class="features__item" style="padding-bottom:90px"><tbody>
                            <tr><td class="features__title" style="font-size:29px; text-decoration: underline; font-weight:bold; line-height:29px; padding-bottom:10px; position:relative; text-transform:uppercase">Эксклюзивные предложения</td></tr>
                            <tr><td class="features__text" style="font-size:26px; font-weight:300; line-height:33px; padding-top:19px">Клубные дома и закрытые продажи</td></tr>
                            </tbody></table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="about" cellpadding="0" cellspacing="0" align="left" width="1100" style="font-size:22px; font-weight:350; line-height:34px; padding-bottom:280px; padding-top:480px"><tbody>
                <tr><td class="about__tel" style="font-size:58.95px; font-weight:300; line-height:76px; text-transform:uppercase"><a href="tel:+74951201100" style="color:inherit; text-decoration: none;">+7 495 120 11 00</a></td></tr>
                <tr><td class="about__mail" style="padding-bottom:40px; padding-top:70px"><a href="mailto:ga@aandp.ru" style="color:inherit; text-decoration: none;">E-mail: <b class="about__link" style="text-decoration: underline;">ga@aandp.ru</b></a></td></tr>
                <tr><td>Элитная недвижимость: <a class="about__link" href="#" style="color:inherit; text-decoration:underline; position:relative"><b>aandp.ru</b></a>
                    </td></tr>
                <tr><td>Пентхаусы Москвы: <a class="about__link" href="#" style="color:inherit; text-decoration:underline; line-height: 30px;"><b>penthouse.moscow</b></a>
                    </td></tr>
                <tr><td class="about__address" style="padding-top:70px"><b>127055 г. Москва, ул. Лесная, дом 43, офис 236</b></td></tr>
                </tbody></table>
        </td>
    </tr>
    </tbody>
</table>

<pagebreak/>

<table cellpadding="0" cellspacing="0" align="center" bgcolor="#fff">
    <tbody>
    <tr>
        <td>
            <table class="header" cellpadding="0" cellspacing="0" align="center" style="padding:95px 0 60px; padding-left: 100px; width: 100%;">
                <tbody>
                <tr><td class="header__title" style="color:rgba(0, 0, 0, 0.9); font-size:56px; font-weight:bold; line-height:56px; padding-bottom: 15px;">Knightsbridge Private Park</td></tr>
                <tr><td class="header__street" style="font-size:33px; font-weight:500; letter-spacing:0.1rem; line-height:40px">Москва, ул. Новый Арбат, 46</td></tr>
                <tr><td class="header__zone" style="color:#AFAFAF; font-size:26px; font-weight:500; letter-spacing:0.1rem; line-height:40px">Замоскворечье</td></tr>
                </tbody>
            </table>
            <table class="img" cellpadding="0" cellspacing="0" align="left" style="width:100%">
                <tbody>
                <tr>
                    <td align="center" width="1825" height="1080" style="background-image: url('/pdf/images/pent-1.jpg'); background-position: center center; background-repeat: no-repeat; background-size: contain;"></td>
                </tr>
                </tbody>
            </table>
            <table class="info" cellpadding="0" cellspacing="0" align="center" width="1685" style="letter-spacing:0.1rem">
                <tbody>
                <tr>
                    <td class="info__left-wrapper" style="position:relative; top:-330px; width:840px" width="840">
                        <table class="info__left" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr><td class="info__left-img"><img src="/pdf/images/map.jpg" alt="карта" style="display:block; height:auto; max-width:100%; margin-top: -50%;" height="auto"></td></tr>
                            <tr><td class="info__left-title" style="font-size:30px; font-weight:500; line-height:38px; padding:0 20px; padding-top:30px">Sed ornare risus si</td></tr>
                            <tr><td class="info__left-text" style="font-size:24px; font-weight:300; line-height:41px; padding:0 20px; padding-top:50px">Sed ornare risus sit amet neque convallis, non elementum odio venenatis. Donec felis elit, sodales et viverra nec, efficitur ac orci. In suscipit justo ac sapien dapibus, vitae iaculis purus accumsan. Sed nec purus mi. Ut elit metus, iaculis id accumsan ac, blandit at tortor. Suspendisse potenti. Nulla vel diam ac turpis egestas malesuada sed a erat. mi. Ut elit metus, iaculis id accumsan ac, blandit at tortor. Suspendisse potenti. Nulla vel diam ac turpis egestas malesuada sed a erat.</td></tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="info__right-wrapper" cellpadding="0" cellspacing="0" valign="top" style="padding-left:110px; padding-top:125px; width:800px" width="800">
                        <table class="info__right">
                            <tbody>
                            <tr class="info__right-order" style="white-space:nowrap;">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Класс</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">Клубный дом</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Готовность</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">Сдача в 2020</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Инфраструктура</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">Бассейн, парковая зона</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;"><p class="dot">Класс</p></td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">Клубный дом</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Готовность</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">Сдача в 2020</td>
                            </tr>
                            <tr class="info__right-order">
                                <td class="dot" style="font-size:25px; font-weight:300; line-height:51px; width:338px; max-width: 338px; overflow:hidden;">Инфраструктура</td>
                                <td style="font-size:25px; font-weight:400; line-height:51px; width:338px; max-width: 338px; padding-left: 5px; overflow:hidden;">Бассейн, парковая зона</td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr><td class="info__right-num" style="font-size:26px; font-weight:350; line-height:40px; padding-top:50px">10 пентхаусов</td></tr>
                            <tr><td class="info__right-price" style="font-size:52px; font-weight:700; line-height:67px">от 122 000 000 ₽</td></tr>
                            <tr><td style="font-size:20px; font-weight:350; line-height:21px; color: #686868; padding-top: 14px;">328 000 р./м2</td></tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0" class="info__right-contact" width="670" style="padding-top:55px">
                            <tbody>
                            <tr>
                                <td valign="center" width="193"><img src="/pdf/images/qr.svg" alt="qr" width="191" height="auto" style="display:block; height:auto; max-width:100%"></td>
                                <td valign="center" align="right" style="padding-left: 100px">
                                    <table cellpadding="0" cellspacing="0" width="392"><tbody>
                                        <tr><td class="info__right-show" align="center" style="-webkit-text-decoration-line:underline; background:#A79078; color:#fff; display:block; font-size:23px; font-weight:500; line-height:30px; padding:22px 5px; text-align:center; text-decoration-line:underline; width:100%" width="100%"><a href="#" style="color:inherit; text-decoration:none">Посмотреть на сайте</a></td></tr>
                                        <tr><td class="info__right-or" align="center" style="font-size:24px; font-weight:300; line-height:41px; padding:18px 0">или</td></tr>
                                        <tr><td class="info__right-tel" align="center" style="font-size:36.95px; font-weight:300; line-height:48px; text-transform:uppercase"><a href="tel:+74951201100" style="color:inherit; text-decoration:none">+7 495 120 11 00</a></td></tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="footer" cellpadding="0" cellspacing="0" style="border-top:1px solid #DFDFDF; padding:60px 0; margin-top: 100px; width:100%" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table class="footer__inner" cellpadding="0" cellspacing="0" align="center" width="1810">
                            <tbody>
                            <tr>
                                <td align="left" class="footer__slogan" style="color:#7E7E7E; font-size:31px; font-weight:300; line-height:40px; text-transform:uppercase; width: 65%;">Быть особенным - быть собой.</td>
                                <td align="right" class="footer__logo"><a href="#" style="color:inherit; text-decoration:none; width: 65%;"><img src="/pdf/images/logo.svg" alt="логотип" width="444" height="auto" style="display:block; height:auto; max-width:100%"></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
