<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/obj1/style.min.css">
    <title>{{ $data['penthouse']->penthouse_title }}</title>
    <style>
        *::before {-webkit-box-sizing:inherit;box-sizing:inherit}
        *::after {-webkit-box-sizing:inherit;box-sizing:inherit}
        .cover__top::after {content:"";position:absolute;left:0;right:0;bottom:0;height:1px;background:#fff}
        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-Light.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-Light.woff2") format("woff2"), url("fonts/stem/Stem-Light.woff") format("woff"), url("fonts/stem/Stem-Light.ttf") format("truetype"), url("fonts/stem/Stem-Light.svg") format("svg");
            font-weight: 300;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-SemiLight.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-SemiLight.woff2") format("woff2"), url("fonts/stem/Stem-SemiLight.woff") format("woff"), url("fonts/stem/Stem-SemiLight.ttf") format("truetype"), url("fonts/stem/Stem-SemiLight.svg") format("svg");
            font-weight: 350;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-Regular.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-Regular.woff2") format("woff2"), url("fonts/stem/Stem-Regular.woff") format("woff"), url("fonts/stem/Stem-Regular.ttf") format("truetype"), url("fonts/stem/Stem-Regular.svg") format("svg");
            font-weight: 400;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-Medium.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-Medium.woff2") format("woff2"), url("fonts/stem/Stem-Medium.woff") format("woff"), url("fonts/stem/Stem-Medium.ttf") format("truetype"), url("fonts/stem/Stem-Medium.svg") format("svg");
            font-weight: 500;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Stem';
            src: url("fonts/stem/Stem-Bold.eot?#iefix") format("embedded-opentype"), url("fonts/stem/Stem-Bold.woff2") format("woff2"), url("fonts/stem/Stem-Bold.woff") format("woff"), url("fonts/stem/Stem-Bold.ttf") format("truetype"), url("fonts/stem/Stem-Bold.svg") format("svg");
            font-weight: 700;
            font-style: normal;
            font-display: swap;
        }

        img {
            display: block;
            max-width: 100%;
            height: auto;
        }
        body {
            font-family: 'Stem', sans-serif;
            margin: 0;
            letter-spacing: 0.1rem;
        }

        a {
            text-decoration: none;
            color: inherit;
        }

        .header {
            width: 100%;
            padding: 50px 120px;
            border-bottom: 1px solid #DFDFDF;
        }

        .header__logo {
            width: 50%;
        }

        .header__logo-pent {
            font-weight: 700;
            font-size: 62px;
            line-height: 80px;
        }

        .header__logo-moscow {
            margin-left: 10px;
            font-weight: 350;
            font-size: 49px;
            line-height: 64px;
            color: #AFAFAF;
        }

        .header__tel {
            width: 50%;
            font-weight: 300;
            font-size: 42px;
            line-height: 54px;
        }

        .about {
            padding: 105px 120px 100px;
        }

        .title {
            padding: 80px 100px;
        }

        .title h1 {
            font-weight: 500;
            font-size: 43px;
            line-height: 40px;
            letter-spacing: 0.1rem;
        }

        .about__title {
            font-weight: bold;
            font-size: 59px;
            line-height: 72px;
            letter-spacing: 0.1rem;
        }

        .about__address {
            padding-top: 43px;
            font-weight: 500;
            font-size: 33px;
            line-height: 40px;
            letter-spacing: 0.1rem;
        }

        .about__qr {
            width: 193px;
            padding-top: 30px;
        }

        .about__right {
            padding-left: 80px;
        }

        .about__right .about__address {
            padding-top: 10px;
        }

        .text {
            padding: 100px;
            font-weight: 300;
            font-size: 30px;
            line-height: 50px;
            text-align: justify;
            letter-spacing: 0.1rem;
        }

        .img {
            padding: 0 100px;
        }

        .img__td {
            width: 100%;
            height: 967px;
            background-image: url("{{asset($data['penthouse']->penthouse_main_img)}}");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }

        .img--plan {
            padding-bottom: 400px;
        }

        .img__plan {
            width: 100%;
            height: 1300px;
            background-image: url("{{asset($data['penthouse']->fit($data['penthouse']->penthouse_plan, 693, 546))}}");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: left top;
        }

        .img__pent-one {
            width: 100%;
            height: 967px;
            margin-bottom: 70px;
            background-image: url("{{asset('/images/pdf/obj2.jpg')}}");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }

        .img__pent-two {
            width: 100%;
            height: 967px;
            background-image: url("{{asset('/images/pdf/obj3.jpg')}}");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }

        .img__pent-space {
            width: 100%;
            height: 70px;
        }

        .img__pent-big__space {
            width: 100%;
            height: 300px;
        }

        .img__barkli {
            width: 100%;
            height: 967px;
            background-image: url("{{asset($data['penthouse']->complex_main_img)}}");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center right;
        }

        .img__map {
            width: 100%;
            height: 967px;
            background-image: url("{{asset('/images/pdf/obj-map.jpg')}}");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center right;
        }

        .type {
            padding: 50px 120px 66px;
            font-weight: 350;
            font-size: 21px;
            line-height: 27px;
            letter-spacing: 0.1rem;
        }

        .type__title {
            width: 200px;
            padding: 9px 27px;
            margin-right: 30px;
            border: 1px solid #E6E6E6;
        }

        .type__space {
            width: 30px;
        }

        .info {
            padding-bottom: 200px;
        }

        .info__td {
            width: 50%;
        }

        .info--barkli {
            padding-bottom: 0;
            padding-left: 36px;
        }

        .info__left {
            padding: 0 100px;
            font-weight: 400;
            font-size: 25px;
            line-height: 52px;
            letter-spacing: 0.1rem;
        }

        .info__left-title {
            font-weight: 300;
        }

        .info__left td {
            width: 375px;
        }

        .text--barkli {
            padding-bottom: 412px;
            padding-left: 136px;
        }

        .price {
            padding: 75px;
            margin-right: 100px;
            background: #F2F2F2;
        }

        .price__size {
            padding-top: 60px;
            font-size: 38.0645px;
            line-height: 49px;
            letter-spacing: 0.1rem;
        }

        .price__money {
            padding: 13px 0 20px;
            font-weight: bold;
            font-size: 80px;
            line-height: 104px;
            letter-spacing: 0.1rem;
        }

        .price__meter {
            font-weight: 350;
            font-size: 30px;
            line-height: 32px;
            letter-spacing: 0.1rem;
            color: #686868;
        }

        .footer {
            padding: 60px 100px;
            border-top: 1px solid #DFDFDF;
        }

        .footer__slogan {
            width: 50%;
            font-weight: 300;
            font-size: 31px;
            line-height: 40px;
            text-transform: uppercase;
            color: #7E7E7E;
        }

        .footer__logo {
            width: 50%;
        }

        .man {
            padding: 200px 100px;
        }

        .man__img {
            width: 524px;
            height: 765px;
        }

        .man__info {
            padding-left: 110px;
        }

        .man__name {
            text-transform: uppercase;
            font-weight: 700;
            font-size: 40px;
            line-height: 52px;
            letter-spacing: 0.1rem;
        }

        .man__name-two {
            font-size: 30px;
            line-height: 50px;
            letter-spacing: 0.1rem;
        }

        .man__text {
            padding-top: 30px;
            font-weight: 300;
            font-size: 30px;
            line-height: 50px;
            letter-spacing: 0.1rem;
        }

        .man__text--thanks {
            font-weight: 500;
        }

        .man .man__tel {
            padding-top: 70px;
            font-weight: 300;
            font-size: 54px;
            line-height: 70px;
            text-transform: uppercase;
        }

        .man__about {
            padding-top: 16px;
            font-weight: 350;
            font-size: 22px;
            line-height: 34px;
            letter-spacing: 0.1rem;
        }

        .man__about a {
            text-decoration: underline;
            font-weight: 700;
        }

    </style>
</head>
<body>

<table class="wrapper" cellpadding="0" cellspacing="0" width="1900" align="center">
    <tbody>
    <tr>
        <td>
            <table class="header" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="header__logo"  align="left" cellpadding="0" cellspacing="0"><span class="header__logo-pent">PENTHOUSE</span> <span class="header__logo-moscow">.moscow</span></td>
                    <td class="header__tel" cellpadding="0" cellspacing="0" align="right">+7 495 120-11-00</td>
                </tr>
                </tbody>
            </table>
            <table class="about" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr><td><h1 class="about__title">{{ $data['penthouse']->penthouse_title }}</h1></td></tr>
                <tr><td class="about__address">{{ $data['penthouse']->complex_address }}</td></tr>
                </tbody>
            </table>
            <table class="img" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="img__td"></td>
                </tr>
                </tbody>
            </table>
            <table class="type" cellpadding="0" cellspacing="0" width="900" align="left">
                <tbody>
                <tr>
                    @foreach($data['penthouse']->tags as $tag)
                        <td class="type__title">{{ $tag->tag_name }}</td>
                        <td class="type__space"></td>
                    @endforeach
                </tr>
                </tbody>
            </table>
            <table class="info" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="info__td" align="left">
                        <table class="info__left" cellpadding="0" cellspacing="0" align="left" width="650">
                            <tbody>
                            <tr><td class="info__left-title">Район</td>
                                @foreach($data['penthouse']->complex->districts as $district)
                                    <td>{{ $district->district}}</td>
                                @endforeach
                            </tr>
                            <tr><td class="info__left-title">Метро</td>
                                @foreach($data['penthouse']->complex->metros as $metro)
                                    <td>{{ $metro->metro}}</td>
                                @endforeach
                            </tr>
                            <tr><td class="info__left-title">Высота потолков</td><td>{{ $data['penthouse']->ceiling }} м.</td></tr>
                            <tr><td class="info__left-title">Комнаты</td><td>{{ $data['penthouse']->rooms }}</td></tr>
                            <tr><td class="info__left-title">Спальни</td><td>{{ $data['penthouse']->bedrooms }}</td></tr>
                            <tr><td class="info__left-title">Количество санузлов</td><td>{{ $data['penthouse']->bathrooms }}</td></tr>
                            <tr><td class="info__left-title">Этаж</td><td>{{ $data['penthouse']->floor }}</td></tr>
                            <tr><td class="info__left-title">Тип отделки</td><td>{{ $data['penthouse']->apartment }}</td></tr>
                            <tr><td class="info__left-title">Инфраструктура</td><td>{{ $data['penthouse']->infrastructure }}</td></tr>
                            <tr><td class="info__left-title">Консьерж-сервис</td><td>@if($data['penthouse']->concierge_service == true)Да@elseНет@endif</td></tr>
                            <tr><td class="info__left-title">ID</td><td>{{ $data['penthouse']->penthouse_id }}</td></tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="info__td">
                        <table class="price" cellpadding="0" cellspacing="0" align="right" width="820">
                            <tbody>
                            <tr><td>{{ $data['qrCodeP'] }}</td></tr>
                            <tr><td class="price__size">{{ $data['penthouse']->square }} м<sup>2</sup>
                                    @if($data['penthouse']->terrace == true)+ терраса {{ $data['penthouse']->square_terrace }} м<sup>2</sup>@endif
                                </td>
                            </tr>
                            <tr><td class="price__money">{{ $data['penthouse']->full_price_ru }} &#8381;</td></tr>
                            <tr><td class="price__meter">328 000 р./м2</td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="footer" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="footer__slogan" cellpadding="0" cellspacing="0" align="left">Быть особенным - быть собой.</td>
                    <td class="footer__logo" cellpadding="0" cellspacing="0" align="right"><img src="{{asset('/images/pdf/logo.svg')}}" width="444"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<pagebreak/>

<table class="wrapper" cellpadding="0" cellspacing="0" width="1900" align="center">
    <tbody>
    <tr>
        <td>
            <table class="header" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="header__logo"  align="left" cellpadding="0" cellspacing="0"><span class="header__logo-pent">PENTHOUSE</span> <span class="header__logo-moscow">.moscow</span></td>
                    <td class="header__tel" cellpadding="0" cellspacing="0" align="right">+7 495 120-11-00</td>
                </tr>
                </tbody>
            </table>
            <table class="text">
                <tr>
                    <td>{{ $data['penthouse']->penthouse_desc }}</td>
                </tr>
            </table>
            <table class="img img--plan" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="img__plan"></td>
                </tr>
                </tbody>
            </table>
            <table class="footer" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="footer__slogan" cellpadding="0" cellspacing="0" align="left">Быть особенным - быть собой.</td>
                    <td class="footer__logo" cellpadding="0" cellspacing="0" align="right"><img src="{{asset('/images/pdf/logo.svg')}}" width="444"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<pagebreak/>

<table class="wrapper" cellpadding="0" cellspacing="0" width="1900" align="center">
    <tbody>
    <tr>
        <td>
            <table class="header" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="header__logo"  align="left" cellpadding="0" cellspacing="0"><span class="header__logo-pent">PENTHOUSE</span> <span class="header__logo-moscow">.moscow</span></td>
                    <td class="header__tel" cellpadding="0" cellspacing="0" align="right">+7 495 120-11-00</td>
                </tr>
                </tbody>
            </table>
            <table class="img img--pent" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="img__pent-one"></td>
                </tr>
                <tr>
                    <td class="img__pent-space"></td>
                </tr>
                <tr>
                    <td class="img__pent-two"></td>
                </tr>
                <tr>
                    <td class="img__pent-big__space"></td>
                </tr>
                </tbody>
            </table>
            <table class="footer" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="footer__slogan" cellpadding="0" cellspacing="0" align="left">Быть особенным - быть собой.</td>
                    <td class="footer__logo" cellpadding="0" cellspacing="0" align="right"><img src="{{asset('/images/pdf/logo.svg')}}" width="444"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<pagebreak/>

<table class="wrapper" cellpadding="0" cellspacing="0" width="1900" align="center">
    <tbody>
    <tr>
        <td>
            <table class="header" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="header__logo"  align="left" cellpadding="0" cellspacing="0"><span class="header__logo-pent">PENTHOUSE</span> <span class="header__logo-moscow">.moscow</span></td>
                    <td class="header__tel" cellpadding="0" cellspacing="0" align="right">+7 495 120-11-00</td>
                </tr>
                </tbody>
            </table>
            <table class="about" cellpadding="0" cellspacing="0" width="1900" align="left">
                <tbody>
                <tr>
                    <td class="about__qr" valign="top">{{ $data['qrCodeC'] }}</td>
                    <td class="about__right" valign="top">
                        <table>
                            <tbody>
                            <tr><td><h1 class="about__title">{{ $data['penthouse']->complex_title }}</h1></td></tr>
                            <tr><td class="about__address">{{ $data['penthouse']->complex_address }}</td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="img" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="img__barkli"></td>
                </tr>
                </tbody>
            </table>
            <table class="type" cellpadding="0" cellspacing="0" width="900" align="left">
                <tbody>
                <tr>
                    @foreach($data['pents'] as $penthouse)
                        @foreach($penthouse->tags as $tag)
                            <td class="type__title">{{ $tag->tag_name }}</td>
                            <td class="type__space"></td>
                        @endforeach
                    @endforeach
                </tr>
                </tbody>
            </table>
            <table class="info info--barkli" cellpadding="0" cellspacing="0" width="1900" align="left">
                <tbody>
                <tr>
                    <td class="info__td" align="left">
                        <table class="info__left" cellpadding="0" cellspacing="0" align="left" width="650">
                            <tbody>
                            <tr><td class="info__left-title">Класс</td>
                                @foreach($data['penthouse']->complex->classifications as $classification)
                                    <td>{{ $classification->classification}}</td>
                                @endforeach
                            </tr>
                            <tr><td class="info__left-title">Готовность</td><td>{{ $data['penthouse']->delivery }}</td></tr>
                            <tr><td class="info__left-title">Инфраструктура</td><td>{{ $data['penthouse']->infrastructure }}</td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="text text--barkli">
                <tbody>
                <tr>
                    <td>{{ $data['penthouse']->complex_desc }}</td>
                </tr>
                </tbody>
            </table>
            <table class="footer" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="footer__slogan" cellpadding="0" cellspacing="0" align="left">Быть особенным - быть собой.</td>
                    <td class="footer__logo" cellpadding="0" cellspacing="0" align="right"><img src="{{asset('/images/pdf/logo.svg')}}" width="444"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<pagebreak/>

<table class="wrapper" cellpadding="0" cellspacing="0" width="1900" align="center">
    <tbody>
    <tr>
        <td>
            <table class="header" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="header__logo"  align="left" cellpadding="0" cellspacing="0"><span class="header__logo-pent">PENTHOUSE</span> <span class="header__logo-moscow">.moscow</span></td>
                    <td class="header__tel" cellpadding="0" cellspacing="0" align="right">+7 495 120-11-00</td>
                </tr>
                </tbody>
            </table>
            <table class="title">
                <tbody>
                <tr><td><h1>{{ $data['penthouse']->complex_address }}</h1></td></tr>
                </tbody>
            </table>
            <table class="img" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="img__map"></td>
                </tr>
                </tbody>
            </table>
            <table class="man" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="man__img"><img src="{{asset('/images/pdf/man.jpg')}}" alt="Григорий Ашихмин" width="524" height="765"></td>
                    <td class="man__info">
                        <table>
                            <tbody>
                            <tr><td class="man__name">АШИХМИН</td></tr>
                            <tr><td class="man__name-two">Григорий</td></tr>
                            <tr><td class="man__text">Если у вас возникли вопросы по любым объектам,
                                    представленным на нашем сайте, предлагаем оставить
                                    заявку, чтобы менеджеры компании могли оперативно
                                    вам перезвонить и дать наиболее полную информацию
                                    по всем интересующим вас объектам.
                                </td></tr>
                            <tr><td class="man__text man__text--thanks">Спасибо за обращение в нашу компанию!</td></tr>
                            <tr><td class="man__tel"><a href="tel:+74951201100">+7 495 120 11 00</a></td></tr>
                            <tr><td class="man__about">E-mail: <a href="mailto:ga@aandp.ru">ga@aandp.ru</a></td></tr>
                            <tr><td class="man__about">Элитная недвижимость: <a href="aandp.ru">aandp.ru</a></td></tr>
                            <tr><td class="man__about">Пентхаусы Москвы: <a href="penthouse.moscow">penthouse.moscow</a></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="footer" cellpadding="0" cellspacing="0" width="1900" align="center">
                <tbody>
                <tr>
                    <td class="footer__slogan" cellpadding="0" cellspacing="0" align="left">Быть особенным - быть собой.</td>
                    <td class="footer__logo" cellpadding="0" cellspacing="0" align="right"><img src="{{asset('/images/pdf/logo.svg')}}" width="444"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
