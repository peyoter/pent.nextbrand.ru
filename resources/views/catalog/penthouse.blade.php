@extends('layouts.layout-2')
@section('page-title', $penthouse->penthouse_title)
@section('breadcrumbs', Breadcrumbs::render('catalog.penthouse', $penthouse))
@section('content')

    <div class="flat">
        <h1 class="flat__title main-title">{{ $penthouse->penthouse_title }}</h1>
        <div class="flat__inner">
            <aside class="flat__aside">
                <div class="flat__list swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($penthouse->penthouse_img as $pent_img)
                        <a class="flat__item swiper-slide" href="#">
                            <div class="flat__item-img">
                                <img src="{{ asset($penthouse->fit($pent_img, 299, 211)) }}" alt="пентхаус" width='299' height="211">
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>
                <ul class="flat__kind">
                @foreach($penthouse->tags as $tag)
                    <li class="flat__kind-item"><a href="#">{{ $tag->tag_name }}</a></li>
                @endforeach
                </ul>
                <div class="order">
                    <div class="order__present">
                        <p class="order__present-size">{{ $penthouse->square }} м<sup>2</sup>
                        @if($penthouse->terrace == true)+ терраса {{ $penthouse->square_terrace }} м<sup>2</sup></p>@endif
                        <p class="order__present-price">{{ $penthouse->full_price_ru }} &#8381;</p>
                        <p class="order__present-metre">328 000 р./м<sup>2</sup></p>
                        <a class="order__present-inspection form-toggle" href="#">Заказать осмотр</a>
                    </div>
                    <div class="order__show flat__show">
                        <p class="order__address">{{ $penthouse->complex_address }}</p>
                        <a class="order__link show-open" href="#">Показать на карте</a>
                    </div>
                    <div class="order__info">
                        <ul class="order__list flat__order-list">
                            <li class="order__item">
                                <span class="order__class">Район</span>
                                @foreach($penthouse->complex->districts as $district)
                                    <span class="order__type">{{ $district->district}}</span>
                                @endforeach
                            </li>
                            <li class="order__item">
                                <span class="order__class">Метро</span>
                                @foreach($penthouse->complex->metros as $metro)
                                    <span class="order__type">{{ $metro->metro }}</span>
                                @endforeach
                            </li>
                            <li class="order__item">
                                <span class="order__class">Высота потолков</span>
                                <span class="order__type">{{ $penthouse->ceiling }} м.</span>
                            </li>
                            <li class="order__item">
                                <span class="order__class">Комнаты</span>
                                <span class="order__type">{{ $penthouse->rooms }}</span>
                            </li>
                            <li class="order__item">
                                <span class="order__class">Спальни</span>
                                <span class="order__type">{{ $penthouse->bedrooms }}</span>
                            </li>
                            <li class="order__item">
                                <span class="order__class">Количество санузлов</span>
                                <span class="order__type">{{ $penthouse->bathrooms }}</span>
                            </li>
                            <li class="order__item">
                                <span class="order__class">Этаж</span>
                                <span class="order__type">{{ $penthouse->floor }}</span>
                            </li>
                            <li class="order__item">
                                <span class="order__class">Тип отделки</span>
                                <span class="order__type">{{ $penthouse->apartment }}</span>
                            </li>
                            <li class="order__item">
                                <span class="order__class">Инфраструктура</span>
                                <span class="order__type">{{ $penthouse->infrastructure }}</span>
                            </li>
                            <li class="order__item">
                                <span class="order__class">Консьерж-сервис</span>
                                <span class="order__type">@if($penthouse->concierge_service == true)Да@elseНет@endif</span>
                            </li>
                            <li class="order__item">
                                <span class="order__class">ID</span>
                                <span class="order__type">{{ $penthouse->penthouse_id }}</span>
                            </li>
                        </ul>
                        <a class="order__info-link" href="#">Все характеристики</a>
                    </div>
                    <div class="order__show flat__present">
                        <p class="order__address">Презентация объекта, pdf</p>
                        <a class="order__link" href="#">Скачать презентацию</a>
                    </div>
                    <section class="profile">
                        <div class="profile__img">
                            <img src="{{ asset($penthouse->fit($penthouse->emp_img, 327, 376)) }}" alt="{{ $penthouse->first_name }} {{ $penthouse->last_name }}">
                        </div>
                        <h2 class="profile__title"><span>{{ $penthouse->first_name }}</span>{{ $penthouse->last_name }}</h2>
                        <p class="profile__about">{{ $penthouse->emp_desc  }}</p>
                        <a class="profile__order form-toggle" href="#">Заказать осмотр</a>
                        <div class="profile__phone"><a href="tel:{{ $penthouse->emp_phone }}">{{ $penthouse->emp_phone }}</a></div>
                        <div class="profile__email"><a href="mailto:{{ $penthouse->emp_email }}">{{ $penthouse->emp_email }}</a></div>
                    </section>
                </div>
            </aside>
            <div class="flat__right">
                <section class="flat__info">
                    <div class="flat__list-wrapper">
                        <div class="flat__list-dekstop swiper-container gallery-top">
                            <button class="swiper-button-prev"></button>
                            <button class="swiper-button-next"></button>
                            <div class="swiper-wrapper">
                            @foreach($penthouse->penthouse_img as $pent_img)
                                <a class="flat__item swiper-slide" href="#">
                                    <div class="flat__item-img">
                                        <img src="{{ asset($penthouse->fit($pent_img, 1100, 620)) }}" alt="пентхаус" width='299' height="211">
                                    </div>
                                </a>
                            @endforeach
                            </div>
                        </div>
                        <div class="flat__list-dekstop flat__list-thumbs swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                            @foreach($penthouse->penthouse_img as $pent_img)
                                <div class="flat__item-thumbs swiper-slide">
                                    <img src="{{ asset($penthouse->fit($pent_img, 99,55)) }}" alt="пентхаус" width='299' height="211">
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                    <h2 class="flat__info-title">Описание пентхауса</h2>
                    <p class="flat__info-text">{{ $penthouse->penthouse_desc }}</p>
                </section>
                <section class="plan">
                    <h2 class="visually-hidden">Планировка Пентхауса</h2>
                    <div class="plan__img">
                        <img src="{{ asset($penthouse->fit($penthouse->penthouse_plan, 693, 546)) }}" alt="планировка пентхауса">
                    </div>
                    <div class="plan__info">
                        <p class="plan__info-about">Скачайте презентацию пентхауса и описание ЖК “БАРКЛИ ГАЛЕРИ”</p>
                        <p class="plan__info-size">Размер — 16.21 MB</p>
                        <a class="plan__info-download" href="#">Скачать презентацию</a>
                        <a class="plan__info-order form-toggle" href="#">Заказать осмотр</a>
                        <div class="plan__info-partner"><a href="#">Презентация для партнера</a></div>
                    </div>
                </section>
                <section class="villa">
                    <header class="villa__header">
                        <h2 class="villa__title">{{ $penthouse->complex_title }}</h2>
                        <p class="villa__text">{{ $penthouse->complex_desc }}</p>
                    </header>
                    <div class="villa__list swiper-container">
                        <button class="swiper-button-prev"></button>
                        <button class="swiper-button-next"></button>
                        <div class="swiper-wrapper">
                        @foreach($penthouse->complex_img as $com_img)
                            <a class="villa__item swiper-slide" href="#">
                                <img class="villa__img-tel" src="{{ asset($penthouse->fit($com_img, 306, 124)) }}" alt="пентхаус">
                                <img class="villa__img-dekstop" src="{{ asset($penthouse->fit($com_img, 540, 313)) }}" alt="пентхаус">
                            </a>
                        @endforeach
                        </div>
                    </div>
                    <div class="villa__info">
                        <div class="villa__order order">
                            <ul class="villa__order-list order__list">
                                <li class="order__item">
                                    <span class="order__class">Класс</span>
                                    @foreach($penthouse->complex->classifications as $classification)
                                        <span class="order__type">{{ $classification->classification}}</span>
                                    @endforeach
                                </li>
                                <li class="order__item">
                                    <span class="order__class">Готовность</span>
                                    <span class="order__type">{{ $penthouse->delivery }}</span>
                                </li>
                                <li class="order__item">
                                    <span class="order__class">Инфраструктура</span>
                                    <span class="order__type">{{ $penthouse->infrastructure }}</span>
                                </li>
                                <li class="order__item">
                                    <span class="order__class">Класс</span>
                                    @foreach($penthouse->complex->classifications as $classification)
                                        <span class="order__type">{{ $classification->classification}}</span>
                                    @endforeach
                                </li>
                                <li class="order__item">
                                    <span class="order__class">Готовность</span>
                                    <span class="order__type">{{ $penthouse->delivery }}</span>
                                </li>
                                <li class="order__item">
                                    <span class="order__class">Инфраструктура</span>
                                    <span class="order__type">{{ $penthouse->infrastructure }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="villa__links">
                            <a class="villa__links-map show-open" href="#">Показать на карте</a>
                            <a class="villa__links-order form-toggle" href="#">Заказать осмотр ЖК</a>
                        </div>
                    </div>
                </section>
                <div class="flat__page mansion">
                    <h2 class="sec-title">Пентхаусы в этом ЖК:</h2>
                    <header class="mansion__header">
                        <div class="mansion__sort">
                            <p class="mansion__sort-num">Пентхаусов найдено: <span>{{ $pentComplex->count() }}</span></p>
                            <p class="mansion__sort-type">Сортировать по:
                            @if($request->price == 'asc')
                                <a class="mansion__sort-type__btn" href="{{ $request->fullUrlWithQuery(['price' => 'desc']) }}">Цене, сначала дорогие</a>
                            @else
                                <a class="mansion__sort-type__btn" href="{{ $request->fullUrlWithQuery(['price' => 'asc']) }}">Цене, сначала дешевле</a>
                            @endif
                            </p>
                        </div>
                    </header>
                    <div class="mansion__house-wrapper">
                        @foreach($pentComplex as $pentComplexes)
                        <div class="mansion__house">
                            <div class="mansion__img">
                                <img src="{{ asset($penthouse->fit($penthouse->penthouse_main_img, 601, 430)) }}" alt="пентхаус">
                            </div>
                            <div class="mansion__info">
                                <div class="mansion__info-top">
                                    <h2 class="mansion__title">{{ $pentComplexes->penthouse_title }}</h2>
                                    <p class="mansion__street">{{ $penthouse->complex_address }}</p>
                                    <ul class="mansion__content">
                                        <li>{{ $pentComplexes->rooms }} <span>спальни</span></li>
                                        <li>Отделка <span>{{ $pentComplexes->apartment }}</span></li>
                                    </ul>
                                    <ul class="mansion__features">
                                        @foreach($pentComplexes->tags as $tag)
                                            <li>{{ $tag->tag_name }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="mansion__info-bottom">
                                    <p class="mansion__size">{{ $pentComplexes->square }} м<sup>2</sup>
                                    @if($pentComplexes->terrace == true)+ терраса {{ $pentComplexes->square_terrace }} м<sup>2</sup></p>@endif
                                    <p class="mansion__price">{{ $pentComplexes->full_price_ru }} &#8381;</p>
                                    <div class="mansion__info-about">
                                        <p class="mansion__metre">328 000 р./м<sup>2</sup></p>
                                        <div class="mansion__more"><a href="{{ route('catalog.penthouse',[$pentComplexes->complex_slug, $pentComplexes->penthouse_slug]) }}">Подробнее</a></div>
                                    </div>
                                </div>
                                <a class="mansion__link" href="{{ route('catalog.penthouse',[$pentComplexes->complex_slug, $pentComplexes->penthouse_slug]) }}">Назначить осмотр</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="house house--line sec js-house-profile">
        <div class="house__inner">
            <header class="house__header">
                <h2 class="house__chapter">Похожие пентхаусы</h2>
                <a class="house__all" href="#">Показать все предложения</a>
            </header>
            <div class="house__list swiper-container">
                <button class="swiper-button-prev"></button>
                <button class="swiper-button-next"></button>
                <div class="swiper-wrapper">
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-1.png') }}" alt="fireplace-1" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус в ЖК <br> "ГРАН ПРИ"</h3>
                            <div class="house__about">
                                <p class="house__area">665 м<sup>2</sup> + терраса 60 м<sup>2</sup></p>
                                <p class="house__price">648 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-2.png') }}" alt="fireplace-2" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус ЖК <br> "АНДРЕЕВСКИЙ"</h3>
                            <div class="house__about">
                                <p class="house__area">85 м<sup>2</sup> + терраса 90 м<sup>2</sup></p>
                                <p class="house__price">675 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-3.png') }}" alt="fireplace-3" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус В ЖК <br> "ДОМ СО ЛЬВАМИ"</h3>
                            <div class="house__about">
                                <p class="house__area">165 м<sup>2</sup> + терраса 120 м<sup>2</sup></p>
                                <p class="house__price">90 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-4.png') }}" alt="fireplace-4" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус В ЖК <br> "BOLSHEVIK"</h3>
                            <div class="house__about">
                                <p class="house__area">112 м<sup>2</sup> + терраса 40 м<sup>2</sup></p>
                                <p class="house__price">874 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-5.png') }}" alt="fireplace-5" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус В ЖК <br> "КУТУЗОВСКИЙ, 12"</h3>
                            <div class="house__about">
                                <p class="house__area">450 м<sup>2</sup> + терраса 10 м<sup>2</sup></p>
                                <p class="house__price">761 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    @include('layouts.modules.catalog')

    @include('layouts.modules.features')


@endsection

@section('mansion-swiper')
    @parent
    <div class="show">
        <button class="show__close"><span class="visually-hidden">Закрыть модальное окно</span></button>
        <form class="main-form" action="{{ route('forms.feedback') }}" method="POST">
            @csrf
            @if ($errors->any())
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="main-form--modal__wrapper">
                <h2 class="main-form__title main-form__title--show">Заявка на осмотр</h2>
                <div class="main-form__inner">
                    <input class="main-form__input" name="name" type="text" placeholder="Ваше имя">
                    <input class="main-form__input" name="phone" type="tel" placeholder="Номер телефона">
                    <input class="main-form__input" name="email" type="email" placeholder="E-mail">il">
                    <textarea class="main-form__textarea" name="textarea" cols="30" rows="10" placeholder="Комментарий к заявке. Здесь вы можете указать желаемое время звонки или другую важную информацию."></textarea>
                    <button class="main-form__btn" type="submit">Отправить</button>
                    <div class="main-form__agree">
                        <label class="main-form__agree-label">
                            <input type="checkbox" name="check">
                            <span class="main-form__agree-check"></span>
                            <span class="main-form__agree-text">Соглашаюсь с политикой конфиденциальности.</span>
                        </label>
                    </div>
                </div>
            </div>
        </form>

        <div class="show__map">
            <div class="show__house">
                <div class="show__house-img">
                    <img src="{{ asset('images/show/show-img.png') }}" alt="пентхаус">
                </div>
                <div class="show__house-info">
                    <h2 class="show__house-title">ЖК "Садовые кварталы"</h2>
                    <ul class="order__list show__house-list">
                        <li class="order__item">
                            <span class="order__class">Пентхаусов</span>
                            <span class="order__type">0</span>
                        </li>
                        <li class="order__item">
                            <span class="order__class">Площадь, от</span>
                            <span class="order__type">85 м<sup>2</sup></span>
                        </li>
                        <li class="order__item">
                            <span class="order__class">Цена, от</span>
                            <span class="order__type">47 000 000 &#8381;</span>
                        </li>
                    </ul>
                    <div class="show__house-more">
                        <a href="#">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/showModal.js')}}"></script>
    <script src="{{ asset('js/profileScroll.js') }}"></script>
@endsection
