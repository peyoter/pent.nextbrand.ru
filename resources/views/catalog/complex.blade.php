@extends('layouts.layout-2')
@section('page-title', $complex->complex_title)
@section('breadcrumbs', Breadcrumbs::render('catalog.complex', $complex))

@section('content')

    <div class="gallery">
        <h1 class="gallery__title main-title">{{$complex->complex_title}}</h1>
        <div class="gallery__list swiper-container">
            <button class="swiper-button-prev"></button>
            <button class="swiper-button-next"></button>
            <div class="swiper-wrapper">
            @foreach($complex->complex_img as $com_img)
                <a href="#" class="gallery__item swiper-slide">
                    <div class="gallery__img-tel">
                        <img src="{{ asset($complex->fit($com_img, 306, 124)) }}" alt='ЖК "БАРКЛИ ГАЛЕРИ"'>
                    </div>
                    <div class="gallery__img-dekstop">
                        <img src="{{ asset($complex->fit($com_img, 1205, 677)) }}" alt='ЖК "БАРКЛИ ГАЛЕРИ"'>
                    </div>
                </a>
            @endforeach
            </div>
        </div>
    </div>

    <div class="barkli">
        <aside class="order">
            <div class="order__show">
                <p class="order__address">{{$complex->complex_address}}</p>
                <a class="order__link" href="#">Показать на карте</a>
            </div>
            <div class="order__info">
                <a class="order__inspection form-toggle" href="#">Заказать осмотр</a>
                <ul class="order__list">
                    <li class="order__item">
                        <span class="order__class">Класс</span>
                        <span class="order__type">{{ $complex->class }}</span>
                    </li>
                    <li class="order__item">
                        <span class="order__class">Готовность</span>
                        <span class="order__type">{{$complex->delivery}}</span>
                    </li>
                    <li class="order__item">
                        <span class="order__class">Инфраструктура</span>
                        <span class="order__type">{{$complex->infrastructure}}</span>
                    </li>
                    <li class="order__item">
                        <span class="order__class">Класс</span>
                        <span class="order__type">{{ $complex->class }}</span>
                    </li>
                    <li class="order__item">
                        <span class="order__class">Готовность</span>
                        <span class="order__type">{{$complex->delivery}}</span>
                    </li>
                    <li class="order__item">
                        <span class="order__class">Инфраструктура</span>
                        <span class="order__type">{{$complex->infrastructure}}</span>
                    </li>
                </ul>
            </div>
        </aside>
        <div class="mansion__page barkli__page">
            <section class="barkli__info">
                <h2 class="visually-hidden">Информация о {{$complex->complex_title}}"</h2>
                <p class="barkli__text">{{$complex->complex_desc}}</p>
            </section>
            <h2 class="barkli__title sec-title">Пентхаусы в {{$complex->complex_title}}</h2>
            <header class="mansion__header">
                <div class="mansion__sort">
                    <p class="mansion__sort-num">Пентхаусов найдено: <span>{{$complex->penthouses_count}}</span></p>
                    <p class="mansion__sort-type">Сортировать по:
                    @if($request->price == 'asc')
                        <a class="mansion__sort-type__btn" href="{{ $request->fullUrlWithQuery(['price' => 'desc']) }}">Цене, сначала дорогие</a>
                    @else
                        <a class="mansion__sort-type__btn" href="{{ $request->fullUrlWithQuery(['price' => 'asc']) }}">Цене, сначала дешевле</a>
                    @endif
                    </p>
                </div>
            </header>
            <ul class="mansion__kind">
            @foreach($pents as $penthouse)
                @foreach($penthouse->tags as $tag)
                    <li class="flat__kind-item"><a class="mansion__kind-btn" href="#">{{ $tag->tag_name }}</a></li>
                @endforeach
            @endforeach
            </ul>
            <div class="mansion__house-wrapper">
            @foreach($pents as $penthouse)
                <div class="mansion__house barkli__house">
                    <div class="mansion__img">
                        <img src="{{ asset($penthouse->fit($penthouse->penthouse_main_img, 600, 428)) }}" alt="пентхаус">
                    </div>
                    <div class="mansion__info">
                        <div class="mansion__info-top">
                            <h2 class="mansion__title">{{ $penthouse->penthouse_title }}</h2>
                            <p class="mansion__street">{{ $complex->complex_address }}</p>
                            <ul class="mansion__content">
                                <li>{{ $penthouse->rooms }} <span>спальни</span></li>
                                <li>Отделка <span>{{ $penthouse->apartment }}</span></li>
                            </ul>
                            <ul class="mansion__features">
                                @foreach($penthouse->tags as $tag)
                                    <li>{{ $tag->tag_name }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="mansion__info-bottom">
                            <p class="mansion__size">{{ $penthouse->square }} м<sup>2</sup>
                                @if($penthouse->terrace == true)
                                    + терраса {{ $penthouse->square_terrace }} м<sup>2</sup></p>
                                @endif
                            <p class="mansion__price">{{ $penthouse->full_price_ru }} &#8381;</p>
                            <div class="mansion__info-about">
                                <p class="mansion__metre">328 000 р./м<sup>2</sup></p>
                                <div class="mansion__more"><a href="{{ route('catalog.penthouse',[$complex->complex_slug, $penthouse->penthouse_slug]) }}">Подробнее</a></div>
                            </div>
                        </div>
                        <a class="mansion__link" href="{{ route('catalog.penthouse',[$complex->complex_slug, $penthouse->penthouse_slug]) }}">Назначить осмотр</a>
                    </div>
                </div>
            @endforeach
            </div>
            <div class="mansion__map" id="map">
            </div>
        </div>
    </div>

    <section class="house sec barkli-house">
        <div class="house__inner">
            <header class="house__header">
                <h2 class="house__chapter">Вам понравятся эти пентхаусы</h2>
                <a class="house__all" href="#">Показать все предложения</a>
            </header>
            <div class="house__list swiper-container">
                <button class="swiper-button-prev"></button>
                <button class="swiper-button-next"></button>
                <div class="swiper-wrapper">
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-1.png') }}" alt="fireplace-1" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус в ЖК <br> "ГРАН ПРИ"</h3>
                            <div class="house__about">
                                <p class="house__area">665 м<sup>2</sup> + терраса 60 м<sup>2</sup></p>
                                <p class="house__price">648 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-2.png') }}" alt="fireplace-2" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус ЖК <br> "АНДРЕЕВСКИЙ"</h3>
                            <div class="house__about">
                                <p class="house__area">85 м<sup>2</sup> + терраса 90 м<sup>2</sup></p>
                                <p class="house__price">675 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-3.png') }}" alt="fireplace-3" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус В ЖК <br> "ДОМ СО ЛЬВАМИ"</h3>
                            <div class="house__about">
                                <p class="house__area">165 м<sup>2</sup> + терраса 120 м<sup>2</sup></p>
                                <p class="house__price">90 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-4.png') }}" alt="fireplace-4" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус В ЖК <br> "BOLSHEVIK"</h3>
                            <div class="house__about">
                                <p class="house__area">112 м<sup>2</sup> + терраса 40 м<sup>2</sup></p>
                                <p class="house__price">874 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                    <a class="house__item swiper-slide" href="#">
                        <div class="house__img">
                            <img src="{{ asset('images/fireplace/fireplace-5.png') }}" alt="fireplace-5" width="208" height="165">
                        </div>
                        <div class="house__info">
                            <h3 class="house__title">Пентхаус В ЖК <br> "КУТУЗОВСКИЙ, 12"</h3>
                            <div class="house__about">
                                <p class="house__area">450 м<sup>2</sup> + терраса 10 м<sup>2</sup></p>
                                <p class="house__price">761 000 000 &#8381;</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    @include('layouts.modules.catalog')

    @include('layouts.modules.features')

@endsection
