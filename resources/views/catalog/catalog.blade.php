@extends('layouts.layout-2')
@section('page-title', 'Каталог пентхаусов')
@section('breadcrumbs', Breadcrumbs::render('catalog'))

@section('content')

    <div class="mansion">
        <h1 class="mansion__chapter main-title">Каталог пентхаусов</h1>
        <div class="mansion__inner">
            <form class="mansion__choose choose" action="{{ route('catalog') }}" method="GET">
                <div class="choose__manage">
                    <a class="choose__listing-btn active" href="#">Списком</a>
                    <a class="choose__map choose__map-tel" href="#">На карте</a>
                    <a class="choose__map choose__map-dekstop" href="#">На карте</a>
                    <a class="choose__parameter" href="#">Параметры объектов</a>
                    <button class="choose__reset" type="reset">
                        <span class="visually-hidden">Сбросить параметры</span>
                    </button>
                </div>
                <div class="choose__list">
                    <div class="choose__price">
                        <div class="choose__price-header">
                            <p class="choose__title">Цена</p>
                            <div class="choose__price-list">
                                <a class="choose__price-currency active" href="#">руб</a>
                                <a class="choose__price-currency" href="#">usd</a>
                                <a class="choose__price-currency" href="#">eur</a>
                            </div>
                        </div>
                        <div class="choose__price-range__wrapper">
                            <div class="choose__price-range range" id="penthousePrice"></div>
                            <p class="range__value-wrapper">
                                <input class="range__value" type="text" id="penthouseAmount" name="priceMin" readonly>
                                <input class="range__value" type="text" id="penthouseAmount2" name="priceMax" readonly>
                            </p>
                        </div>
                    </div>
                    <div class="choose__bedroom">
                        <p class="choose__title">Количество спален:</p>
                        <div class="choose__bedroom-inner">
                            <label>
                                <input type="radio" name="bedroomNum" value="1">
                                <span class="choose__bedroom-radio">1</span>
                            </label>
                            <label>
                                <input type="radio" name="bedroomNum" value="2">
                                <span class="choose__bedroom-radio">2</span>
                            </label>
                            <label>
                                <input type="radio" name="bedroomNum" value="3">
                                <span class="choose__bedroom-radio">3</span>
                            </label>
                            <label>
                                <input type="radio" name="bedroomNum" value="4">
                                <span class="choose__bedroom-radio">4</span>
                            </label>
                            <label>
                                <input type="radio" name="bedroomNum" value="5+">
                                <span class="choose__bedroom-radio">5+</span>
                            </label>
                        </div>
                    </div>
                    <div class="choose__style">
                        <label class="choose__style-label checkbox">
                            <input type="radio" name="chooseStyle" value="С отделкой">
                            <span class="checkbox__check"></span>
                            <span class="checkbox__text">С отделкой</span>
                        </label>
                        <label class="choose__style-label checkbox">
                            <input type="radio" name="chooseStyle" value="Без отделки">
                            <span class="checkbox__check"></span>
                            <span class="checkbox__text">Без отделки</span>
                        </label>
                        <label class="choose__style-label checkbox">
                            <input type="radio" name="chooseStyle" value="Whitebox">
                            <span class="checkbox__check"></span>
                            <span class="checkbox__text">Whitebox</span>
                        </label>
                        <label class="choose__style-label checkbox">
                            <input type="radio" name="chooseStyle" value="С мебелью">
                            <span class="checkbox__check"></span>
                            <span class="checkbox__text">С мебелью</span>
                        </label>
                    </div>
                    <div class="choose__size">
                        <p class="choose__title">Площадь пентхауса:</p>
                        <div class="choose__size-range__wrapper">
                            <div class="choose__size-range range" id="sizeRange"></div>
                            <p class="range__value-wrapper">
                                <input class="range__value" type="text" id="sizeAmount" name="squareMin" readonly>
                                <input class="range__value" type="text" id="sizeAmount2" name="squareMax" readonly>
                            </p>
                        </div>
                    </div>
                    <div class="choose__terrace">
                        <p class="choose__title">Площадь террасы:</p>
                        <div class="choose__terrace-range__wrapper">
                            <div class="choose__terrace-range range" id="terraceRange"></div>
                            <p class="range__value-wrapper">
                                <input class="range__value" type="text" id="terraceAmount" name="terraceMin" readonly>
                                <input class="range__value" type="text" id="terraceAmount2" name="terraceMax" readonly>
                            </p>
                        </div>
                        <div class="choose__terrace-without">
                            <label class="checkbox">
                                <input type="checkbox" name="terraceBool" value="1">
                                <span class="checkbox__check"></span>
                                <span class="checkbox__text">Без терассы</span>
                            </label>
                        </div>
                    </div>
                    <div class="choose__about">
                        <div class="choose__about-inner">
                            <a class="choose__about-btn" href="#">Расположение</a>
                            <ul class="choose__about-list">
                                @foreach($complexes as $complexOption)
                                <li class="choose__about-item">
                                    <label class="choose__about-checkbox checkbox">
                                        <input type="checkbox" name="location[]" value="{{ $complexOption->location }}">
                                        <span class="checkbox__check"></span>
                                        <span class="choose__about-text checkbox__text">{{ $complexOption->location }}<span class="choose__about-num"> - {{ $complexOption->penthouses_count }}</span></span>
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="choose__about-inner">
                            <a class="choose__about-btn" href="#">Жилые комплексы</a>
                            <ul class="choose__about-list">
                                @foreach($complexes as $complexOption)
                                <li class="choose__about-item">
                                    <label class="choose__about-checkbox checkbox">
                                        <input type="checkbox" name="complex[]" value="{{ $complexOption->id }}">
                                        <span class="checkbox__check"></span>
                                        <span class="choose__about-text checkbox__text">{{ $complexOption->complex_title }}<span class="choose__about-num"> - {{ $complexOption->penthouses_count }}</span></span>
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <button class="choose__reset-btn" type="reset">Очистить</button>
                    <button class="choose__reset-btn" type="submit">Поиск</button>
                </div>
            </form>
            <div class="mansion__page">
                <header class="mansion__header">
                    <div class="mansion__sort">
                        <p class="mansion__sort-num">Пентхаусов найдено: <span>{{ $penthouses->total() }}</span></p>
                        <p class="mansion__sort-type">Сортировать по:
                            @if($request->price == 'asc')
                                <a class="mansion__sort-type__btn" href="{{ $request->fullUrlWithQuery(['price' => 'desc']) }}" name="price">Цене, сначала дорогие</a>
                            @else
                                <a class="mansion__sort-type__btn" href="{{ $request->fullUrlWithQuery(['price' => 'asc']) }}" name="price">Цене, сначала дешевле</a>
                            @endif
                        </p>
                    </div>
                </header>
                <ul class="mansion__kind">
                    @foreach($tags as $tag)
                        <li class="mansion__kind-item"><a class="mansion__kind-btn" href="{{ $request->fullUrlWithQuery(['tags' => $tag->tag_name]) }}" name="tags">{{ $tag->tag_name }}</a></li>
                    @endforeach
                </ul>
                <div class="mansion__house-wrapper">
                    @foreach($penthouses as $penthouse)
                    <div class="mansion__house mansion__house--catalog">
                        <div class="mansion__img"><img src="{{asset($penthouse->fit($penthouse->penthouse_main_img, 700, 500))}}" alt="пентхаус"></div>
                        <div class="mansion__info">
                            <div class="mansion__info-top">
                                <h2 class="mansion__title">{{ $penthouse->penthouse_title }}</h2>
                                <p class="mansion__street">{{ $penthouse->complex_address }}</p>
                                <ul class="mansion__content">
                                    <li>{{ $penthouse->rooms }} <span>спальни</span></li>
                                    <li>Отделка <span>{{ $penthouse->apartment }}</span></li>
                                </ul>
                                <ul class="mansion__features">
                                    @foreach($penthouse->tags as $tag)
                                    <li>{{ $tag->tag_name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="mansion__info-bottom">
                                <p class="mansion__size">{{ $penthouse->square }} м<sup>2</sup>
                                    @if($penthouse->terrace)
                                    + терраса {{ $penthouse->square_terrace }} м<sup>2</sup></p>
                                    @endif
                                <p class="mansion__price">{{ $penthouse->full_price_ru }} &#8381;</p>
                                <div class="mansion__info-about">
                                    <p class="mansion__metre">328 000 р./м<sup>2</sup></p>
                                    <div class="mansion__more"><a href="{{ route('catalog.penthouse',[$penthouse->complex_slug, $penthouse->penthouse_slug]) }}">Подробнее</a></div>
                                </div>
                            </div>
                            <a class="mansion__link" href="{{ route('catalog.penthouse',[$penthouse->complex_slug, $penthouse->penthouse_slug]) }}">Назначить осмотр</a>
                        </div>
                    </div>
                    @endforeach
                    {{ $penthouses->appends(['priceMin' => request()->priceMin,'priceMax' => request()->priceMax,'bedroomNum' => request()->bedroomNum,
'chooseStyle' => request()->chooseStyle,'squareMin' => request()->squareMin,'squareMax' => request()->squareMax,'terraceMin' => request()->terraceMin,
'terraceMax' => request()->terraceMax,'terraceBool' => request()->terraceBool,'location' => request()->location,'complex' => request()->complex,
'price' => request()->price, 'tags' => request()->tags])
                        ->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>

    @include('layouts.modules.catalog')

    @include('layouts.modules.features')
    <script>
        ymaps.ready(init);

        function init () {
            var myMap = new ymaps.Map('map', {
                    center: [55.76, 37.64],
                    zoom: 10
                }, {
                    searchControlProvider: 'yandex#search'
                }),
                objectManager = new ymaps.ObjectManager({
                    // Чтобы метки начали кластеризоваться, выставляем опцию.
                    clusterize: true,
                    // ObjectManager принимает те же опции, что и кластеризатор.
                    gridSize: 32,
                    clusterDisableClickZoom: true
                });

            // Чтобы задать опции одиночным объектам и кластерам,
            // обратимся к дочерним коллекциям ObjectManager.
            objectManager.objects.options.set('preset', 'islands#greenDotIcon');
            objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
            myMap.geoObjects.add(objectManager);

            $.ajax({
                url: "data.json"
            }).done(function(data) {
                objectManager.add(data);
            });

        }
    </script>
@endsection
