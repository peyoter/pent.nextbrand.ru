<?php

namespace App\Models;

use App\Http\Traits\ImageResizer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Penthouse extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ImageResizer;

    public $casts = [
        'penthouse_img' => 'array',
        'complex_img' => 'array',
    ];

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function link(): string
    {
        return $link = 'gallery';
    }

    public function tags(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function employee(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Employee::class);
    }

    public function complex(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Complex::class);
    }

    public function apartment(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Apartment::class);
    }

    public function getPenthouse($complex_slug, $penthouse_slug): \Illuminate\Database\Eloquent\Builder
    {
        return Penthouse::query()->join('complexes', 'complexes.id', '=', 'penthouses.complex_id')
            ->join('apartments', 'apartments.id', '=', 'penthouses.apartment_id')
            ->join('employees','employees.id', '=', 'penthouses.employee_id')
            ->select('penthouses.id', 'penthouse_slug', 'penthouse_title', 'penthouse_desc', 'full_price_ru',
                'square', 'ceiling', 'floor', 'rooms', 'bedrooms', 'bathrooms', 'penthouse_id', 'terrace', 'square_terrace',
                'concierge_service', 'penthouse_main_img', 'penthouse_img', 'penthouse_plan', 'complex_id', 'complexes.id as complexes_id',
                'complex_slug', 'complex_title', 'complex_desc', 'complex_address', 'delivery', 'infrastructure',
                'complex_main_img', 'complex_img', 'apartments.apartment_type as apartment', 'employees.id as employees_id', 'first_name',
                'last_name', 'emp_email','emp_phone','emp_desc','emp_img')
            ->where('complex_slug', $complex_slug)
            ->where('penthouse_slug', 'LIKE', $penthouse_slug);
    }

    public function getPentComplex($complex_slug): \Illuminate\Database\Eloquent\Builder
    {
        return Penthouse::query()->join('complexes', 'complexes.id', '=', 'penthouses.complex_id')
            ->join('apartments', 'apartments.id', '=', 'penthouses.apartment_id')
            ->select('penthouses.id','penthouse_slug', 'penthouse_title', 'full_price_ru', 'square', 'rooms', 'square_terrace',
                'penthouse_main_img', 'terrace','apartments.apartment_type as apartment', 'complex_id', 'complexes.id as complexes_id',
                'complex_slug')
            ->where('complex_slug', $complex_slug);
    }
}
