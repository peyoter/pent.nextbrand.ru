<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Search extends Model
{
    use HasFactory;

    public function link(): string
    {
        return $link = 'search';
    }

    public function search(Request $request): array
    {
        $search = $request->search;

        $searchComplex = Complex::query()
            ->select('id','complex_slug', 'complex_title')
            ->where('complex_title', 'LIKE', "%{$search}%")
            ->orderBy('complex_title')
            ->get()
            ->map(function ($row) use ($search) {
                $row->complex_title = preg_replace('/(' . $search . ')/i', "<span>$1</span>", $row->complex_title);
                return $row;
            });

        $searchPenthouse = Penthouse::query()->join('complexes', 'complexes.id', '=', 'penthouses.complex_id')
            ->select('penthouses.id','penthouse_slug', 'penthouse_title', 'complexes.complex_slug')
            ->where('penthouse_title', 'LIKE', "%{$search}%")
            ->orderBy('penthouse_title')
            ->get()
            ->map(function ($row) use ($search) {
                $row->penthouse_title = preg_replace('/(' . $search . ')/i', "<span>$1</span>", $row->penthouse_title);
                return $row;
            });

        return ['searchComplex' => $searchComplex,'searchPenthouse' => $searchPenthouse];
    }
}
