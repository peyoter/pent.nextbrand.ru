<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Apartment extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function penthouses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Penthouse::class);
    }
}
