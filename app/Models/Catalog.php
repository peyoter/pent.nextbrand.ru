<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    use HasFactory;

    public function link(): string
    {
        return $link = 'catalog';
    }

    public function getTagName(): \Illuminate\Database\Eloquent\Builder
    {
        return Tag::query()->select('tag_name');
    }
    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function complexCount(): \Illuminate\Database\Eloquent\Builder
    {
        return Complex::query()->join('complex_location', 'complex_location.complex_id', '=', 'complexes.id')
            ->join('locations','complex_location.location_id', '=', 'locations.id')
            ->select('complexes.id','complex_title','locations.location')
            ->withCount('penthouses');
    }

    public function getPenthouses(): \Illuminate\Database\Eloquent\Builder
    {
        return Penthouse::query()->with('apartment')
            ->join('complexes', 'complexes.id', '=', 'penthouses.complex_id')
            ->join('complex_location', 'complex_location.complex_id', '=', 'complexes.id')
            ->join('locations','complex_location.location_id', '=', 'locations.id')
            ->select('penthouses.id', 'penthouse_slug', 'penthouse_title', 'full_price_ru', 'square', 'rooms', 'terrace',
                'square_terrace', 'penthouse_main_img', 'penthouse_img', 'penthouses.complex_id','complexes.id as complexes_id',
                'complex_slug', 'complex_title', 'complex_desc', 'complex_address', 'delivery', 'infrastructure',
                'complex_main_img', 'complex_img', 'locations.location as location');
    }
}
