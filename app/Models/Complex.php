<?php

namespace App\Models;

use App\Http\Traits\ImageResizer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Complex extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ImageResizer;

    public $casts = [
        'complex_img' => 'array',
    ];

    public function penthouses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Penthouse::class);
    }

    public function classifications(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Classification::class);
    }

    public function parkings(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Parking::class);
    }

    public function metros(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Metro::class);
    }

    public function districts(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(District::class);
    }

    public function locations(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Location::class);
    }

    public function link(): string
    {
        return $link = 'barkli';
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function getComplex($complex_slug): \Illuminate\Database\Eloquent\Builder
    {
        return Complex::query()->join('classification_complex', 'classification_complex.complex_id', '=', 'complexes.id')
            ->join('classifications','classification_complex.classification_id', '=', 'classifications.id')
            ->select('complexes.id','complex_slug', 'complex_title','complex_desc', 'complex_address', 'delivery',
                'infrastructure', 'complex_main_img', 'complex_img', 'classifications.classification as class')
            ->where('complex_slug', $complex_slug)
            ->withCount('penthouses')
            ->withMin('penthouses', 'full_price_ru');
    }

    public function getComplexPent($id): \Illuminate\Database\Eloquent\Builder
    {
        return Penthouse::query()->join('apartments', 'apartments.id', '=', 'penthouses.apartment_id')
            ->select('penthouses.id','penthouse_slug','penthouse_title', 'penthouse_desc', 'rooms', 'full_price_ru', 'square', 'terrace', 'square_terrace',
                'penthouse_main_img', 'complex_id', 'apartments.apartment_type as apartment')
            ->where('complex_id', $id);
    }
}
