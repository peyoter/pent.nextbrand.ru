<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Index extends Model
{
    use HasFactory;

    protected int $getLimit = 5;

    protected function getQueryPent(): \Illuminate\Database\Eloquent\Builder
    {
        return Penthouse::query()
            ->join('complexes', 'complexes.id', '=', 'penthouses.complex_id')
            ->select('penthouse_slug','penthouse_title', 'full_price_ru', 'square', 'terrace', 'square_terrace', 'penthouse_main_img',
                'complex_id','complexes.id as complexes_id','complex_slug');
    }

    public function randomPenthouses(): \Illuminate\Database\Eloquent\Builder
    {
        return $this->getQueryPent()
            ->inRandomOrder()
            ->limit($this->getLimit);
    }

    public function terracePenthouses(): \Illuminate\Database\Eloquent\Builder
    {
        return $this->getQueryPent()
            ->where('terrace', true)
            ->inRandomOrder()
            ->limit($this->getLimit);
    }

    public function fireplacePenthouses(): \Illuminate\Database\Eloquent\Builder
    {
        return $this->getQueryPent()
            ->where('fireplace', true)
            ->inRandomOrder()
            ->limit($this->getLimit);
    }

    public function twostoreyPenthouses(): \Illuminate\Database\Eloquent\Builder
    {
        return $this->getQueryPent()
            ->where('twostorey', true)
            ->inRandomOrder()
            ->limit($this->getLimit);
    }

    public function getComplex(): \Illuminate\Database\Eloquent\Builder
    {
        return Complex::query()
            ->join('classification_complex', 'classification_complex.complex_id', '=', 'complexes.id')
            ->join('classifications','classification_complex.classification_id', '=', 'classifications.id')
            ->select('complexes.*', 'classifications.classification as class')
            ->withCount('penthouses')
            ->withMin('penthouses', 'full_price_ru');
    }
}
