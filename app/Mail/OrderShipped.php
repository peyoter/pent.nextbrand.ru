<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $name;
    public $phone;
    public $email;
    public $textarea;

    public function __construct($name, $phone, $email, $textarea)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->email = $email;
        $this->textarea = $textarea;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): OrderShipped
    {
       return $this->from('youmail@gmail.com','TestFrom')
           ->view('emails.orders.shipped')
           ->subject('Test Sub');
    }
}
