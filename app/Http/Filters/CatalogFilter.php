<?php

namespace App\Http\Filters;

class CatalogFilter extends QueryFilter
{
    public function priceMin($value)
    {
        $value = (int)preg_replace('/\xA0/u', '',$value);
        $this->builder->where('full_price_ru', '>=', $value);
    }

    public function priceMax($value)
    {
        $value = (int)preg_replace('/\xA0/u', '',$value);
        $this->builder->where('full_price_ru', '<=', $value);
    }

    public function squareMin($value)
    {
        $value = (int)preg_replace('/\xA0/u', '',$value);
        $this->builder->where('square', '>=', $value);
    }

    public function squareMax($value)
    {
        $value = (int)preg_replace('/\xA0/u', '',$value);
        $this->builder->where('square', '<=', $value);
    }

    public function bedroomNum($value)
    {
        $value = (int)preg_replace('/\xA0/u', '',$value);
        if ($value <= 4)
            $this->builder->where('bedrooms', $value);
        else
            $this->builder->where('bedrooms', '>=', $value);
    }

    public function fireplace($value)
    {
        $this->builder->where('fireplace', $value);
    }

    public function twostorey($value)
    {
        $this->builder->where('twostorey', $value);
    }

    public function terraceBool($value)
    {
       $this->builder->where('terrace', $value);
    }

    public function terraceMin($value)
    {
        $value = (int)preg_replace('/\xA0/u', '',$value);
        $this->builder->where('square_terrace', '>=', $value);
    }

    public function terraceMax($value)
    {
        $value = (int)preg_replace('/\xA0/u', '',$value);
        $this->builder->where('square_terrace', '<=', $value);
    }
//        if ($request->isNotFilled('terraceBool')) {
//            if ($request->filled('terraceMin')) {
//                $querySelect->where('square_terrace', '>=', $terraceMin);
//            }
//
//            if ($request->filled('terraceMax')) {
//                $querySelect->where('square_terrace', '<=', $terraceMax);
//            }
//        }
//        else {
//            $querySelect->where('terrace', $terraceBool);
//        }
    public function chooseStyle($value)
    {
        $this->builder->whereHas('apartment', function ($query) use ($value) {
            $query->where('apartment_type', 'LIKE', "$value");
        });
    }

    public function complex($value)
    {
        foreach ($value as $item)
            $this->builder->orWhere('penthouses.complex_id', $item);
    }

    public function location($value)
    {
        foreach ($value as $item)
            $this->builder->orWhere('locations.location', 'LIKE', "$item");
    }

    public function price($value)
    {
        if ($value == 'asc')
            $this->builder->orderBy('full_price_ru');
        elseif ($value == 'desc')
            $this->builder->orderByDesc('full_price_ru');
    }

    public function tags($value)
    {
        $this->builder->whereHas('tags', function ($query) use ($value) {
            $query->where('tag_name', 'LIKE', "$value");
        });
    }
}
