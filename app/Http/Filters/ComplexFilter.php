<?php

namespace App\Http\Filters;

class ComplexFilter extends QueryFilter
{
    public function price($value)
    {
        if ($value == 'asc')
            $this->builder->orderBy('full_price_ru');
        elseif ($value == 'desc')
            $this->builder->orderByDesc('full_price_ru');
    }

    public function tags($value)
    {
        $this->builder->whereHas('tags', function ($query) use ($value) {
            $query->where('tag_name', 'LIKE', "$value");
        });
    }
}
