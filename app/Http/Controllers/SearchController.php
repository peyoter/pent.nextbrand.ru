<?php

namespace App\Http\Controllers;

use App\Models\Search;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected function index(Request $request, Search $searchModel)
    {
        $link = $searchModel->link();
        $searching = $searchModel->search($request);

        return view('search', compact('link', 'request', 'searching'));
    }
}
