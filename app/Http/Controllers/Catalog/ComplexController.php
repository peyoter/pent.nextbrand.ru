<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Filters\PenthouseFilter;
use App\Models\Complex;
use Illuminate\Http\Request;

class ComplexController extends Controller
{
    public function index(Complex $complexModel, PenthouseFilter $filters, Request $request, $complex_slug)
    {
        $link = $complexModel->link();
        $complex = $complexModel->getComplex($complex_slug)->firstOrFail();
        $pents = $complexModel->getComplexPent($complex->id)->filter($filters)->get();

        return view('catalog.complex', compact('link', 'complex', 'pents', 'request'));
    }
}
