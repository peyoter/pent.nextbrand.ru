<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Filters\PenthouseFilter;
use App\Models\Penthouse;
use Illuminate\Http\Request;

class PenthouseController extends Controller
{
    public function index(Penthouse $penthouseModel, PenthouseFilter $filters, Request $request, $complex_slug, $penthouse_slug)
    {
        $link = $penthouseModel->link();
        $penthouse = $penthouseModel->getPenthouse($complex_slug, $penthouse_slug)->firstOrFail();
        $pentComplex = $penthouseModel->getPentComplex($complex_slug)->filter($filters)->get();

        return view('catalog.penthouse', compact(  'link','penthouse','pentComplex', 'request'));
    }
}
