<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Complex;
use App\Models\Index;
use App\Models\Penthouse;
use PDF;
use Image;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PdfController extends Controller
{
    public function index(Index $indexModel)
    {
//        ->generate('http://pent.nextbrand.ru/catalog/'. $complex_slug .'/'. $penthouse_slug .'')
        $qrCodeC = QrCode::encoding('UTF-8')->size(193);
        $qrCodeP = QrCode::encoding('UTF-8')->size(193);
        $complex = $indexModel->getComplex()->get();
        $data = [
            'qrCodeP' => $qrCodeP,
            'qrCodeC' => $qrCodeC,
            'complex' => $complex
        ];

        $pdf = PDF::loadView('pdf.pdf-index', compact('data'));
        return $pdf->stream('pdf-index.pdf');
    }

    public function complex(Complex $complexModel, $complex_slug)
    {
        $qrCode = QrCode::encoding('UTF-8')->size(193)->generate('http://pent.nextbrand.ru/catalog/'. $complex_slug .'');
        $complex = $complexModel->getComplex($complex_slug)->firstOrFail();
        $pents = $complexModel->getComplexPent($complex->id)->get();

        $data = [
            'qrCode' => $qrCode,
            'complex' => $complex,
            'pents' => $pents,
        ];

        $pdf = PDF::loadView('pdf.pdf-complex', compact('data'));
        return $pdf->stream('pdf-complex.pdf');
    }

    public function penthouse(Complex $complexModel, Penthouse $penthouseModel, $complex_slug, $penthouse_slug)
    {
        $qrCodeC = QrCode::encoding('UTF-8')->size(193)->generate('http://pent.nextbrand.ru/catalog/'. $complex_slug .'');
        $qrCodeP = QrCode::encoding('UTF-8')->size(193)->generate('http://pent.nextbrand.ru/catalog/'. $complex_slug .'/'. $penthouse_slug .'');
        $penthouse = $penthouseModel->getPenthouse($complex_slug, $penthouse_slug)->firstOrFail();
        $pents = $complexModel->getComplexPent($penthouse->complex->id)->get();

        $data = [
            'qrCodeP' => $qrCodeP,
            'qrCodeC' => $qrCodeC,
            'penthouse' => $penthouse,
            'pents' => $pents
        ];

        $pdf = PDF::loadView('pdf.pdf-penthouse', compact('data'));
        return $pdf->stream('pdf-penthouse.pdf');
    }

    public function image()
    {
        $img = Image::make(public_path('/images/barkli/barkli-1-tel.png'))->resize(300, 200);
        return $img->response('jpg');
    }
}
