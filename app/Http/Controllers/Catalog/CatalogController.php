<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Catalog;
use App\Http\Filters\CatalogFilter;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index(Catalog $catalogModel, CatalogFilter $filters, Request $request)
    {
        $link = $catalogModel->link();
        $tags = $catalogModel->getTagName()->get();
        $complexes = $catalogModel->complexCount()->get();
        $penthouses = $catalogModel->getPenthouses()->filter($filters)->paginate(6);

        return view('catalog.catalog', compact('link', 'penthouses', 'tags', 'complexes', 'request'));
    }
}
