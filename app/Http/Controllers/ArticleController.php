<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $link = 'article';
        $article = Article::first();
        return view('article', compact('link', 'article'));
    }
}
