<?php

namespace App\Http\Controllers;

use App\Models\Index;

class IndexController extends Controller
{
    public function index(Index $indexModel)
    {
        $link = 'index';
        $randomPenthouses = $indexModel->randomPenthouses()->get();
        $terracePenthouses = $indexModel->terracePenthouses()->get();
        $fireplacePenthouses = $indexModel->fireplacePenthouses()->get();
        $twostoreyPenthouses = $indexModel->twostoreyPenthouses()->get();

        return view('index', compact('link', 'randomPenthouses', 'terracePenthouses', 'fireplacePenthouses', 'twostoreyPenthouses'));
    }
}
