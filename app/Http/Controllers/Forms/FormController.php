<?php

namespace App\Http\Controllers\Forms;

use App\Http\Controllers\Controller;
use App\Mail\OrderShipped;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    public function feedback(Request $request)
    {
        $validation = $request->validate([
            'name' => 'required|min:2|max:30',
            'phone' => 'integer',
            'email' => 'required',
            'textarea' => 'min:5|max:500',
            'check' => 'required',
        ]);

        $name = $request->name;
        $phone = $request->phone;
        $email = $request->email;
        $textarea = $request->textarea;

        Mail::to(request('email'))
            ->send(new OrderShipped($name, $phone, $email, $textarea));

        return redirect('thanks');
    }
}
