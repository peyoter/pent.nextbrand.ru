<?php

namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Flat;

/**
 * Class District
 *
 * @property \App\Models\District $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class DistrictSection extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Список всех районов';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables()->setHtmlAttribute('class', 'table-primary')->paginate(10);

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            $country = AdminColumn::text('district', 'Районы')
                ->setHtmlAttribute('class', 'hidden-sm hidden-xs hidden-md'),
        ]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::card();

        $form->addBody([
            AdminFormElement::text('district', 'Название Района')->required(),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
