<?php

namespace App\Http\Sections;

use App\Models\Penthouse;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Flat;

/**
 * Class Penthouse
 *
 * @property Penthouse $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class PenthouseSection extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Пентхаусы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables()->setHtmlAttribute('class', 'table-primary')->paginate(10);

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            $country = AdminColumn::text('penthouse_title', 'Пентхаусы')
                ->setHtmlAttribute('class', 'hidden-sm hidden-xs hidden-md'),
        ]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::card();

        $form->addBody([
            AdminFormElement::text('penthouse_slug', 'slug')->required(),
            AdminFormElement::text('penthouse_title', 'Название')->required(),
            AdminFormElement::wysiwyg('penthouse_desc', 'Описание')->required(),
            AdminFormElement::number('full_price_ru', 'Цена в рублях')
                ->setMin(0)
                ->required(),
            AdminFormElement::number('full_price_eur', 'Цена в евро')
                ->setMin(0)
                ->required(),
            AdminFormElement::number('full_price_usd', 'Цена в долларах')
                ->setMin(0)
                ->required(),
            AdminFormElement::number('square', 'Площадь пентхауса')
                ->setMin(0)
                ->required(),
            AdminFormElement::number('ceiling', 'Высота потолков')
                ->setMin(2)
                ->required(),
            AdminFormElement::number('floor', 'Этаж')
                ->setMin(0)
                ->required(),
            AdminFormElement::number('rooms', 'Количество комнат')
                ->setMin(1)
                ->required(),
            AdminFormElement::number('bedrooms', 'Количество спален')
                ->setMin(1)
                ->required(),
            AdminFormElement::number('bathrooms', 'Количество санузлов')
                ->setMin(1)
                ->required(),
            AdminFormElement::number('penthouse_id', 'ID')
                ->setMin(0)
                ->required(),
            AdminFormElement::number('square_terrace', 'Площадь террасы')->setMin(0),
            AdminFormElement::checkbox('terrace', 'Наличие террасы'),
            AdminFormElement::checkbox('concierge_service', 'Консьерж-сервис'),
            AdminFormElement::checkbox('fireplace', 'Наличие камина'),
            AdminFormElement::checkbox('twostorey', 'Наличие второго этажа'),
            AdminFormElement::checkbox('authors_design', 'Авторский дизайн'),
            AdminFormElement::image('penthouse_main_img', 'Главное изображение'),
            AdminFormElement::image('penthouse_plan', 'Изображение планировки'),
            AdminFormElement::images('penthouse_img', 'Галерея изображений'),
            AdminFormElement::number('coordinates_x', 'Координаты X')
                ->required(),
            AdminFormElement::number('coordinates_y', 'Координаты Y')
                ->required(),
            AdminFormElement::select('complex_id', 'Комплекс', \App\Models\Complex::class)
                ->setDisplay('complex_title')
                ->required(),
            AdminFormElement::select('employee_id', 'Риелтор', \App\Models\Employee::class)
                ->setDisplay('first_name')
                ->required(),
            AdminFormElement::select('apartment_id', 'Отделка', \App\Models\Apartment::class)
                ->setDisplay('apartment_type')
                ->required(),
            AdminFormElement::multiselect('tags', 'Теги', \App\Models\Tag::class)->setDisplay('tag_name'),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @param $id
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @param $id
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
