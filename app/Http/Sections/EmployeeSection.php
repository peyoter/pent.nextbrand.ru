<?php

namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Flat;

/**
 * Class Employee
 *
 * @property \App\Models\Employee $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class EmployeeSection extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Риелторы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables()->setHtmlAttribute('class', 'table-primary')->paginate(10);

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            $country = AdminColumn::text('first_name', 'Имя')
                ->setHtmlAttribute('class', 'hidden-sm hidden-xs hidden-md'),
            $country = AdminColumn::text('last_name', 'Фамилия')
                ->setHtmlAttribute('class', 'hidden-sm hidden-xs hidden-md'),
        ]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::card();

        $form->addBody([
            AdminFormElement::text('first_name', 'Имя')->required(),
            AdminFormElement::text('last_name', 'Фамилия')->required(),
            AdminFormElement::text('emp_email', 'E-mail')
                ->required()
                ->addValidationRule('email'),
            AdminFormElement::text('emp_phone', 'Телефон риелтора'),
            AdminFormElement::wysiwyg('emp_desc', 'Описание'),
            AdminFormElement::image('emp_img', 'Изображение'),
            AdminFormElement::multiselect('penthouses', 'Пентхаусы', \App\Models\Penthouse::class)
                ->setDisplay('penthouse_title'),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
