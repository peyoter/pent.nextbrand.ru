<?php

namespace App\Http\Traits;

trait EnvyBoxTrait
{

    function sendToSrm($name = '', $phone ='', $email= '', $msg = '', $type ='', $object=''){
        $link = 'https://aandp.envycrm.com/crm/api/v1/lead/set/?api_key=5a0a49da44328e467e264eafa24da88553bc349d';
        $curl = curl_init();

        $data = [
            'method' => 'create', // метод, 'create' - для создания, 'update' - для обновления, в данном случае использовать нет необходимости
//                    'pipeline_id' => 0000, // id воронки (не обязательное поле, если не указано используется воронка по-умолчанию)
//                    'stage_id' => 0000, // id этапа (не обязательное поле, если не указано используется этап воронки по-умолчанию)
//            'employee_id' => 524654, // id ответственного сотрудника (не обязательное поле, если не указано, то ответственный не указывается, лид доступен всем, если значение 0, то "Администратор", если указан id сотрудника, то будет назначен сотрудник, чей id передан)
            'inbox_type_id' => 550851, // id типа входящего обращения
//                    'visit_id' => $_COOKIE['WhiteCallback_visit'], // идентификатор визита сервиса, будет присутствовать, если установлен js код наших виджетов, поле не обязательное, автоматически добавит информацию о посетителе, получается из Cookie
            'values' => [ // массив значений системных и произвольных полей
                'name' =>($name ? $name : 'Заявка с сайта ' . ($phone ? $phone : $email)), // имя
                'phone' => $phone, // телефон
                'email' => $email, // email
                'comment' => @$msg ? $msg : '', // примечание
                'utm_source' => '', // utm-метка utm_source
                'utm_medium' => '', // utm-метка utm_medium
                'utm_campaign' => '', // utm-метка utm_campaign
                'utm_content' => '', // utm-метка utm_content
//                'utm_term' => ''
                'custom' => [ // массив дополнительных полей
                    ['input_id' => 186537, 'value' => $type], // массив идентификатора поля и его значения Тут ПЕНТХАУС
                    ['input_id' => 245294, 'value' => $object] // массив идентификатора поля и его значения Тут ОБЪЕКТ
                ]
            ]
        ];

        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_POST,true);
        curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode(['request' => $data]));
        curl_setopt($curl,CURLOPT_HEADER,false);

        $out=curl_exec($curl);
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);
    }

}
