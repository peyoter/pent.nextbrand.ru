<?php

namespace App\Http\Traits;

use Image;


trait ImageResizer {

//    public function getImage($width = false, $height = false, $crop = false)
//    {
//
//        $image = $this->image;
//
//        $pp = pathinfo($image);
//
//        $public_path = public_path($pp['dirname'].'/thumbs/'.$pp['basename']);
//
//        $path = $pp['dirname'].'/thumbs/'.$pp['basename'];
//
//        if (is_file($public_path)) {
//            return $path;
//        }
//
//        if ($crop == 'height') {
//            $img = Image::make(public_path($image));
//
//            $p = $img->width() / $img->height();
//
//            $normalHeight = ceil($width / $p);
//
//            Image::make(public_path($image))
//                ->resize($width, $normalHeight)
//                ->crop($width, $height)
//                ->save($public_path);
//            return $path;
//        }
//
//        else if ($crop == 'width'){
//
//            $img = Image::make(public_path($image));
//
//            $p = $img->height() /  $img->width();
//
//            $normalWidth = ceil($height / $p);
//
//            Image::make(public_path($image))
//                ->resize($normalWidth, $height)
//                ->crop($width, $height)
//                ->save($public_path);
//
//            return $path;
//        }
//
//
//    }
//
//    public function fit($image, $width, $height, $watermark = false){
//
//        $pp = pathinfo($image);
//
//        if ($watermark == 'watermark') {
//            $wg = '';
//        } else if ($watermark == false) {
//            $wg = 'wmn';
//        } else {
//            $wg = $watermark;
//        }
//
//
//        if(!is_file(public_path($image))){
//            return $image;
//        }
//
//        $path = '/upload/thumbs/z'.$wg.$width.'x'.$height.'x'.md5($pp['basename'].$pp['dirname'].@$wg).'.'.@$pp['extension'];
//        $public_path = public_path($path);
//
//        if (is_file($public_path)) {
//            return $path;
//        } else {
//            $render = Image::make(public_path($image))->fit($width, $height);
//            if ($watermark) {
//                $render = $render->insert('img/watermark.png', 'center');
//            }
//            $render->save($public_path);
//            return $path;
//        }
//    }

    public function fit($image, $width, $height): string
    {
        $pp = pathinfo($image);

        if(!is_file(public_path($image))){
            return $image;
        }

        $path = 'images/uploads/thumbs/z'.$width.'x'.$height.'x'.md5($pp['basename'].$pp['dirname']).'.'.@$pp['extension'];
        $public_path = public_path($path);

        if (is_file($public_path)) {
            return $path;
        } else {
            $render = Image::make(public_path($image))->fit($width, $height);
            $render->save($public_path);
            return $path;
        }
    }
}
