<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\Models\User::class => 'App\Http\Sections\UserSection',
        \App\Models\Apartment::class => 'App\Http\Sections\ApartmentSection',
        \App\Models\Article::class => 'App\Http\Sections\ArticleSection',
        \App\Models\Classification::class => 'App\Http\Sections\ClassificationSection',
        \App\Models\Complex::class => 'App\Http\Sections\ComplexSection',
        \App\Models\District::class => 'App\Http\Sections\DistrictSection',
        \App\Models\Employee::class => 'App\Http\Sections\EmployeeSection',
        \App\Models\Location::class => 'App\Http\Sections\LocationSection',
        \App\Models\Metro::class => 'App\Http\Sections\MetroSection',
        \App\Models\Parking::class => 'App\Http\Sections\ParkingSection',
        \App\Models\Penthouse::class => 'App\Http\Sections\PenthouseSection',
        \App\Models\Tag::class => 'App\Http\Sections\TagSection',
    ];

    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
