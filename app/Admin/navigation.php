<?php

use SleepingOwl\Admin\Navigation\Page;

// Default check access logic
// AdminNavigation::setAccessLogic(function(Page $page) {
// 	   return auth()->user()->isSuperAdmin();
// });
//
// AdminNavigation::addPage(\App\User::class)->setTitle('test')->setPages(function(Page $page) {
// 	  $page
//		  ->addPage()
//	  	  ->setTitle('Dashboard')
//		  ->setUrl(route('admin.dashboard'))
//		  ->setPriority(100);
//
//	  $page->addPage(\App\User::class);
// });
//
// // or
//
// AdminSection::addMenuPage(\App\User::class)

return [
    [
        'title' => 'Пользователи',
        'icon'  => 'fas fa-users',
        'url'   => route('admin.model', 'users'),
    ],

    [
        'title' => 'Комплексы',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'complexes'),
    ],

    [
        'title' => 'Пентхаусы',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'penthouses'),
    ],
    [
        'title' => 'Риелторы',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'employees'),
    ],
    [
        'title' => 'Статьи',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'articles'),
    ],
    [
        'title' => 'Класс ЖК',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'classifications'),
    ],
    [
        'title' => 'Районы',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'districts'),
    ],
    [
        'title' => 'Расположения',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'locations'),
    ],
    [
        'title' => 'Метро',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'metros'),
    ],
    [
        'title' => 'Виды отделок',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'apartments'),
    ],
    [
        'title' => 'Виды парковки',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'parkings'),
    ],
    [
        'title' => 'Теги',
        'icon'  => 'fas fa-plus',
        'url'   => route('admin.model', 'tags'),
    ],
//    [
//        'title' => 'Information',
//        'icon'  => 'fas fa-info-circle',
//        'url'   => route('admin.information'),
//    ],

    // Examples
    // [
    //    'title' => 'Content',
    //    'pages' => [
    //
    //        \App\User::class,
    //
    //        // or
    //
    //        (new Page(\App\User::class))
    //            ->setPriority(100)
    //            ->setIcon('fas fa-users')
    //            ->setUrl('users')
    //            ->setAccessLogic(function (Page $page) {
    //                return auth()->user()->isSuperAdmin();
    //            }),
    //
    //        // or
    //
    //        new Page([
    //            'title'    => 'News',
    //            'priority' => 200,
    //            'model'    => \App\News::class
    //        ]),
    //
    //        // or
    //        (new Page(/* ... */))->setPages(function (Page $page) {
    //            $page->addPage([
    //                'title'    => 'Blog',
    //                'priority' => 100,
    //                'model'    => \App\Blog::class
	//		      ));
    //
	//		      $page->addPage(\App\Blog::class);
    //	      }),
    //
    //        // or
    //
    //        [
    //            'title'       => 'News',
    //            'priority'    => 300,
    //            'accessLogic' => function ($page) {
    //                return $page->isActive();
    //		      },
    //            'pages'       => [
    //
    //                // ...
    //
    //            ]
    //        ]
    //    ]
    // ]
];
